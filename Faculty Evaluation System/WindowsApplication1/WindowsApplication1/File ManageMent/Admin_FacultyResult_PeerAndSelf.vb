﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class Admin_FacultyResult_PeerAndSelf
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim ID As String
    Public Faculty_ID As String
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FacultyResultManagementForm.Show()
        Me.Close()

    End Sub

    Private Sub btn_View_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_View.Click
        Faculty_ID = Combobox_ID.Text
        Admin_FacultyResult_PeerAndSelf_Actual.Show()
    End Sub

    Private Sub Admin_FacultyResult_PeerAndSelf_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IDCombobox()
    End Sub
    Sub IDCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblfaculty"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                ID = Reader.GetString("ID_Number")
                Combobox_ID.Items.Add(ID)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class