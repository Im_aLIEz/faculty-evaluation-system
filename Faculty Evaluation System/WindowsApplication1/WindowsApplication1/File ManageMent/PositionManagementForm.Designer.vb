﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PositionManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PositionManagementForm))
        Me.DGV_Position = New System.Windows.Forms.DataGridView()
        Me.Button_AddPosition = New System.Windows.Forms.Button()
        Me.Textbox_Position = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button_BackPosition = New System.Windows.Forms.Button()
        Me.Button_DeletePosition = New System.Windows.Forms.Button()
        CType(Me.DGV_Position, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Position
        '
        Me.DGV_Position.AllowUserToAddRows = False
        Me.DGV_Position.AllowUserToDeleteRows = False
        Me.DGV_Position.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.DGV_Position.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Position.Location = New System.Drawing.Point(12, 59)
        Me.DGV_Position.Name = "DGV_Position"
        Me.DGV_Position.ReadOnly = True
        Me.DGV_Position.Size = New System.Drawing.Size(571, 311)
        Me.DGV_Position.TabIndex = 0
        '
        'Button_AddPosition
        '
        Me.Button_AddPosition.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_AddPosition.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_AddPosition.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_AddPosition.FlatAppearance.BorderSize = 0
        Me.Button_AddPosition.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_AddPosition.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_AddPosition.Location = New System.Drawing.Point(345, 407)
        Me.Button_AddPosition.Name = "Button_AddPosition"
        Me.Button_AddPosition.Size = New System.Drawing.Size(70, 34)
        Me.Button_AddPosition.TabIndex = 14
        Me.Button_AddPosition.Text = "Add"
        Me.Button_AddPosition.UseVisualStyleBackColor = False
        '
        'Textbox_Position
        '
        Me.Textbox_Position.Font = New System.Drawing.Font("Franklin Gothic Book", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_Position.Location = New System.Drawing.Point(178, 409)
        Me.Textbox_Position.Name = "Textbox_Position"
        Me.Textbox_Position.Size = New System.Drawing.Size(161, 26)
        Me.Textbox_Position.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(111, 409)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 21)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Position:"
        '
        'Button_BackPosition
        '
        Me.Button_BackPosition.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_BackPosition.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackPosition.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_BackPosition.FlatAppearance.BorderSize = 0
        Me.Button_BackPosition.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackPosition.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackPosition.Location = New System.Drawing.Point(12, 22)
        Me.Button_BackPosition.Name = "Button_BackPosition"
        Me.Button_BackPosition.Size = New System.Drawing.Size(83, 29)
        Me.Button_BackPosition.TabIndex = 22
        Me.Button_BackPosition.Text = "Back"
        Me.Button_BackPosition.UseVisualStyleBackColor = False
        '
        'Button_DeletePosition
        '
        Me.Button_DeletePosition.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_DeletePosition.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_DeletePosition.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_DeletePosition.FlatAppearance.BorderSize = 0
        Me.Button_DeletePosition.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeletePosition.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_DeletePosition.Location = New System.Drawing.Point(421, 407)
        Me.Button_DeletePosition.Name = "Button_DeletePosition"
        Me.Button_DeletePosition.Size = New System.Drawing.Size(70, 34)
        Me.Button_DeletePosition.TabIndex = 15
        Me.Button_DeletePosition.Text = "Delete"
        Me.Button_DeletePosition.UseVisualStyleBackColor = False
        '
        'PositionManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 499)
        Me.Controls.Add(Me.Button_BackPosition)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Textbox_Position)
        Me.Controls.Add(Me.Button_DeletePosition)
        Me.Controls.Add(Me.Button_AddPosition)
        Me.Controls.Add(Me.DGV_Position)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PositionManagementForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Position Management Form"
        CType(Me.DGV_Position, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Position As System.Windows.Forms.DataGridView
    Friend WithEvents Button_AddPosition As System.Windows.Forms.Button
    Friend WithEvents Textbox_Position As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button_BackPosition As System.Windows.Forms.Button
    Friend WithEvents Button_DeletePosition As System.Windows.Forms.Button
End Class
