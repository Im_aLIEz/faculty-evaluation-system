﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class FacultyManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Button_AddFaculty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddFaculty.Click
        Add()
        FacultyDataGridView()
    End Sub

    Private Sub Button_EditFaculty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_EditFaculty.Click
        UpdateButton()
        FacultyDataGridView()
    End Sub

    Private Sub Button_ClearFaculty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ClearFaculty.Click
        Textbox_FnameFaculty.Text = ""
        Textbox_LnameFaculty.Text = ""
        Combobox_IDFaculty.Text = ""
    End Sub

    Private Sub Button_DeleteFaculty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteFaculty.Click
        DeleteButton()
        FacultyDataGridView()
    End Sub

    Private Sub Button_BackFaculty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackFaculty.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub FacultyManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FacultyDataGridView()
        PositionCombobox()
        IDNumberCombobox()
    End Sub

    'method for add button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblfaculty (Last_Name,First_Name,ID_Number,Position) values ('" & Textbox_LnameFaculty.Text & "','" & Textbox_FnameFaculty.Text & "','" & Combobox_IDFaculty.Text & "','" & Combobox_PositionFaculty.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try


    End Sub

    'method for update button'

    Sub UpdateButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update dbcvs.tblfaculty set Last_Name='" & Textbox_LnameFaculty.Text & "',First_Name='" & Textbox_FnameFaculty.Text & "',ID_Number='" & Combobox_IDFaculty.Text & "',Position='" & Combobox_PositionFaculty.Text & "' where ID_Number = '" & Combobox_IDFaculty.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Updated")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for delete button'

    Sub DeleteButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblfaculty where ID_Number='" & Combobox_IDFaculty.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for data grid view'
    Sub FacultyDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblfaculty"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            DGV_Faculty.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        DGV_Faculty.Columns(0).Visible = False
        DGV_Faculty.Columns(4).Visible = False
        DGV_Faculty.Columns(5).Visible = False
        DGV_Faculty.Columns(6).Visible = False
    End Sub

    Sub PositionCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblposition"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim position = Reader.GetString("Position")
                Combobox_PositionFaculty.Items.Add(position)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub IDNumberCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblaccount where Priv_Lvl = 'Faculty'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim ID_Num = Reader.GetString("ID_Number")
                Combobox_IDFaculty.Items.Add(ID_Num)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub DGV_Faculty_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DGV_Faculty.MouseClick
        Textbox_LnameFaculty.Text = DGV_Faculty.SelectedRows(0).Cells(2).Value.ToString()
        Textbox_FnameFaculty.Text = DGV_Faculty.SelectedRows(0).Cells(3).Value.ToString()
        Combobox_IDFaculty.Text = DGV_Faculty.SelectedRows(0).Cells(4).Value.ToString()
        Combobox_PositionFaculty.Text = DGV_Faculty.SelectedRows(0).Cells(5).Value.ToString()
    End Sub
End Class