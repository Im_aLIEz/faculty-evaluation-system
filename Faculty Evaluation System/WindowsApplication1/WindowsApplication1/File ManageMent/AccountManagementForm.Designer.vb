﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AccountManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AccountManagementForm))
        Me.Button_BackAccount = New System.Windows.Forms.Button()
        Me.DGV_Account = New System.Windows.Forms.DataGridView()
        Me.DbcvsDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DbcvsDataSet = New Faculty_Evaluation_System.dbcvsDataSet()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Textbox_IDNumAccount = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Textbox_PasswordAccount = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Combobox_PrivilegeAccount = New System.Windows.Forms.ComboBox()
        Me.DbcvsDataSetBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DbcvsDataSetBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button_AddAccount = New System.Windows.Forms.Button()
        Me.Button_DeleteAccount = New System.Windows.Forms.Button()
        Me.Button_UpdateAccount = New System.Windows.Forms.Button()
        Me.Button_ClearAccount = New System.Windows.Forms.Button()
        CType(Me.DGV_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DbcvsDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DbcvsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DbcvsDataSetBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DbcvsDataSetBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button_BackAccount
        '
        Me.Button_BackAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_BackAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackAccount.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_BackAccount.FlatAppearance.BorderSize = 0
        Me.Button_BackAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackAccount.Location = New System.Drawing.Point(12, 24)
        Me.Button_BackAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button_BackAccount.Name = "Button_BackAccount"
        Me.Button_BackAccount.Size = New System.Drawing.Size(71, 33)
        Me.Button_BackAccount.TabIndex = 0
        Me.Button_BackAccount.Text = "Back"
        Me.Button_BackAccount.UseVisualStyleBackColor = False
        '
        'DGV_Account
        '
        Me.DGV_Account.AllowUserToAddRows = False
        Me.DGV_Account.AllowUserToDeleteRows = False
        Me.DGV_Account.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.DGV_Account.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DGV_Account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Account.Location = New System.Drawing.Point(12, 63)
        Me.DGV_Account.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.DGV_Account.Name = "DGV_Account"
        Me.DGV_Account.ReadOnly = True
        Me.DGV_Account.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Account.Size = New System.Drawing.Size(571, 311)
        Me.DGV_Account.TabIndex = 6
        '
        'DbcvsDataSetBindingSource
        '
        Me.DbcvsDataSetBindingSource.DataSource = Me.DbcvsDataSet
        Me.DbcvsDataSetBindingSource.Position = 0
        '
        'DbcvsDataSet
        '
        Me.DbcvsDataSet.DataSetName = "dbcvsDataSet"
        Me.DbcvsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(154, 396)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "ID number:"
        '
        'Textbox_IDNumAccount
        '
        Me.Textbox_IDNumAccount.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_IDNumAccount.Location = New System.Drawing.Point(220, 393)
        Me.Textbox_IDNumAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Textbox_IDNumAccount.Name = "Textbox_IDNumAccount"
        Me.Textbox_IDNumAccount.Size = New System.Drawing.Size(201, 22)
        Me.Textbox_IDNumAccount.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(154, 420)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Password:"
        '
        'Textbox_PasswordAccount
        '
        Me.Textbox_PasswordAccount.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_PasswordAccount.Location = New System.Drawing.Point(220, 419)
        Me.Textbox_PasswordAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Textbox_PasswordAccount.Name = "Textbox_PasswordAccount"
        Me.Textbox_PasswordAccount.Size = New System.Drawing.Size(201, 22)
        Me.Textbox_PasswordAccount.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(154, 445)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 17)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Privilege:"
        '
        'Combobox_PrivilegeAccount
        '
        Me.Combobox_PrivilegeAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Combobox_PrivilegeAccount.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_PrivilegeAccount.FormattingEnabled = True
        Me.Combobox_PrivilegeAccount.Items.AddRange(New Object() {"Student", "Faculty"})
        Me.Combobox_PrivilegeAccount.Location = New System.Drawing.Point(220, 445)
        Me.Combobox_PrivilegeAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Combobox_PrivilegeAccount.Name = "Combobox_PrivilegeAccount"
        Me.Combobox_PrivilegeAccount.Size = New System.Drawing.Size(201, 25)
        Me.Combobox_PrivilegeAccount.TabIndex = 12
        '
        'DbcvsDataSetBindingSource1
        '
        Me.DbcvsDataSetBindingSource1.DataSource = Me.DbcvsDataSet
        Me.DbcvsDataSetBindingSource1.Position = 0
        '
        'DbcvsDataSetBindingSource2
        '
        Me.DbcvsDataSetBindingSource2.DataSource = Me.DbcvsDataSet
        Me.DbcvsDataSetBindingSource2.Position = 0
        '
        'Button_AddAccount
        '
        Me.Button_AddAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_AddAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_AddAccount.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_AddAccount.FlatAppearance.BorderSize = 0
        Me.Button_AddAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_AddAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_AddAccount.Location = New System.Drawing.Point(199, 489)
        Me.Button_AddAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button_AddAccount.Name = "Button_AddAccount"
        Me.Button_AddAccount.Size = New System.Drawing.Size(90, 43)
        Me.Button_AddAccount.TabIndex = 1
        Me.Button_AddAccount.Text = "Add"
        Me.Button_AddAccount.UseVisualStyleBackColor = False
        '
        'Button_DeleteAccount
        '
        Me.Button_DeleteAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_DeleteAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_DeleteAccount.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_DeleteAccount.FlatAppearance.BorderSize = 0
        Me.Button_DeleteAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeleteAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_DeleteAccount.Location = New System.Drawing.Point(297, 538)
        Me.Button_DeleteAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button_DeleteAccount.Name = "Button_DeleteAccount"
        Me.Button_DeleteAccount.Size = New System.Drawing.Size(90, 43)
        Me.Button_DeleteAccount.TabIndex = 4
        Me.Button_DeleteAccount.Text = "Delete"
        Me.Button_DeleteAccount.UseVisualStyleBackColor = False
        '
        'Button_UpdateAccount
        '
        Me.Button_UpdateAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_UpdateAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_UpdateAccount.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_UpdateAccount.FlatAppearance.BorderSize = 0
        Me.Button_UpdateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_UpdateAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_UpdateAccount.Location = New System.Drawing.Point(297, 489)
        Me.Button_UpdateAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button_UpdateAccount.Name = "Button_UpdateAccount"
        Me.Button_UpdateAccount.Size = New System.Drawing.Size(90, 43)
        Me.Button_UpdateAccount.TabIndex = 5
        Me.Button_UpdateAccount.Text = "Update"
        Me.Button_UpdateAccount.UseVisualStyleBackColor = False
        '
        'Button_ClearAccount
        '
        Me.Button_ClearAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_ClearAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_ClearAccount.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue
        Me.Button_ClearAccount.FlatAppearance.BorderSize = 0
        Me.Button_ClearAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_ClearAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_ClearAccount.Location = New System.Drawing.Point(199, 538)
        Me.Button_ClearAccount.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button_ClearAccount.Name = "Button_ClearAccount"
        Me.Button_ClearAccount.Size = New System.Drawing.Size(90, 43)
        Me.Button_ClearAccount.TabIndex = 13
        Me.Button_ClearAccount.Text = "Clear"
        Me.Button_ClearAccount.UseVisualStyleBackColor = False
        '
        'AccountManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 616)
        Me.Controls.Add(Me.Button_ClearAccount)
        Me.Controls.Add(Me.Combobox_PrivilegeAccount)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Textbox_PasswordAccount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Textbox_IDNumAccount)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DGV_Account)
        Me.Controls.Add(Me.Button_UpdateAccount)
        Me.Controls.Add(Me.Button_DeleteAccount)
        Me.Controls.Add(Me.Button_AddAccount)
        Me.Controls.Add(Me.Button_BackAccount)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AccountManagementForm"
        Me.RightToLeftLayout = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Account Management Form"
        CType(Me.DGV_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DbcvsDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DbcvsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DbcvsDataSetBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DbcvsDataSetBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_BackAccount As System.Windows.Forms.Button
    Friend WithEvents DGV_Account As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Textbox_IDNumAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Textbox_PasswordAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Combobox_PrivilegeAccount As System.Windows.Forms.ComboBox
    Friend WithEvents DbcvsDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DbcvsDataSet As Faculty_Evaluation_System.dbcvsDataSet
    Friend WithEvents DbcvsDataSetBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DbcvsDataSetBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents Button_AddAccount As System.Windows.Forms.Button
    Friend WithEvents Button_DeleteAccount As System.Windows.Forms.Button
    Friend WithEvents Button_UpdateAccount As System.Windows.Forms.Button
    Friend WithEvents Button_ClearAccount As System.Windows.Forms.Button
End Class
