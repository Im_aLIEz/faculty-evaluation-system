﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SubjectForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubjectForm))
        Me.Back_SubjectMngmnt = New System.Windows.Forms.Button()
        Me.Textbox_SubjectCode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button_DeleteSubject = New System.Windows.Forms.Button()
        Me.Button_AddSubject = New System.Windows.Forms.Button()
        Me.Dgv_Subject = New System.Windows.Forms.DataGridView()
        Me.Textbox_Subject = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.Dgv_Subject, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Back_SubjectMngmnt
        '
        Me.Back_SubjectMngmnt.BackColor = System.Drawing.Color.DodgerBlue
        Me.Back_SubjectMngmnt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Back_SubjectMngmnt.FlatAppearance.BorderSize = 0
        Me.Back_SubjectMngmnt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Back_SubjectMngmnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Back_SubjectMngmnt.Location = New System.Drawing.Point(12, 28)
        Me.Back_SubjectMngmnt.Name = "Back_SubjectMngmnt"
        Me.Back_SubjectMngmnt.Size = New System.Drawing.Size(83, 29)
        Me.Back_SubjectMngmnt.TabIndex = 32
        Me.Back_SubjectMngmnt.Text = "Back"
        Me.Back_SubjectMngmnt.UseVisualStyleBackColor = False
        '
        'Textbox_SubjectCode
        '
        Me.Textbox_SubjectCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_SubjectCode.Location = New System.Drawing.Point(151, 424)
        Me.Textbox_SubjectCode.Name = "Textbox_SubjectCode"
        Me.Textbox_SubjectCode.Size = New System.Drawing.Size(185, 22)
        Me.Textbox_SubjectCode.TabIndex = 29
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(53, 424)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 20)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Subject Code:"
        '
        'Button_DeleteSubject
        '
        Me.Button_DeleteSubject.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_DeleteSubject.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_DeleteSubject.FlatAppearance.BorderSize = 0
        Me.Button_DeleteSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeleteSubject.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_DeleteSubject.Location = New System.Drawing.Point(437, 402)
        Me.Button_DeleteSubject.Name = "Button_DeleteSubject"
        Me.Button_DeleteSubject.Size = New System.Drawing.Size(89, 43)
        Me.Button_DeleteSubject.TabIndex = 25
        Me.Button_DeleteSubject.Text = "Delete"
        Me.Button_DeleteSubject.UseVisualStyleBackColor = False
        '
        'Button_AddSubject
        '
        Me.Button_AddSubject.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_AddSubject.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_AddSubject.FlatAppearance.BorderSize = 0
        Me.Button_AddSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_AddSubject.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_AddSubject.Location = New System.Drawing.Point(342, 402)
        Me.Button_AddSubject.Name = "Button_AddSubject"
        Me.Button_AddSubject.Size = New System.Drawing.Size(89, 43)
        Me.Button_AddSubject.TabIndex = 24
        Me.Button_AddSubject.Text = "Add"
        Me.Button_AddSubject.UseVisualStyleBackColor = False
        '
        'Dgv_Subject
        '
        Me.Dgv_Subject.AllowUserToAddRows = False
        Me.Dgv_Subject.AllowUserToDeleteRows = False
        Me.Dgv_Subject.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.Dgv_Subject.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Dgv_Subject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv_Subject.Location = New System.Drawing.Point(12, 63)
        Me.Dgv_Subject.Name = "Dgv_Subject"
        Me.Dgv_Subject.ReadOnly = True
        Me.Dgv_Subject.RowHeadersWidth = 60
        Me.Dgv_Subject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Dgv_Subject.Size = New System.Drawing.Size(571, 311)
        Me.Dgv_Subject.TabIndex = 23
        '
        'Textbox_Subject
        '
        Me.Textbox_Subject.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_Subject.Location = New System.Drawing.Point(151, 396)
        Me.Textbox_Subject.Name = "Textbox_Subject"
        Me.Textbox_Subject.Size = New System.Drawing.Size(185, 22)
        Me.Textbox_Subject.TabIndex = 34
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(86, 396)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 20)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Subject:"
        '
        'SubjectForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 473)
        Me.Controls.Add(Me.Textbox_Subject)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Back_SubjectMngmnt)
        Me.Controls.Add(Me.Textbox_SubjectCode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button_DeleteSubject)
        Me.Controls.Add(Me.Button_AddSubject)
        Me.Controls.Add(Me.Dgv_Subject)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SubjectForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SubjectForm"
        CType(Me.Dgv_Subject, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Back_SubjectMngmnt As System.Windows.Forms.Button
    Friend WithEvents Textbox_SubjectCode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button_DeleteSubject As System.Windows.Forms.Button
    Friend WithEvents Button_AddSubject As System.Windows.Forms.Button
    Friend WithEvents Dgv_Subject As System.Windows.Forms.DataGridView
    Friend WithEvents Textbox_Subject As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
