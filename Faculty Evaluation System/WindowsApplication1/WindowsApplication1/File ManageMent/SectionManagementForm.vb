﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class SectionManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Combobox_Section.Items.Clear()
        Add()
        SectionCombobox()
    End Sub

    'method for addSection button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblsection (Section) values ('" & Textbox_addsection.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    'method for data grid view'
    Sub SectionCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblsection"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Section = Reader.GetString("Section")
                Combobox_Section.Items.Add(Section)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub SectionDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblstudent where Section = '" & Combobox_Section.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            DGV_Section.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        DGV_Section.Columns(0).Visible = False
    End Sub

    Sub Delete()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblsection where Section='" & Combobox_Section.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Section Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub SectionManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SectionCombobox()
    End Sub

    Private Sub Button_Search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Search.Click
        SectionDataGridView()
    End Sub

    Private Sub Button_BackAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackAccount.Click
        AdminForm.Show()
        Me.Hide()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Delete()
        SectionCombobox()
    End Sub

    Private Sub Label3_Click(sender As System.Object, e As System.EventArgs) Handles Label3.Click

    End Sub
End Class