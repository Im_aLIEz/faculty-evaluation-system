﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class AccountManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Button_AddAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddAccount.Click
        Add()
        If Combobox_PrivilegeAccount.Text = "Faculty" Then
            Add_adaptability()
            Add_cat()
            Add_commitment()
            Add_cr()
            Add_da()
            Add_ec()
            Add_es()
            Add_iaj()
            Add_kosm()
            Add_mc_atl()
            Add_mol()
            Add_pa_cah()
            Add_punctuality()
            Add_respect()
            Add_responsibility()
            Add_tfil()
        End If
        Clear()
        AccountDataGridView()
    End Sub

    Private Sub Button_ClearAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ClearAccount.Click
        Clear()
    End Sub

    Private Sub Button_UpdateAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_UpdateAccount.Click
        UpdateButton()
        AccountDataGridView()
        Clear()
    End Sub

    Private Sub Button_DeleteAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteAccount.Click
        DeleteButton()
        AccountDataGridView()
        Clear()
    End Sub

    Private Sub Button_BackAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackAccount.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub AccountManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AccountDataGridView()
        Clear()
    End Sub

    'method for add button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblaccount (ID_Number,Password,Priv_Lvl) values ('" & Textbox_IDNumAccount.Text & "','" & Textbox_PasswordAccount.Text & "','" & Combobox_PrivilegeAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for update button'

    Sub UpdateButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update dbcvs.tblaccount set ID_Number='" & Textbox_IDNumAccount.Text & "',Password='" & Textbox_PasswordAccount.Text &
                    "',Priv_Lvl='" & Combobox_PrivilegeAccount.Text & "' where ID = '" & DGV_Account.SelectedRows(0).Cells(0).Value.ToString() & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Updated")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for delete button'

    Sub DeleteButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblaccount where ID='" & DGV_Account.SelectedRows(0).Cells(0).Value.ToString() & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for data grid view'
    Sub AccountDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblaccount"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            DGV_Account.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        DGV_Account.Columns(0).Visible = False
    End Sub


    Sub Clear()
        Button_AddAccount.Enabled = True
        Button_DeleteAccount.Enabled = False
        Button_UpdateAccount.Enabled = False
        Textbox_IDNumAccount.Text = ""
        Textbox_PasswordAccount.Text = ""
        Combobox_PrivilegeAccount.Text = ""
        DGV_Account.ClearSelection()
        Combobox_PrivilegeAccount.Enabled = True
    End Sub

    Sub ButtonMechanics()
        
    End Sub


    'mouseclick event from DataGridView'
    Private Sub DGV_Account_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DGV_Account.MouseClick
        Textbox_IDNumAccount.Text = DGV_Account.SelectedRows(0).Cells(1).Value.ToString()
        Textbox_PasswordAccount.Text = DGV_Account.SelectedRows(0).Cells(2).Value.ToString()
        Combobox_PrivilegeAccount.Text = DGV_Account.SelectedRows(0).Cells(3).Value.ToString()
        Button_DeleteAccount.Enabled = True
        Button_AddAccount.Enabled = False
        Button_UpdateAccount.Enabled = True
        Combobox_PrivilegeAccount.Enabled = False
    End Sub


    Private Sub Textbox_SearchAccount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    
    Private Sub Textbox_IDNumAccount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Textbox_IDNumAccount.TextChanged
        ButtonMechanics()
    End Sub

    Private Sub Textbox_PasswordAccount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Textbox_PasswordAccount.TextChanged
        ButtonMechanics()
    End Sub

    Sub Add_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_iaj (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_adaptability (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_cat()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_cat (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_cr (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_da (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_ec (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_es (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_mc_atl (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_pa_cah (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_punctuality (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_respect (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultfaculty_responsibility (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_commitment()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultstd_commitment (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_kosm()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultstd_kosm (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_mol()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultstd_mol (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Add_tfil()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblresultstd_tfil (ID_Number) values ('" & Textbox_IDNumAccount.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub DGV_Account_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_Account.CellContentClick

    End Sub
End Class