﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class StudentManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Button_ClearStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ClearStudent.Click
        Combobox_IDNumberStudent.Text = ""
        Textbox_FNameStudent.Text = ""
        Textbox_LNameStudent.Text = ""
    End Sub

    Private Sub Button_AddStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddStudent.Click
        Add()
        StudentDataGridView()
    End Sub

    Private Sub Button_EditStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_EditStudent.Click
        UpdateButton()
        StudentDataGridView()
    End Sub

    Private Sub Button_DeleteStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteStudent.Click
        DeleteButton()
        StudentDataGridView()
    End Sub

    Private Sub Button_BackStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackStudent.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub StudentManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        StudentDataGridView()
        IDNumCombobox()
        SectionCombobox()
    End Sub

    'method for add button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblstudent (ID_Number,First_Name,Last_Name,Section) values ('" & Combobox_IDNumberStudent.Text & "','" & Textbox_FNameStudent.Text & "','" & Textbox_LNameStudent.Text & "','" & Combobox_SectionStudent.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for update button'

    Sub UpdateButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update dbcvs.tblstudent set Last_Name='" & Textbox_LNameStudent.Text & "',First_Name='" & Textbox_FNameStudent.Text & "',ID_Number='" & Combobox_IDNumberStudent.Text & "',Section='" & Combobox_SectionStudent.Text & "' where ID_Number = '" & Combobox_IDNumberStudent.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Updated")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for delete button'

    Sub DeleteButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblstudent where ID_Number='" & Combobox_IDNumberStudent.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for data grid view'
    Sub StudentDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblstudent"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            DGV_Student.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        DGV_Student.Columns(0).Visible = False
    End Sub

    Sub IDNumCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblaccount where Priv_Lvl = 'Student'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim ID_Num = Reader.GetString("ID_Number")
                Combobox_IDNumberStudent.Items.Add(ID_Num)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub SectionCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblsection"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Section = Reader.GetString("Section")
                Combobox_SectionStudent.Items.Add(Section)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub DGV_Student_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DGV_Student.MouseClick
        Textbox_LNameStudent.Text = DGV_Student.SelectedRows(0).Cells(2).Value.ToString()
        Textbox_FNameStudent.Text = DGV_Student.SelectedRows(0).Cells(3).Value.ToString()
        Combobox_IDNumberStudent.Text = DGV_Student.SelectedRows(0).Cells(4).Value.ToString()
        Combobox_SectionStudent.Text = DGV_Student.SelectedRows(0).Cells(5).Value.ToString()
    End Sub
End Class