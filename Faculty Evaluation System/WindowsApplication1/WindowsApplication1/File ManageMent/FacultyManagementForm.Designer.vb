﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_Faculty = New System.Windows.Forms.DataGridView()
        Me.Button_ClearFaculty = New System.Windows.Forms.Button()
        Me.Textbox_FnameFaculty = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Textbox_LnameFaculty = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button_EditFaculty = New System.Windows.Forms.Button()
        Me.Button_DeleteFaculty = New System.Windows.Forms.Button()
        Me.Button_AddFaculty = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button_BackFaculty = New System.Windows.Forms.Button()
        Me.Combobox_IDFaculty = New System.Windows.Forms.ComboBox()
        Me.Position = New System.Windows.Forms.Label()
        Me.Combobox_PositionFaculty = New System.Windows.Forms.ComboBox()
        CType(Me.DGV_Faculty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Faculty
        '
        Me.DGV_Faculty.AllowUserToAddRows = False
        Me.DGV_Faculty.AllowUserToDeleteRows = False
        Me.DGV_Faculty.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.DGV_Faculty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Faculty.Location = New System.Drawing.Point(12, 64)
        Me.DGV_Faculty.Name = "DGV_Faculty"
        Me.DGV_Faculty.ReadOnly = True
        Me.DGV_Faculty.RowHeadersWidth = 50
        Me.DGV_Faculty.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Faculty.Size = New System.Drawing.Size(571, 311)
        Me.DGV_Faculty.TabIndex = 13
        '
        'Button_ClearFaculty
        '
        Me.Button_ClearFaculty.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_ClearFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_ClearFaculty.FlatAppearance.BorderSize = 0
        Me.Button_ClearFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_ClearFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_ClearFaculty.Location = New System.Drawing.Point(360, 487)
        Me.Button_ClearFaculty.Name = "Button_ClearFaculty"
        Me.Button_ClearFaculty.Size = New System.Drawing.Size(80, 53)
        Me.Button_ClearFaculty.TabIndex = 23
        Me.Button_ClearFaculty.Text = "Clear"
        Me.Button_ClearFaculty.UseVisualStyleBackColor = False
        '
        'Textbox_FnameFaculty
        '
        Me.Textbox_FnameFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_FnameFaculty.Location = New System.Drawing.Point(149, 457)
        Me.Textbox_FnameFaculty.Name = "Textbox_FnameFaculty"
        Me.Textbox_FnameFaculty.Size = New System.Drawing.Size(205, 22)
        Me.Textbox_FnameFaculty.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(68, 461)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 20)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "First Name:"
        '
        'Textbox_LnameFaculty
        '
        Me.Textbox_LnameFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_LnameFaculty.Location = New System.Drawing.Point(149, 428)
        Me.Textbox_LnameFaculty.Name = "Textbox_LnameFaculty"
        Me.Textbox_LnameFaculty.Size = New System.Drawing.Size(205, 22)
        Me.Textbox_LnameFaculty.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(68, 430)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 20)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Last Name:"
        '
        'Button_EditFaculty
        '
        Me.Button_EditFaculty.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_EditFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_EditFaculty.FlatAppearance.BorderSize = 0
        Me.Button_EditFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_EditFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_EditFaculty.Location = New System.Drawing.Point(447, 428)
        Me.Button_EditFaculty.Name = "Button_EditFaculty"
        Me.Button_EditFaculty.Size = New System.Drawing.Size(80, 53)
        Me.Button_EditFaculty.TabIndex = 16
        Me.Button_EditFaculty.Text = "Edit"
        Me.Button_EditFaculty.UseVisualStyleBackColor = False
        '
        'Button_DeleteFaculty
        '
        Me.Button_DeleteFaculty.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_DeleteFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_DeleteFaculty.FlatAppearance.BorderSize = 0
        Me.Button_DeleteFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeleteFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_DeleteFaculty.Location = New System.Drawing.Point(447, 487)
        Me.Button_DeleteFaculty.Name = "Button_DeleteFaculty"
        Me.Button_DeleteFaculty.Size = New System.Drawing.Size(80, 53)
        Me.Button_DeleteFaculty.TabIndex = 15
        Me.Button_DeleteFaculty.Text = "Delete"
        Me.Button_DeleteFaculty.UseVisualStyleBackColor = False
        '
        'Button_AddFaculty
        '
        Me.Button_AddFaculty.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_AddFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_AddFaculty.FlatAppearance.BorderSize = 0
        Me.Button_AddFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_AddFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_AddFaculty.Location = New System.Drawing.Point(360, 428)
        Me.Button_AddFaculty.Name = "Button_AddFaculty"
        Me.Button_AddFaculty.Size = New System.Drawing.Size(80, 53)
        Me.Button_AddFaculty.TabIndex = 14
        Me.Button_AddFaculty.Text = "Add"
        Me.Button_AddFaculty.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(68, 489)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(90, 20)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "ID Number:"
        '
        'Button_BackFaculty
        '
        Me.Button_BackFaculty.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_BackFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackFaculty.FlatAppearance.BorderSize = 0
        Me.Button_BackFaculty.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackFaculty.Location = New System.Drawing.Point(12, 22)
        Me.Button_BackFaculty.Name = "Button_BackFaculty"
        Me.Button_BackFaculty.Size = New System.Drawing.Size(71, 33)
        Me.Button_BackFaculty.TabIndex = 32
        Me.Button_BackFaculty.Text = "Back"
        Me.Button_BackFaculty.UseVisualStyleBackColor = False
        '
        'Combobox_IDFaculty
        '
        Me.Combobox_IDFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Combobox_IDFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combobox_IDFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_IDFaculty.FormattingEnabled = True
        Me.Combobox_IDFaculty.Location = New System.Drawing.Point(148, 485)
        Me.Combobox_IDFaculty.Name = "Combobox_IDFaculty"
        Me.Combobox_IDFaculty.Size = New System.Drawing.Size(204, 24)
        Me.Combobox_IDFaculty.TabIndex = 33
        '
        'Position
        '
        Me.Position.AutoSize = True
        Me.Position.BackColor = System.Drawing.Color.Transparent
        Me.Position.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Position.Location = New System.Drawing.Point(68, 519)
        Me.Position.Name = "Position"
        Me.Position.Size = New System.Drawing.Size(69, 20)
        Me.Position.TabIndex = 34
        Me.Position.Text = "Position:"
        '
        'Combobox_PositionFaculty
        '
        Me.Combobox_PositionFaculty.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Combobox_PositionFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combobox_PositionFaculty.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_PositionFaculty.FormattingEnabled = True
        Me.Combobox_PositionFaculty.Location = New System.Drawing.Point(148, 515)
        Me.Combobox_PositionFaculty.Name = "Combobox_PositionFaculty"
        Me.Combobox_PositionFaculty.Size = New System.Drawing.Size(204, 24)
        Me.Combobox_PositionFaculty.TabIndex = 35
        '
        'FacultyManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 616)
        Me.Controls.Add(Me.Combobox_PositionFaculty)
        Me.Controls.Add(Me.Position)
        Me.Controls.Add(Me.Combobox_IDFaculty)
        Me.Controls.Add(Me.Button_BackFaculty)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button_ClearFaculty)
        Me.Controls.Add(Me.Textbox_FnameFaculty)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Textbox_LnameFaculty)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button_EditFaculty)
        Me.Controls.Add(Me.Button_DeleteFaculty)
        Me.Controls.Add(Me.Button_AddFaculty)
        Me.Controls.Add(Me.DGV_Faculty)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyManagementForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Faculty Management Form"
        CType(Me.DGV_Faculty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Faculty As System.Windows.Forms.DataGridView
    Friend WithEvents Button_ClearFaculty As System.Windows.Forms.Button
    Friend WithEvents Textbox_FnameFaculty As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Textbox_LnameFaculty As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button_EditFaculty As System.Windows.Forms.Button
    Friend WithEvents Button_DeleteFaculty As System.Windows.Forms.Button
    Friend WithEvents Button_AddFaculty As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button_BackFaculty As System.Windows.Forms.Button
    Friend WithEvents Combobox_IDFaculty As System.Windows.Forms.ComboBox
    Friend WithEvents Position As System.Windows.Forms.Label
    Friend WithEvents Combobox_PositionFaculty As System.Windows.Forms.ComboBox
End Class
