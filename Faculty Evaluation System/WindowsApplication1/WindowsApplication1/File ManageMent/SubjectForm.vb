﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class SubjectForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Back_SubjectMngmnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Back_SubjectMngmnt.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub Add_Subject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddSubject.Click
        Add()
        SubjectDataGridView()
    End Sub

    Private Sub Delete_Subject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteSubject.Click
        DeleteButton()
        SubjectDataGridView()
    End Sub

    Private Sub SubjectForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SubjectDataGridView()
    End Sub

    'method for add button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblsubject (Subject_Code,Subject) values ('" & Textbox_SubjectCode.Text & "', '" & Textbox_Subject.Text & "')"
            Command = New MySqlCommand(Query, Mysqlconn)
            Reader = Command.ExecuteReader

            MessageBox.Show("Data Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for delete button'

    Sub DeleteButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblsubject where Subject = '" & Textbox_Subject.Text & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for data grid view'
    Sub SubjectDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblsubject"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            Dgv_Subject.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        Dgv_Subject.Columns(0).Visible = False
    End Sub

    
    Private Sub Dgv_Subject_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dgv_Subject.CellContentClick

    End Sub

    Private Sub Dgv_Subject_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv_Subject.MouseClick
        Textbox_Subject.Text = Dgv_Subject.SelectedRows(0).Cells(2).Value.ToString()
        Textbox_SubjectCode.Text = Dgv_Subject.SelectedRows(0).Cells(1).Value.ToString()
    End Sub
End Class