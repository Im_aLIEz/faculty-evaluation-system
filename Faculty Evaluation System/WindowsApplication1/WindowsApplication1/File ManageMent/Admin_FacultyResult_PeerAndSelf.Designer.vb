﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Admin_FacultyResult_PeerAndSelf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Admin_FacultyResult_PeerAndSelf))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Combobox_ID = New System.Windows.Forms.ComboBox()
        Me.btn_View = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(483, 402)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(71, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Back"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(394, 259)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ID:"
        '
        'Combobox_ID
        '
        Me.Combobox_ID.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Combobox_ID.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_ID.FormattingEnabled = True
        Me.Combobox_ID.Location = New System.Drawing.Point(427, 255)
        Me.Combobox_ID.Name = "Combobox_ID"
        Me.Combobox_ID.Size = New System.Drawing.Size(180, 25)
        Me.Combobox_ID.TabIndex = 2
        '
        'btn_View
        '
        Me.btn_View.BackColor = System.Drawing.Color.DodgerBlue
        Me.btn_View.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_View.FlatAppearance.BorderSize = 0
        Me.btn_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_View.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_View.Location = New System.Drawing.Point(468, 287)
        Me.btn_View.Name = "btn_View"
        Me.btn_View.Size = New System.Drawing.Size(101, 42)
        Me.btn_View.TabIndex = 3
        Me.btn_View.Text = "View"
        Me.btn_View.UseVisualStyleBackColor = False
        '
        'Admin_FacultyResult_PeerAndSelf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.btn_View)
        Me.Controls.Add(Me.Combobox_ID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Admin_FacultyResult_PeerAndSelf"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Admin_FacultyResult_PeerAndSelf"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Combobox_ID As System.Windows.Forms.ComboBox
    Friend WithEvents btn_View As System.Windows.Forms.Button
End Class
