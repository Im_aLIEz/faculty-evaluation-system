﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class FacultyResultManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim ID As String
    Public Faculty_ID As String
    Public idcheck As String
    Dim usercontrol1 As New UserControl1

    Private Sub Button_BackFacultyResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackFacultyResult.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        idcheck = Combobox_ID.Text
        If idcheck = "" Then
            MsgBox("Select an ID Number first to Proceed.")
        Else
            test.Show()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        idcheck = Combobox_ID.Text
        If idcheck = "" Then
            MsgBox("Select an ID Number first to Proceed.")
        Else
            Admin_FacultyResult_PeerAndSelf_Actual.Show()
        End If
    End Sub

    Private Sub FacultyResultManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IDCombobox()
    End Sub

    Sub IDCombobox()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblfaculty"
            Command = New MySqlCommand(Query, Mysqlconn)
            Reader = Command.ExecuteReader
            While Reader.Read
                ID = Reader.GetString("ID_Number")
                Combobox_ID.Items.Add(ID)
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class