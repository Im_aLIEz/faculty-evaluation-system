﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SectionManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_Section = New System.Windows.Forms.DataGridView()
        Me.Button_Search = New System.Windows.Forms.Button()
        Me.Textbox_AddSection = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Combobox_Section = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button_BackAccount = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DGV_Section, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Section
        '
        Me.DGV_Section.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.DGV_Section.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Section.Location = New System.Drawing.Point(16, 89)
        Me.DGV_Section.Name = "DGV_Section"
        Me.DGV_Section.Size = New System.Drawing.Size(566, 311)
        Me.DGV_Section.TabIndex = 0
        '
        'Button_Search
        '
        Me.Button_Search.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_Search.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_Search.FlatAppearance.BorderSize = 0
        Me.Button_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_Search.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_Search.Location = New System.Drawing.Point(390, 50)
        Me.Button_Search.Name = "Button_Search"
        Me.Button_Search.Size = New System.Drawing.Size(75, 33)
        Me.Button_Search.TabIndex = 2
        Me.Button_Search.Text = "Search"
        Me.Button_Search.UseVisualStyleBackColor = False
        '
        'Textbox_AddSection
        '
        Me.Textbox_AddSection.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_AddSection.Location = New System.Drawing.Point(109, 422)
        Me.Textbox_AddSection.Name = "Textbox_AddSection"
        Me.Textbox_AddSection.Size = New System.Drawing.Size(202, 22)
        Me.Textbox_AddSection.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(50, 423)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 21)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Section:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(320, 417)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(101, 34)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Add Section"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Combobox_Section
        '
        Me.Combobox_Section.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Combobox_Section.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_Section.FormattingEnabled = True
        Me.Combobox_Section.Location = New System.Drawing.Point(182, 55)
        Me.Combobox_Section.Name = "Combobox_Section"
        Me.Combobox_Section.Size = New System.Drawing.Size(202, 25)
        Me.Combobox_Section.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(122, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 21)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Section:"
        '
        'Button_BackAccount
        '
        Me.Button_BackAccount.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_BackAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackAccount.FlatAppearance.BorderSize = 0
        Me.Button_BackAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackAccount.Location = New System.Drawing.Point(12, 12)
        Me.Button_BackAccount.Name = "Button_BackAccount"
        Me.Button_BackAccount.Size = New System.Drawing.Size(83, 29)
        Me.Button_BackAccount.TabIndex = 10
        Me.Button_BackAccount.Text = "Back"
        Me.Button_BackAccount.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(427, 417)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 34)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Delete Section"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'SectionManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 499)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button_BackAccount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Combobox_Section)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Textbox_AddSection)
        Me.Controls.Add(Me.Button_Search)
        Me.Controls.Add(Me.DGV_Section)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SectionManagementForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SectionManagementFom"
        CType(Me.DGV_Section, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Section As System.Windows.Forms.DataGridView
    Friend WithEvents Button_Search As System.Windows.Forms.Button
    Friend WithEvents Textbox_AddSection As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Combobox_Section As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button_BackAccount As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
