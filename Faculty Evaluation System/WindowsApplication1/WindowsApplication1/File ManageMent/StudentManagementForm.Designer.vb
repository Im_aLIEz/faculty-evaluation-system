﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StudentManagementForm))
        Me.DGV_Student = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Textbox_FNameStudent = New System.Windows.Forms.TextBox()
        Me.Textbox_LNameStudent = New System.Windows.Forms.TextBox()
        Me.Button_ClearStudent = New System.Windows.Forms.Button()
        Me.Button_EditStudent = New System.Windows.Forms.Button()
        Me.Button_DeleteStudent = New System.Windows.Forms.Button()
        Me.Button_AddStudent = New System.Windows.Forms.Button()
        Me.Button_BackStudent = New System.Windows.Forms.Button()
        Me.Combobox_IDNumberStudent = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Combobox_SectionStudent = New System.Windows.Forms.ComboBox()
        CType(Me.DGV_Student, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV_Student
        '
        Me.DGV_Student.AllowUserToAddRows = False
        Me.DGV_Student.AllowUserToDeleteRows = False
        Me.DGV_Student.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.DGV_Student.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Student.Location = New System.Drawing.Point(12, 57)
        Me.DGV_Student.Name = "DGV_Student"
        Me.DGV_Student.ReadOnly = True
        Me.DGV_Student.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Student.Size = New System.Drawing.Size(566, 311)
        Me.DGV_Student.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(94, 377)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 20)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "ID Number:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(95, 404)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 20)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "First Name:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(95, 431)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 20)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Last Name:"
        '
        'Textbox_FNameStudent
        '
        Me.Textbox_FNameStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_FNameStudent.Location = New System.Drawing.Point(175, 401)
        Me.Textbox_FNameStudent.Name = "Textbox_FNameStudent"
        Me.Textbox_FNameStudent.Size = New System.Drawing.Size(173, 22)
        Me.Textbox_FNameStudent.TabIndex = 24
        '
        'Textbox_LNameStudent
        '
        Me.Textbox_LNameStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Textbox_LNameStudent.Location = New System.Drawing.Point(175, 427)
        Me.Textbox_LNameStudent.Name = "Textbox_LNameStudent"
        Me.Textbox_LNameStudent.Size = New System.Drawing.Size(173, 22)
        Me.Textbox_LNameStudent.TabIndex = 25
        '
        'Button_ClearStudent
        '
        Me.Button_ClearStudent.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_ClearStudent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_ClearStudent.FlatAppearance.BorderSize = 0
        Me.Button_ClearStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_ClearStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_ClearStudent.Location = New System.Drawing.Point(363, 429)
        Me.Button_ClearStudent.Name = "Button_ClearStudent"
        Me.Button_ClearStudent.Size = New System.Drawing.Size(68, 46)
        Me.Button_ClearStudent.TabIndex = 30
        Me.Button_ClearStudent.Text = "Clear"
        Me.Button_ClearStudent.UseVisualStyleBackColor = False
        '
        'Button_EditStudent
        '
        Me.Button_EditStudent.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_EditStudent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_EditStudent.FlatAppearance.BorderSize = 0
        Me.Button_EditStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_EditStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_EditStudent.Location = New System.Drawing.Point(437, 374)
        Me.Button_EditStudent.Name = "Button_EditStudent"
        Me.Button_EditStudent.Size = New System.Drawing.Size(71, 49)
        Me.Button_EditStudent.TabIndex = 29
        Me.Button_EditStudent.Text = "Edit"
        Me.Button_EditStudent.UseVisualStyleBackColor = False
        '
        'Button_DeleteStudent
        '
        Me.Button_DeleteStudent.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_DeleteStudent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_DeleteStudent.FlatAppearance.BorderSize = 0
        Me.Button_DeleteStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeleteStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_DeleteStudent.Location = New System.Drawing.Point(437, 429)
        Me.Button_DeleteStudent.Name = "Button_DeleteStudent"
        Me.Button_DeleteStudent.Size = New System.Drawing.Size(71, 46)
        Me.Button_DeleteStudent.TabIndex = 28
        Me.Button_DeleteStudent.Text = "Delete"
        Me.Button_DeleteStudent.UseVisualStyleBackColor = False
        '
        'Button_AddStudent
        '
        Me.Button_AddStudent.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_AddStudent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_AddStudent.FlatAppearance.BorderSize = 0
        Me.Button_AddStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_AddStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_AddStudent.Location = New System.Drawing.Point(363, 374)
        Me.Button_AddStudent.Name = "Button_AddStudent"
        Me.Button_AddStudent.Size = New System.Drawing.Size(68, 49)
        Me.Button_AddStudent.TabIndex = 27
        Me.Button_AddStudent.Text = "Add"
        Me.Button_AddStudent.UseVisualStyleBackColor = False
        '
        'Button_BackStudent
        '
        Me.Button_BackStudent.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button_BackStudent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackStudent.FlatAppearance.BorderSize = 0
        Me.Button_BackStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackStudent.Location = New System.Drawing.Point(12, 12)
        Me.Button_BackStudent.Name = "Button_BackStudent"
        Me.Button_BackStudent.Size = New System.Drawing.Size(83, 29)
        Me.Button_BackStudent.TabIndex = 31
        Me.Button_BackStudent.Text = "Back"
        Me.Button_BackStudent.UseVisualStyleBackColor = False
        '
        'Combobox_IDNumberStudent
        '
        Me.Combobox_IDNumberStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_IDNumberStudent.FormattingEnabled = True
        Me.Combobox_IDNumberStudent.Location = New System.Drawing.Point(175, 374)
        Me.Combobox_IDNumberStudent.Name = "Combobox_IDNumberStudent"
        Me.Combobox_IDNumberStudent.Size = New System.Drawing.Size(173, 24)
        Me.Combobox_IDNumberStudent.TabIndex = 34
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(95, 457)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 20)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Section:"
        '
        'Combobox_SectionStudent
        '
        Me.Combobox_SectionStudent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_SectionStudent.FormattingEnabled = True
        Me.Combobox_SectionStudent.Location = New System.Drawing.Point(175, 453)
        Me.Combobox_SectionStudent.Name = "Combobox_SectionStudent"
        Me.Combobox_SectionStudent.Size = New System.Drawing.Size(173, 24)
        Me.Combobox_SectionStudent.TabIndex = 36
        '
        'StudentManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(594, 499)
        Me.Controls.Add(Me.Combobox_SectionStudent)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Combobox_IDNumberStudent)
        Me.Controls.Add(Me.Button_BackStudent)
        Me.Controls.Add(Me.Button_ClearStudent)
        Me.Controls.Add(Me.Button_EditStudent)
        Me.Controls.Add(Me.Button_DeleteStudent)
        Me.Controls.Add(Me.Button_AddStudent)
        Me.Controls.Add(Me.Textbox_LNameStudent)
        Me.Controls.Add(Me.Textbox_FNameStudent)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DGV_Student)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StudentManagementForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "StudentManagementForm"
        CType(Me.DGV_Student, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_Student As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Textbox_FNameStudent As System.Windows.Forms.TextBox
    Friend WithEvents Textbox_LNameStudent As System.Windows.Forms.TextBox
    Friend WithEvents Button_ClearStudent As System.Windows.Forms.Button
    Friend WithEvents Button_EditStudent As System.Windows.Forms.Button
    Friend WithEvents Button_DeleteStudent As System.Windows.Forms.Button
    Friend WithEvents Button_AddStudent As System.Windows.Forms.Button
    Friend WithEvents Button_BackStudent As System.Windows.Forms.Button
    Friend WithEvents Combobox_IDNumberStudent As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Combobox_SectionStudent As System.Windows.Forms.ComboBox
End Class
