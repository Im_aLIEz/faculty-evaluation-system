﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class PositionManagementForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand

    Private Sub Button_AddPosition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddPosition.Click
        Add()
        PositionDataGridView()
    End Sub

    Private Sub Button_DeletePosition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeletePosition.Click
        DeleteButton()
        PositionDataGridView()

    End Sub

    Private Sub Button_BackPosition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackPosition.Click
        AdminForm.Show()
        Me.Close()
    End Sub

    Private Sub PositionManagementForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PositionDataGridView()
    End Sub

    'method for add button'
    Sub Add()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "insert into dbcvs.tblposition (Position) values ('" & Textbox_Position.Text & "')"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for delete button'

    Sub DeleteButton()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Delete from dbcvs.tblposition where ID ='" & DGV_Position.SelectedRows(0).Cells(0).Value.ToString() & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader

            MessageBox.Show("Data Deleted")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try

    End Sub

    'method for data grid view'
    Sub PositionDataGridView()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim sqlDataAdapter As New MySqlDataAdapter
        Dim dt As New DataTable
        Dim bSource As New BindingSource

        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "select * from tblposition"
            command = New MySqlCommand(Query, Mysqlconn)
            sqlDataAdapter.SelectCommand = command
            sqlDataAdapter.Fill(dt)
            bSource.DataSource = dt
            DGV_Position.DataSource = bSource
            sqlDataAdapter.Update(dt)

            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        DGV_Position.Columns(0).Visible = False
    End Sub

    
    Private Sub DGV_Position_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_Position.CellContentClick

    End Sub
End Class