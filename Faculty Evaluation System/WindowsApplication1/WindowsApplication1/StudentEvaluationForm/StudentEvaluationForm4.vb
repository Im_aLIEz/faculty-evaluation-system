﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class StudentEvaluationForm4
    Public Rating_4_1 As Integer
    Public Rating_4_2 As Integer
    Public Rating_4_3 As Integer
    Public Rating_4_4 As Integer
    Public Rating_4_5 As Integer
    Dim Rating1_Total As Integer
    Dim Rating2_Total As Integer
    Dim Rating3_Total As Integer
    Dim Rating4_Total As Integer
    Dim evaluee As Integer
    Dim Get_MOL As Integer
    Dim Get_KOSM As Integer
    Dim Get_TFIL As Integer
    Dim Get_Commitment As Integer
    Dim command As MySqlCommand
    Dim Mysqlconn As MySqlConnection

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Rating4_Total = Rating_4_1 + Rating_4_2 + Rating_4_3 + Rating_4_4 + Rating_4_5


        If Rating1_Total = 0 Then
            MsgBox("Incomplete Values. Please Fill up every parameter to submit.")
        ElseIf Rating2_Total = 0 Then
            MsgBox("Incomplete Values. Please Fill up every parameter to submit.")
        ElseIf Rating3_Total = 0 Then
            MsgBox("Incomplete Values. Please Fill up every parameter to submit.")
        ElseIf Rating4_Total = 0 Then
            MsgBox("Incomplete Values. Please Fill up every parameter to submit.")
        Else
            Dump_Commitment()
            Dump_KOSM()
            Dump_MOL()
            Dump_TFIL()
            Update_Commitment()
            Update_KOSM()
            Update_MOL()
            Update_TFIL()
            GetEvaluee()
            setevaluee()
            MsgBox("Thank you for Participating in the Evaluation, the results will be subjected for Review.")
            StudentForm.Refresh()
            StudentForm.Show()
            StudentEvaluationForm1.Close()
            StudentEvaluationForm2.Close()
            StudentEvaluationForm3.Close()
            Me.Close()
        End If
    End Sub
    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        StudentEvaluationForm3.Show()
        Me.Hide()
    End Sub

    Private Sub StudentEvaluationForm4_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Rating1_Total = StudentEvaluationForm1.Rating_1_1 + StudentEvaluationForm1.Rating_1_2 + StudentEvaluationForm1.Rating_1_3 + StudentEvaluationForm1.Rating_1_4 + StudentEvaluationForm1.Rating_1_5
        Rating2_Total = StudentEvaluationForm2.Rating_2_1 + StudentEvaluationForm2.Rating_2_2 + StudentEvaluationForm2.Rating_2_3 + StudentEvaluationForm2.Rating_2_4 + StudentEvaluationForm2.Rating_2_5
        Rating3_Total = StudentEvaluationForm3.Rating_3_1 + StudentEvaluationForm3.Rating_3_2 + StudentEvaluationForm3.Rating_3_3 + StudentEvaluationForm3.Rating_3_4 + StudentEvaluationForm3.Rating_3_5
    End Sub

    Private Sub Radio4_1_5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1_5.CheckedChanged
        Rating_4_1 = 5
    End Sub

    Private Sub Radio4_1_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1_2.CheckedChanged
        Rating_4_1 = 2
    End Sub

    Private Sub Radio4_1_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1_3.CheckedChanged
        Rating_4_1 = 3
    End Sub

    Private Sub Radio4_1_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1_4.CheckedChanged
        Rating_4_1 = 4
    End Sub

    Private Sub Radio4_1_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1_1.CheckedChanged
        Rating_4_1 = 1
    End Sub

    Private Sub Radio4_2_5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2_5.CheckedChanged
        Rating_4_2 = 5
    End Sub

    Private Sub Radio4_2_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2_2.CheckedChanged
        Rating_4_2 = 2
    End Sub

    Private Sub Radio4_2_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2_3.CheckedChanged
        Rating_4_2 = 3
    End Sub

    Private Sub Radio4_2_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2_4.CheckedChanged
        Rating_4_2 = 4
    End Sub

    Private Sub Radio4_2_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2_1.CheckedChanged
        Rating_4_2 = 1
    End Sub

    Private Sub Radio4_3_5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3_5.CheckedChanged
        Rating_4_3 = 5
    End Sub

    Private Sub Radio4_3_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3_2.CheckedChanged
        Rating_4_3 = 2
    End Sub

    Private Sub Radio4_3_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3_3.CheckedChanged
        Rating_4_3 = 3
    End Sub

    Private Sub Radio4_3_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3_4.CheckedChanged
        Rating_4_3 = 4
    End Sub

    Private Sub Radio4_3_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3_1.CheckedChanged
        Rating_4_3 = 1
    End Sub

    Private Sub Radio4_4_5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4_5.CheckedChanged
        Rating_4_4 = 5
    End Sub

    Private Sub Radio4_4_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4_2.CheckedChanged
        Rating_4_4 = 2
    End Sub

    Private Sub Radio4_4_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4_3.CheckedChanged
        Rating_4_4 = 3
    End Sub

    Private Sub Radio4_4_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4_4.CheckedChanged
        Rating_4_4 = 4
    End Sub

    Private Sub Radio4_4_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4_1.CheckedChanged
        Rating_4_4 = 1
    End Sub

    Private Sub Radio4_5_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_5_1.CheckedChanged
        Rating_4_5 = 1
    End Sub

    Private Sub Radio4_5_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_5_2.CheckedChanged
        Rating_4_5 = 2
    End Sub

    Private Sub Radio4_5_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_5_3.CheckedChanged
        Rating_4_5 = 3
    End Sub

    Private Sub Radio4_5_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_5_4.CheckedChanged
        Rating_4_5 = 4
    End Sub

    Private Sub Radio4_5_5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_5_5.CheckedChanged
        Rating_4_5 = 5
    End Sub

    Sub GetEvaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblfaculty Where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                evaluee = Reader.GetInt32("Evaluee_std")
            End While
            evaluee = evaluee + 1
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub setevaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblfaculty set evaluee_std='" & evaluee & "' where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub Dump_Commitment()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_commitment Where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_Commitment = Reader.GetInt32("Std_CommitmentDump")
            End While
            Rating1_Total = Get_Commitment + Rating1_Total
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_MOL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_MOL Where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_MOL = Reader.GetInt32("Std_MOLDump")
            End While
            Rating4_Total = Get_MOL + Rating4_Total
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_KOSM()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_KOSM Where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_KOSM = Reader.GetInt32("Std_kosmDump")
            End While
            Rating2_Total = Get_KOSM + Rating2_Total
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_TFIL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_TFIL Where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_TFIL = Reader.GetInt32("Std_TFILDump")
            End While
            Rating3_Total = Get_TFIL + Rating3_Total
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_Commitment()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultstd_commitment set std_commitmentDump='" & Rating1_Total & "' where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_MOL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultstd_mol set std_molDump='" & Rating4_Total & "' where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_KOSM()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultstd_kosm set std_kosmDump='" & Rating2_Total & "' where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_TFIL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultstd_tfil set std_tfilDump='" & Rating3_Total & "' where ID_Number = '" & StudentForm.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class