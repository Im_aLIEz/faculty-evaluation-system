﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentEvaluationForm1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Groupbox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Radio1_5_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_5_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_5_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_5_3 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Radio1_5_5 = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Radio1_4_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_4_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_4_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_4_3 = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Radio1_4_5 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Radio1_1_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_1_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_1_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_1_3 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Radio1_1_5 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Radio1_2_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_2_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_2_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_2_3 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Radio1_2_5 = New System.Windows.Forms.RadioButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Radio1_3_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_3_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_3_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_3_3 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Radio1_3_5 = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Groupbox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.SandyBrown
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(923, 571)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 29)
        Me.Button1.TabIndex = 38
        Me.Button1.Text = "Next"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Groupbox1
        '
        Me.Groupbox1.BackColor = System.Drawing.Color.Transparent
        Me.Groupbox1.Controls.Add(Me.GroupBox6)
        Me.Groupbox1.Controls.Add(Me.GroupBox4)
        Me.Groupbox1.Controls.Add(Me.GroupBox3)
        Me.Groupbox1.Controls.Add(Me.GroupBox2)
        Me.Groupbox1.Controls.Add(Me.Label7)
        Me.Groupbox1.Controls.Add(Me.GroupBox5)
        Me.Groupbox1.Controls.Add(Me.Label6)
        Me.Groupbox1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Groupbox1.Location = New System.Drawing.Point(160, 69)
        Me.Groupbox1.Name = "Groupbox1"
        Me.Groupbox1.Size = New System.Drawing.Size(704, 438)
        Me.Groupbox1.TabIndex = 37
        Me.Groupbox1.TabStop = False
        Me.Groupbox1.Text = "1. Commitment"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Radio1_5_2)
        Me.GroupBox6.Controls.Add(Me.Radio1_5_4)
        Me.GroupBox6.Controls.Add(Me.Radio1_5_1)
        Me.GroupBox6.Controls.Add(Me.Radio1_5_3)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.Radio1_5_5)
        Me.GroupBox6.Location = New System.Drawing.Point(9, 350)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(689, 66)
        Me.GroupBox6.TabIndex = 39
        Me.GroupBox6.TabStop = False
        '
        'Radio1_5_2
        '
        Me.Radio1_5_2.AutoSize = True
        Me.Radio1_5_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_5_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_5_2.Location = New System.Drawing.Point(374, 38)
        Me.Radio1_5_2.Name = "Radio1_5_2"
        Me.Radio1_5_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_5_2.TabIndex = 1
        Me.Radio1_5_2.Text = "2"
        Me.Radio1_5_2.UseVisualStyleBackColor = True
        '
        'Radio1_5_4
        '
        Me.Radio1_5_4.AutoSize = True
        Me.Radio1_5_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_5_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_5_4.Location = New System.Drawing.Point(300, 38)
        Me.Radio1_5_4.Name = "Radio1_5_4"
        Me.Radio1_5_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_5_4.TabIndex = 3
        Me.Radio1_5_4.Text = "4"
        Me.Radio1_5_4.UseVisualStyleBackColor = True
        '
        'Radio1_5_1
        '
        Me.Radio1_5_1.AutoSize = True
        Me.Radio1_5_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_5_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_5_1.Location = New System.Drawing.Point(411, 38)
        Me.Radio1_5_1.Name = "Radio1_5_1"
        Me.Radio1_5_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_5_1.TabIndex = 0
        Me.Radio1_5_1.Text = "1"
        Me.Radio1_5_1.UseVisualStyleBackColor = True
        '
        'Radio1_5_3
        '
        Me.Radio1_5_3.AutoSize = True
        Me.Radio1_5_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_5_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_5_3.Location = New System.Drawing.Point(337, 38)
        Me.Radio1_5_3.Name = "Radio1_5_3"
        Me.Radio1_5_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_5_3.TabIndex = 2
        Me.Radio1_5_3.Text = "3"
        Me.Radio1_5_3.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(187, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(357, 20)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "1.5 Keeps accurate records of student's performance."
        '
        'Radio1_5_5
        '
        Me.Radio1_5_5.AutoSize = True
        Me.Radio1_5_5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_5_5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_5_5.Location = New System.Drawing.Point(263, 38)
        Me.Radio1_5_5.Name = "Radio1_5_5"
        Me.Radio1_5_5.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_5_5.TabIndex = 4
        Me.Radio1_5_5.Text = "5"
        Me.Radio1_5_5.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Radio1_4_2)
        Me.GroupBox4.Controls.Add(Me.Radio1_4_4)
        Me.GroupBox4.Controls.Add(Me.Radio1_4_1)
        Me.GroupBox4.Controls.Add(Me.Radio1_4_3)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.Radio1_4_5)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 278)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(689, 66)
        Me.GroupBox4.TabIndex = 38
        Me.GroupBox4.TabStop = False
        '
        'Radio1_4_2
        '
        Me.Radio1_4_2.AutoSize = True
        Me.Radio1_4_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4_2.Location = New System.Drawing.Point(374, 38)
        Me.Radio1_4_2.Name = "Radio1_4_2"
        Me.Radio1_4_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4_2.TabIndex = 1
        Me.Radio1_4_2.Text = "2"
        Me.Radio1_4_2.UseVisualStyleBackColor = True
        '
        'Radio1_4_4
        '
        Me.Radio1_4_4.AutoSize = True
        Me.Radio1_4_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4_4.Location = New System.Drawing.Point(300, 38)
        Me.Radio1_4_4.Name = "Radio1_4_4"
        Me.Radio1_4_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4_4.TabIndex = 3
        Me.Radio1_4_4.Text = "4"
        Me.Radio1_4_4.UseVisualStyleBackColor = True
        '
        'Radio1_4_1
        '
        Me.Radio1_4_1.AutoSize = True
        Me.Radio1_4_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4_1.Location = New System.Drawing.Point(411, 38)
        Me.Radio1_4_1.Name = "Radio1_4_1"
        Me.Radio1_4_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4_1.TabIndex = 0
        Me.Radio1_4_1.Text = "1"
        Me.Radio1_4_1.UseVisualStyleBackColor = True
        '
        'Radio1_4_3
        '
        Me.Radio1_4_3.AutoSize = True
        Me.Radio1_4_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4_3.Location = New System.Drawing.Point(337, 38)
        Me.Radio1_4_3.Name = "Radio1_4_3"
        Me.Radio1_4_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4_3.TabIndex = 2
        Me.Radio1_4_3.Text = "3"
        Me.Radio1_4_3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(157, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(401, 20)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "1.4 Regularly comes to class well-groomed and well prepared."
        '
        'Radio1_4_5
        '
        Me.Radio1_4_5.AutoSize = True
        Me.Radio1_4_5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4_5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4_5.Location = New System.Drawing.Point(263, 38)
        Me.Radio1_4_5.Name = "Radio1_4_5"
        Me.Radio1_4_5.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4_5.TabIndex = 4
        Me.Radio1_4_5.Text = "5"
        Me.Radio1_4_5.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Radio1_1_2)
        Me.GroupBox3.Controls.Add(Me.Radio1_1_4)
        Me.GroupBox3.Controls.Add(Me.Radio1_1_1)
        Me.GroupBox3.Controls.Add(Me.Radio1_1_3)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Radio1_1_5)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 59)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(689, 66)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = False
        '
        'Radio1_1_2
        '
        Me.Radio1_1_2.AutoSize = True
        Me.Radio1_1_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1_2.Location = New System.Drawing.Point(374, 35)
        Me.Radio1_1_2.Name = "Radio1_1_2"
        Me.Radio1_1_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1_2.TabIndex = 1
        Me.Radio1_1_2.Text = "2"
        Me.Radio1_1_2.UseVisualStyleBackColor = True
        '
        'Radio1_1_4
        '
        Me.Radio1_1_4.AutoSize = True
        Me.Radio1_1_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1_4.Location = New System.Drawing.Point(300, 35)
        Me.Radio1_1_4.Name = "Radio1_1_4"
        Me.Radio1_1_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1_4.TabIndex = 3
        Me.Radio1_1_4.Text = "4"
        Me.Radio1_1_4.UseVisualStyleBackColor = True
        '
        'Radio1_1_1
        '
        Me.Radio1_1_1.AutoSize = True
        Me.Radio1_1_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1_1.Location = New System.Drawing.Point(411, 35)
        Me.Radio1_1_1.Name = "Radio1_1_1"
        Me.Radio1_1_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1_1.TabIndex = 0
        Me.Radio1_1_1.Text = "1"
        Me.Radio1_1_1.UseVisualStyleBackColor = True
        '
        'Radio1_1_3
        '
        Me.Radio1_1_3.AutoSize = True
        Me.Radio1_1_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1_3.Location = New System.Drawing.Point(337, 35)
        Me.Radio1_1_3.Name = "Radio1_1_3"
        Me.Radio1_1_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1_3.TabIndex = 2
        Me.Radio1_1_3.Text = "3"
        Me.Radio1_1_3.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label2.Location = New System.Drawing.Point(109, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(511, 20)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "1.1 Orients the class on teacher and students roles at the start of the session."
        '
        'Radio1_1_5
        '
        Me.Radio1_1_5.AutoSize = True
        Me.Radio1_1_5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1_5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1_5.Location = New System.Drawing.Point(263, 35)
        Me.Radio1_1_5.Name = "Radio1_1_5"
        Me.Radio1_1_5.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1_5.TabIndex = 4
        Me.Radio1_1_5.Text = "5"
        Me.Radio1_1_5.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Radio1_2_2)
        Me.GroupBox2.Controls.Add(Me.Radio1_2_4)
        Me.GroupBox2.Controls.Add(Me.Radio1_2_1)
        Me.GroupBox2.Controls.Add(Me.Radio1_2_3)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Radio1_2_5)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 134)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(689, 66)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        '
        'Radio1_2_2
        '
        Me.Radio1_2_2.AutoSize = True
        Me.Radio1_2_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2_2.Location = New System.Drawing.Point(374, 38)
        Me.Radio1_2_2.Name = "Radio1_2_2"
        Me.Radio1_2_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2_2.TabIndex = 1
        Me.Radio1_2_2.Text = "2"
        Me.Radio1_2_2.UseVisualStyleBackColor = True
        '
        'Radio1_2_4
        '
        Me.Radio1_2_4.AutoSize = True
        Me.Radio1_2_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2_4.Location = New System.Drawing.Point(300, 38)
        Me.Radio1_2_4.Name = "Radio1_2_4"
        Me.Radio1_2_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2_4.TabIndex = 3
        Me.Radio1_2_4.Text = "4"
        Me.Radio1_2_4.UseVisualStyleBackColor = True
        '
        'Radio1_2_1
        '
        Me.Radio1_2_1.AutoSize = True
        Me.Radio1_2_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2_1.Location = New System.Drawing.Point(411, 38)
        Me.Radio1_2_1.Name = "Radio1_2_1"
        Me.Radio1_2_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2_1.TabIndex = 0
        Me.Radio1_2_1.Text = "1"
        Me.Radio1_2_1.UseVisualStyleBackColor = True
        '
        'Radio1_2_3
        '
        Me.Radio1_2_3.AutoSize = True
        Me.Radio1_2_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2_3.Location = New System.Drawing.Point(337, 38)
        Me.Radio1_2_3.Name = "Radio1_2_3"
        Me.Radio1_2_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2_3.TabIndex = 2
        Me.Radio1_2_3.Text = "3"
        Me.Radio1_2_3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(187, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(377, 20)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "1.2 Show enthusiasm and positive attitude towards work."
        '
        'Radio1_2_5
        '
        Me.Radio1_2_5.AutoSize = True
        Me.Radio1_2_5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2_5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2_5.Location = New System.Drawing.Point(263, 38)
        Me.Radio1_2_5.Name = "Radio1_2_5"
        Me.Radio1_2_5.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2_5.TabIndex = 4
        Me.Radio1_2_5.Text = "5"
        Me.Radio1_2_5.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(34, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(382, 21)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "student's well-being and the advancement of his/her discipline."
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Radio1_3_2)
        Me.GroupBox5.Controls.Add(Me.Radio1_3_4)
        Me.GroupBox5.Controls.Add(Me.Radio1_3_1)
        Me.GroupBox5.Controls.Add(Me.Radio1_3_3)
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.Radio1_3_5)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 206)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(689, 66)
        Me.GroupBox5.TabIndex = 34
        Me.GroupBox5.TabStop = False
        '
        'Radio1_3_2
        '
        Me.Radio1_3_2.AutoSize = True
        Me.Radio1_3_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3_2.Location = New System.Drawing.Point(374, 38)
        Me.Radio1_3_2.Name = "Radio1_3_2"
        Me.Radio1_3_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3_2.TabIndex = 1
        Me.Radio1_3_2.Text = "2"
        Me.Radio1_3_2.UseVisualStyleBackColor = True
        '
        'Radio1_3_4
        '
        Me.Radio1_3_4.AutoSize = True
        Me.Radio1_3_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3_4.Location = New System.Drawing.Point(300, 38)
        Me.Radio1_3_4.Name = "Radio1_3_4"
        Me.Radio1_3_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3_4.TabIndex = 3
        Me.Radio1_3_4.Text = "4"
        Me.Radio1_3_4.UseVisualStyleBackColor = True
        '
        'Radio1_3_1
        '
        Me.Radio1_3_1.AutoSize = True
        Me.Radio1_3_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3_1.Location = New System.Drawing.Point(411, 38)
        Me.Radio1_3_1.Name = "Radio1_3_1"
        Me.Radio1_3_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3_1.TabIndex = 0
        Me.Radio1_3_1.Text = "1"
        Me.Radio1_3_1.UseVisualStyleBackColor = True
        '
        'Radio1_3_3
        '
        Me.Radio1_3_3.AutoSize = True
        Me.Radio1_3_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3_3.Location = New System.Drawing.Point(337, 38)
        Me.Radio1_3_3.Name = "Radio1_3_3"
        Me.Radio1_3_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3_3.TabIndex = 2
        Me.Radio1_3_3.Text = "3"
        Me.Radio1_3_3.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(50, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(618, 20)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "1.3 Allows flexibility to meet for a specific needs, interest and capabilities of" & _
    " individual students."
        '
        'Radio1_3_5
        '
        Me.Radio1_3_5.AutoSize = True
        Me.Radio1_3_5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3_5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3_5.Location = New System.Drawing.Point(263, 38)
        Me.Radio1_3_5.Name = "Radio1_3_5"
        Me.Radio1_3_5.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3_5.TabIndex = 4
        Me.Radio1_3_5.Text = "5"
        Me.Radio1_3_5.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(31, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(629, 21)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "This refers to the faculty member's deep sense of responsibility to render servic" & _
    "es for the development of the" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.SandyBrown
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(12, 571)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 29)
        Me.Button2.TabIndex = 52
        Me.Button2.Text = "Previous"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(476, 565)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 34)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "1 of 4"
        '
        'StudentEvaluationForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Groupbox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StudentEvaluationForm1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "StudentEvaluationForm"
        Me.Groupbox1.ResumeLayout(False)
        Me.Groupbox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Groupbox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_3_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_3_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_3_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_3_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Radio1_3_5 As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_5_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_5_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_5_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_5_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Radio1_5_5 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_4_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_4_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_4_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_4_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Radio1_4_5 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_1_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_1_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_1_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_1_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Radio1_1_5 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_2_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_2_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_2_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_2_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Radio1_2_5 As System.Windows.Forms.RadioButton
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
