﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class FacultyForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim command2 As MySqlCommand
    Dim UserLogged As String
    Public Username As String
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ResultForm.Show()
        Me.Hide()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FacultyEvaluationSelection.Show()
        Me.Hide()

    End Sub

    Private Sub Logout_Admin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Logout_Admin.Click
        LoginForm.Show()
        Me.Close()
        FacultyEvaluationSelection.Close()
        FacultyPeerEvaluationSelection.Close()
        ResultForm.Close()

        LoginForm.ID_Box.Text = ""
        LoginForm.Password_box.Text = ""

    End Sub

    Private Sub FacultyForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Username = LoginForm.Username
        facultyName()
    End Sub

    Sub facultyName()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty where ID_Number = '" & Username & "'"
            Command = New MySqlCommand(query, Mysqlconn)
            Reader = Command.ExecuteReader
            While Reader.Read
                Dim Fname = Reader.GetString("First_Name")
                Dim LName = Reader.GetString("Last_Name")
                LblID.Text = (Fname + " " + LName)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class