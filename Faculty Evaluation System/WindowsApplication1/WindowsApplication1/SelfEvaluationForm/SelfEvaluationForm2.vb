﻿Public Class SelfEvaluationForm2
    Public Username As String
    Public Parameter_iaj As Integer
    Public Parameter_responsibility As Integer
    Public Parameter_es As Integer
    Public Parameter_adaptability As Integer
    Public Parameter_cat As Integer
    Public Parameter_respect As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SelfEvaluationForm3.Show()
        Me.Hide()
    End Sub
    Private Sub Radio4_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1.CheckedChanged
        Parameter_adaptability = 1
    End Sub
    Private Sub Radio4_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2.CheckedChanged
        Parameter_adaptability = 2
    End Sub
    Private Sub Radio4_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3.CheckedChanged
        Parameter_adaptability = 3
    End Sub
    Private Sub Radio4_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4.CheckedChanged
        Parameter_adaptability = 4
    End Sub
    Private Sub Radio6_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_1.CheckedChanged
        Parameter_respect = 1
    End Sub
    Private Sub Radio6_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_2.CheckedChanged
        Parameter_respect = 2
    End Sub
    Private Sub Radio6_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_3.CheckedChanged
        Parameter_respect = 3
    End Sub
    Private Sub Radio6_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_4.CheckedChanged
        Parameter_respect = 4
    End Sub
    Private Sub Radio5_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_1.CheckedChanged
        Parameter_cat = 1
    End Sub
    Private Sub Radio5_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_2.CheckedChanged
        Parameter_cat = 2
    End Sub
    Private Sub Radio5_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_3.CheckedChanged
        Parameter_cat = 3
    End Sub
    Private Sub Radio5_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_4.CheckedChanged
        Parameter_cat = 4
    End Sub

    Private Sub SelfEvaluationForm2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Username = SelfEvaluationForm1.Username
        Parameter_iaj = SelfEvaluationForm1.Parameter_iaj
        Parameter_responsibility = SelfEvaluationForm1.Parameter_responsibility
        Parameter_es = SelfEvaluationForm1.Parameter_es
        'MessageBox.Show(Parameter_iaj)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        SelfEvaluationForm1.Show()
        Me.Hide()
    End Sub
End Class