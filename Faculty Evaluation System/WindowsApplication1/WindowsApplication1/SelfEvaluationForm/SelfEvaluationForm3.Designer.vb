﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelfEvaluationForm3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelfEvaluationForm3))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Radio9_1 = New System.Windows.Forms.RadioButton()
        Me.Radio9_2 = New System.Windows.Forms.RadioButton()
        Me.Radio9_4 = New System.Windows.Forms.RadioButton()
        Me.Radio9_3 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Radio8_1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Radio8_2 = New System.Windows.Forms.RadioButton()
        Me.Radio8_4 = New System.Windows.Forms.RadioButton()
        Me.Radio8_3 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Radio7_1 = New System.Windows.Forms.RadioButton()
        Me.Radio7_2 = New System.Windows.Forms.RadioButton()
        Me.Radio7_4 = New System.Windows.Forms.RadioButton()
        Me.Radio7_3 = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Radio9_1)
        Me.GroupBox3.Controls.Add(Me.Radio9_2)
        Me.GroupBox3.Controls.Add(Me.Radio9_4)
        Me.GroupBox3.Controls.Add(Me.Radio9_3)
        Me.GroupBox3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(37, 369)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(942, 155)
        Me.GroupBox3.TabIndex = 46
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Punctuality ( Arriving at tand starting work on time )"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(73, 111)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(196, 20)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "- Rarely arrives on time (50%)"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(73, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(234, 20)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "- Occasionally arrives on time (75%)"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(73, 65)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(216, 20)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "- Generally arrives on time (90%)"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(71, 40)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(111, 20)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "- Always on time"
        '
        'Radio9_1
        '
        Me.Radio9_1.AutoSize = True
        Me.Radio9_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio9_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio9_1.Location = New System.Drawing.Point(36, 109)
        Me.Radio9_1.Name = "Radio9_1"
        Me.Radio9_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio9_1.TabIndex = 0
        Me.Radio9_1.Text = "1"
        Me.Radio9_1.UseVisualStyleBackColor = True
        '
        'Radio9_2
        '
        Me.Radio9_2.AutoSize = True
        Me.Radio9_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio9_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio9_2.Location = New System.Drawing.Point(36, 86)
        Me.Radio9_2.Name = "Radio9_2"
        Me.Radio9_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio9_2.TabIndex = 1
        Me.Radio9_2.Text = "2"
        Me.Radio9_2.UseVisualStyleBackColor = True
        '
        'Radio9_4
        '
        Me.Radio9_4.AutoSize = True
        Me.Radio9_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio9_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio9_4.Location = New System.Drawing.Point(37, 40)
        Me.Radio9_4.Name = "Radio9_4"
        Me.Radio9_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio9_4.TabIndex = 3
        Me.Radio9_4.Text = "4"
        Me.Radio9_4.UseVisualStyleBackColor = True
        '
        'Radio9_3
        '
        Me.Radio9_3.AutoSize = True
        Me.Radio9_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio9_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio9_3.Location = New System.Drawing.Point(36, 63)
        Me.Radio9_3.Name = "Radio9_3"
        Me.Radio9_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio9_3.TabIndex = 2
        Me.Radio9_3.Text = "3"
        Me.Radio9_3.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(73, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(596, 20)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "- Less than 75% of the time provides utmost service and courtesy to clients and c" & _
    "o-workers." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(73, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(843, 40)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "- At least 75% of the time provides utmost service and courtesy to clients and co" & _
    "-workers. Sometimes encourages others to do the" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "same and adhere to professional" & _
    " ethics and work standards."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(71, 67)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(842, 40)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "- Commonly provides utmost service and courtesy to clients and co-workers. Usuall" & _
    "y encourages others to do the same and adhere" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " to prefessional ethics and work " & _
    "standards." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(73, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(787, 40)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "- Always provides utmost service and courtesy to clients and co-workers. Encourag" & _
    "es others to do the same and adhere to" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " professional ethics and work standards." & _
    ""
        '
        'Radio8_1
        '
        Me.Radio8_1.AutoSize = True
        Me.Radio8_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio8_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio8_1.Location = New System.Drawing.Point(36, 140)
        Me.Radio8_1.Name = "Radio8_1"
        Me.Radio8_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio8_1.TabIndex = 0
        Me.Radio8_1.Text = "1"
        Me.Radio8_1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Radio8_1)
        Me.GroupBox2.Controls.Add(Me.Radio8_2)
        Me.GroupBox2.Controls.Add(Me.Radio8_4)
        Me.GroupBox2.Controls.Add(Me.Radio8_3)
        Me.GroupBox2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(37, 192)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(942, 171)
        Me.GroupBox2.TabIndex = 45
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Customer Relations ( Commitment to serve the customers in accordance to professio" & _
    "nal ethics and work standards "
        '
        'Radio8_2
        '
        Me.Radio8_2.AutoSize = True
        Me.Radio8_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio8_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio8_2.Location = New System.Drawing.Point(36, 101)
        Me.Radio8_2.Name = "Radio8_2"
        Me.Radio8_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio8_2.TabIndex = 1
        Me.Radio8_2.Text = "2"
        Me.Radio8_2.UseVisualStyleBackColor = True
        '
        'Radio8_4
        '
        Me.Radio8_4.AutoSize = True
        Me.Radio8_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio8_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio8_4.Location = New System.Drawing.Point(36, 28)
        Me.Radio8_4.Name = "Radio8_4"
        Me.Radio8_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio8_4.TabIndex = 3
        Me.Radio8_4.Text = "4"
        Me.Radio8_4.UseVisualStyleBackColor = True
        '
        'Radio8_3
        '
        Me.Radio8_3.AutoSize = True
        Me.Radio8_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio8_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio8_3.Location = New System.Drawing.Point(36, 65)
        Me.Radio8_3.Name = "Radio8_3"
        Me.Radio8_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio8_3.TabIndex = 2
        Me.Radio8_3.Text = "3"
        Me.Radio8_3.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(73, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(413, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "- Actively involved in few (less than 50% of) extra work activities"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(363, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "- Actively involved in some (50% of) extra work activities"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(360, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "- Actively involved in most (75% of) extra work activities"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(73, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(284, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "- Actively involved in all extra-work activities"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Radio7_1)
        Me.GroupBox1.Controls.Add(Me.Radio7_2)
        Me.GroupBox1.Controls.Add(Me.Radio7_4)
        Me.GroupBox1.Controls.Add(Me.Radio7_3)
        Me.GroupBox1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(37, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(942, 155)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = resources.GetString("GroupBox1.Text")
        '
        'Radio7_1
        '
        Me.Radio7_1.AutoSize = True
        Me.Radio7_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio7_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio7_1.Location = New System.Drawing.Point(36, 113)
        Me.Radio7_1.Name = "Radio7_1"
        Me.Radio7_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio7_1.TabIndex = 0
        Me.Radio7_1.Text = "1"
        Me.Radio7_1.UseVisualStyleBackColor = True
        '
        'Radio7_2
        '
        Me.Radio7_2.AutoSize = True
        Me.Radio7_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio7_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio7_2.Location = New System.Drawing.Point(36, 90)
        Me.Radio7_2.Name = "Radio7_2"
        Me.Radio7_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio7_2.TabIndex = 1
        Me.Radio7_2.Text = "2"
        Me.Radio7_2.UseVisualStyleBackColor = True
        '
        'Radio7_4
        '
        Me.Radio7_4.AutoSize = True
        Me.Radio7_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio7_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio7_4.Location = New System.Drawing.Point(36, 44)
        Me.Radio7_4.Name = "Radio7_4"
        Me.Radio7_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio7_4.TabIndex = 3
        Me.Radio7_4.Text = "4"
        Me.Radio7_4.UseVisualStyleBackColor = True
        '
        'Radio7_3
        '
        Me.Radio7_3.AutoSize = True
        Me.Radio7_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio7_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio7_3.Location = New System.Drawing.Point(36, 67)
        Me.Radio7_3.Name = "Radio7_3"
        Me.Radio7_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio7_3.TabIndex = 2
        Me.Radio7_3.Text = "3"
        Me.Radio7_3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.SandyBrown
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(923, 571)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 29)
        Me.Button1.TabIndex = 47
        Me.Button1.Text = "Next"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.SandyBrown
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(12, 571)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 29)
        Me.Button2.TabIndex = 49
        Me.Button2.Text = "Previous"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(476, 565)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 34)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "3 of 4"
        '
        'SelfEvaluationForm3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SelfEvaluationForm3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SelfEvaluationForm3"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Radio9_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio9_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio9_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio9_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Radio8_1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio8_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio8_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio8_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio7_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio7_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio7_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio7_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
End Class
