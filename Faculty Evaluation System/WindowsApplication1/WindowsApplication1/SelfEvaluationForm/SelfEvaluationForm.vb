﻿Public Class SelfEvaluationForm1
    Public Username As String
    Public Parameter_iaj As Integer
    Public Parameter_responsibility As Integer
    Public Parameter_es As Integer

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SelfEvaluationForm2.Show()
        Me.Hide()
    End Sub
    Private Sub Radio1_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio1_4.CheckedChanged
        Parameter_iaj = 4
    End Sub
    Private Sub Radio1_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio1_1.CheckedChanged
        Parameter_iaj = 1
    End Sub
    Private Sub Radio1_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio1_2.CheckedChanged
        Parameter_iaj = 2
    End Sub
    Private Sub Radio1_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio1_3.CheckedChanged
        Parameter_iaj = 3
    End Sub
    Private Sub Radio2_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio2_1.CheckedChanged
        Parameter_responsibility = 1
    End Sub
    Private Sub Radio2_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio2_2.CheckedChanged
        Parameter_responsibility = 2
    End Sub
    Private Sub Radio2_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio2_3.CheckedChanged
        Parameter_responsibility = 3
    End Sub
    Private Sub Radio2_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio2_4.CheckedChanged
        Parameter_responsibility = 4
    End Sub
    Private Sub Radio3_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio3_1.CheckedChanged
        Parameter_es = 1
    End Sub
    Private Sub Radio3_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio3_2.CheckedChanged
        Parameter_es = 2
    End Sub
    Private Sub Radio3_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio3_3.CheckedChanged
        Parameter_es = 3
    End Sub
    Private Sub Radio3_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio3_4.CheckedChanged
        Parameter_es = 4
    End Sub

    Private Sub SelfEvaluationForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Username = FacultyEvaluationSelection.Username
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FacultyEvaluationSelection.Show()
        SelfEvaluationForm2.Close()
        SelfEvaluationForm3.Close()
        SelfEvaluationForm4.Close()
        Me.Close()
    End Sub
End Class