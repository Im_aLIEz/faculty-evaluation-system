﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class SelfEvaluationForm4
    Public Username As String
    Public Parameter_iaj As Integer
    Public Parameter_responsibility As Integer
    Public Parameter_es As Integer
    Public Parameter_adaptability As Integer
    Public Parameter_cat As Integer
    Public Parameter_respect As Integer
    Public Parameter_ec As Integer
    Public Parameter_cr As Integer
    Public Parameter_punctuality As Integer
    Public Parameter_mc_atl As Integer
    Public Parameter_da As Integer
    Public Parameter_pa_cah As Integer
    Dim EvalueeSet As Integer
    Dim Evaluee As Integer
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim P_iaj As Integer
    '  Dim T_iaj As Double
    Dim P_responsibility As Integer
    '  Dim t_responsibility As Double
    Dim P_es As Integer
    '   Dim t_es As Double
    Dim P_adaptability As Integer
    '   Dim t_adaptability As Double
    Dim P_cat As Integer
    '   Dim t_cat As Double
    Dim P_respect As Integer
    '   Dim t_respect As Double
    Dim P_ec As Integer
    '  Dim t_ec As Double
    Dim P_cr As Integer
    ' Dim t_cr As Double
    Dim P_punctuality As Integer
    '  Dim t_punctuality As Double
    Dim P_mc_atl As Integer
    '  Dim t_mc_atl As Double
    Dim P_da As Integer
    '  Dim t_da As Double
    Dim P_pa_cah As Integer
    '  Dim t_pa_cah As Double


    Private Sub Radio12_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_1.CheckedChanged
        Parameter_pa_cah = 1
    End Sub
    Private Sub Radio12_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_2.CheckedChanged
        Parameter_pa_cah = 2
    End Sub
    Private Sub Radio12_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_3.CheckedChanged
        Parameter_pa_cah = 3
    End Sub
    Private Sub Radio12_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_4.CheckedChanged
        Parameter_pa_cah = 4
    End Sub
    Private Sub Radio11_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_1.CheckedChanged
        Parameter_da = 1
    End Sub
    Private Sub Radio11_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_2.CheckedChanged
        Parameter_da = 2
    End Sub
    Private Sub Radio11_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_3.CheckedChanged
        Parameter_da = 3
    End Sub

    Private Sub Radio11_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_4.CheckedChanged
        Parameter_da = 4
    End Sub

    Private Sub Radio10_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_1.CheckedChanged
        Parameter_mc_atl = 1
    End Sub

    Private Sub Radio10_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_2.CheckedChanged
        Parameter_mc_atl = 2
    End Sub

    Private Sub Radio10_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_3.CheckedChanged
        Parameter_mc_atl = 3
    End Sub

    Private Sub Radio10_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_4.CheckedChanged
        Parameter_mc_atl = 4
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        EvalueeSet = 1
        Parameter_iaj = SelfEvaluationForm3.Parameter_iaj
        Parameter_responsibility = SelfEvaluationForm3.Parameter_responsibility
        Parameter_es = SelfEvaluationForm3.Parameter_es
        Parameter_adaptability = SelfEvaluationForm3.Parameter_adaptability
        Parameter_cat = SelfEvaluationForm3.Parameter_cat
        Parameter_respect = SelfEvaluationForm3.Parameter_respect
        Parameter_ec = SelfEvaluationForm3.Parameter_ec
        Parameter_cr = SelfEvaluationForm3.Parameter_cr
        Parameter_punctuality = SelfEvaluationForm3.Parameter_punctuality
        GetEvaluee()
        setevaluee()
        Dump_iaj()
        Dump_responsibility()
        Dump_es()
        Dump_adaptability()
        Dump_cat()
        Dump_respect()
        Dump_ec()
        Dump_cr()
        Dump_punctuality()
        Dump_mc_atl()
        Dump_da()
        Dump_pa_cah()
        Update_iaj()
        Update_responsibility()
        Update_es()
        Update_adaptability()
        Update_cat()
        Update_respect()
        Update_ec()
        Update_cr()
        Update_punctuality()
        Update_mc_atl()
        Update_da()
        Update_pa_cah()

        MsgBox("Thank you for Participating in the Evaluation, the results will be subjected for Review.")
        FacultyForm.Refresh()
        FacultyEvaluationSelection.Close()
        FacultyPeerEvaluationSelection.Close()
        ResultForm.Close()
        StudentEvaluationResult.Close()
        FacultyForm.Show()
        Me.Close()
    End Sub
    Sub GetEvaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblfaculty Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Evaluee = Reader.GetInt32("Evaluee_fclt")
            End While
            Evaluee = Evaluee + 1
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub setevaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblfaculty set evaluee_fclt='" & Evaluee & "', evaluee_self = 1 where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_iaj Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_iaj = Reader.GetInt32("Faculty_IAJDump")
            End While
            P_iaj = P_iaj + Parameter_iaj
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_responsibility Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_responsibility = Reader.GetInt32("Faculty_responsibilityDump")
            End While
            P_responsibility = P_responsibility + Parameter_responsibility
            '    t_responsibility = P_responsibility / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_es Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_es = Reader.GetInt32("Faculty_esDump")
            End While
            P_es = P_es + Parameter_es
            '   t_es = P_es / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_adaptability Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_adaptability = Reader.GetInt32("Faculty_adaptabilityDump")
            End While
            P_adaptability = P_adaptability + Parameter_adaptability
            '  t_adaptability = P_adaptability / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_cat()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cat Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_cat = Reader.GetInt32("Faculty_catDump")
            End While
            P_cat = P_cat + Parameter_cat
            '  t_cat = P_cat / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_respect Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_respect = Reader.GetInt32("Faculty_respectDump")
            End While
            P_respect = P_respect + Parameter_respect
            '   t_respect = P_respect / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_ec Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_ec = Reader.GetInt32("Faculty_ecDump")
            End While
            P_ec = P_ec + Parameter_ec
            '  t_ec = P_ec / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cr Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_cr = Reader.GetInt32("Faculty_crDump")
            End While
            P_cr = P_cr + Parameter_cr
            ' t_cr = P_cr / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_punctuality Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_punctuality = Reader.GetInt32("Faculty_punctualityDump")
            End While
            P_punctuality = P_punctuality + Parameter_punctuality
            '  t_punctuality = P_punctuality / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_mc_atl Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_mc_atl = Reader.GetInt32("Faculty_mc_atlDump")
            End While
            P_mc_atl = P_mc_atl + Parameter_mc_atl
            '  t_mc_atl = P_mc_atl / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_da Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_da = Reader.GetInt32("Faculty_daDump")
            End While
            P_da = P_da + Parameter_da
            '  t_da = P_da / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_pa_cah Where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                P_pa_cah = Reader.GetInt32("Faculty_pa_cahDump")
            End While
            P_pa_cah = P_pa_cah + Parameter_pa_cah
            '  t_pa_cah = P_pa_cah / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub


    Sub Update_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_iaj set Faculty_IAJDump='" & P_iaj & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_responsibility set Faculty_responsibilityDump='" & P_responsibility & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_es set Faculty_esDump='" & P_es & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_adaptability set Faculty_adaptabilityDump='" & P_adaptability & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_cat()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_cat set Faculty_catDump='" & P_cat & "'where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_respect set Faculty_respectDump='" & P_respect & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_ec set Faculty_ecDump='" & P_ec & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_cr set Faculty_crDump='" & P_cr & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_punctuality set Faculty_punctualityDump='" & P_punctuality & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_mc_atl set Faculty_mc_atlDump='" & P_mc_atl & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_da set Faculty_daDump='" & P_da & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_pa_cah set Faculty_pa_cahDump='" & P_pa_cah & "' where ID_Number = '" & Username & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub



    Private Sub SelfEvaluationForm4_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Username = SelfEvaluationForm3.Username
    End Sub

    Private Sub Label9_Click(sender As System.Object, e As System.EventArgs) Handles Label9.Click

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        SelfEvaluationForm3.Show()
        Me.Hide()
    End Sub
End Class