﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelfEvaluationForm4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Radio11_1 = New System.Windows.Forms.RadioButton()
        Me.Radio11_2 = New System.Windows.Forms.RadioButton()
        Me.Radio11_4 = New System.Windows.Forms.RadioButton()
        Me.Radio11_3 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Radio10_1 = New System.Windows.Forms.RadioButton()
        Me.Radio10_2 = New System.Windows.Forms.RadioButton()
        Me.Radio10_4 = New System.Windows.Forms.RadioButton()
        Me.Radio10_3 = New System.Windows.Forms.RadioButton()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Radio12_1 = New System.Windows.Forms.RadioButton()
        Me.Radio12_2 = New System.Windows.Forms.RadioButton()
        Me.Radio12_4 = New System.Windows.Forms.RadioButton()
        Me.Radio12_3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(76, 153)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(826, 40)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "- Not within standards more than 6 times during the past 3 months; Rarely (less t" & _
    "han 75% of the time) keeps self and work area" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " clean and orderly; rarely practi" & _
    "ces the 5S principles"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Radio11_1)
        Me.GroupBox2.Controls.Add(Me.Radio11_2)
        Me.GroupBox2.Controls.Add(Me.Radio11_4)
        Me.GroupBox2.Controls.Add(Me.Radio11_3)
        Me.GroupBox2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(34, 174)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(941, 155)
        Me.GroupBox2.TabIndex = 45
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Deadline Awareness"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(73, 109)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(632, 20)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "- Rarely (less than 75%) submits correct and accurate work, reports and other doc" & _
    "uments on time"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(73, 86)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(644, 20)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "- Sometimes (at least 75) submits correct and accurate work, reports and other do" & _
    "cuments on time"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(73, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(575, 20)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "- Usually (90%) submits correct and accurate work, reports and other documents on" & _
    " time"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(73, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(532, 20)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "- Always submits correct and accurate work, reports and other documents on time"
        '
        'Radio11_1
        '
        Me.Radio11_1.AutoSize = True
        Me.Radio11_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio11_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio11_1.Location = New System.Drawing.Point(36, 107)
        Me.Radio11_1.Name = "Radio11_1"
        Me.Radio11_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio11_1.TabIndex = 0
        Me.Radio11_1.Text = "1"
        Me.Radio11_1.UseVisualStyleBackColor = True
        '
        'Radio11_2
        '
        Me.Radio11_2.AutoSize = True
        Me.Radio11_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio11_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio11_2.Location = New System.Drawing.Point(36, 84)
        Me.Radio11_2.Name = "Radio11_2"
        Me.Radio11_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio11_2.TabIndex = 1
        Me.Radio11_2.Text = "2"
        Me.Radio11_2.UseVisualStyleBackColor = True
        '
        'Radio11_4
        '
        Me.Radio11_4.AutoSize = True
        Me.Radio11_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio11_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio11_4.Location = New System.Drawing.Point(35, 38)
        Me.Radio11_4.Name = "Radio11_4"
        Me.Radio11_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio11_4.TabIndex = 3
        Me.Radio11_4.Text = "4"
        Me.Radio11_4.UseVisualStyleBackColor = True
        '
        'Radio11_3
        '
        Me.Radio11_3.AutoSize = True
        Me.Radio11_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio11_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio11_3.Location = New System.Drawing.Point(36, 61)
        Me.Radio11_3.Name = "Radio11_3"
        Me.Radio11_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio11_3.TabIndex = 2
        Me.Radio11_3.Text = "3"
        Me.Radio11_3.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(73, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(826, 40)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "- A slow learner; prefers same kind of works; finds great difficulty in making ne" & _
    "w adjustments; cannot grasp new work even after" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " careful instructions"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(75, 117)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(836, 40)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "- Missed some of the grooming standards 4-6 times during the past 3 months; at le" & _
    "ast 75% of the time keeps self and work area " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "clean and orderly; sometimes prac" & _
    "tices 5S principles"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(686, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "- Good, sufficient for present responsibilities; normally able to adjust to new w" & _
    "ork after careful instructions"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Radio10_1)
        Me.GroupBox1.Controls.Add(Me.Radio10_2)
        Me.GroupBox1.Controls.Add(Me.Radio10_4)
        Me.GroupBox1.Controls.Add(Me.Radio10_3)
        Me.GroupBox1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(34, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(941, 155)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Mental Capacity / Ability to Learn"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(831, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "- A little more capable than with present responsibilities readily adjustable to " & _
    "new  work with brief instructions; welcomes changes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(73, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(817, 40)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "- Very capable for present responsibilities; unusual keenness of perception; gras" & _
    "ps new work quickly and easily anticipates new" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " developments"
        '
        'Radio10_1
        '
        Me.Radio10_1.AutoSize = True
        Me.Radio10_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio10_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio10_1.Location = New System.Drawing.Point(36, 113)
        Me.Radio10_1.Name = "Radio10_1"
        Me.Radio10_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio10_1.TabIndex = 0
        Me.Radio10_1.Text = "1"
        Me.Radio10_1.UseVisualStyleBackColor = True
        '
        'Radio10_2
        '
        Me.Radio10_2.AutoSize = True
        Me.Radio10_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio10_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio10_2.Location = New System.Drawing.Point(36, 90)
        Me.Radio10_2.Name = "Radio10_2"
        Me.Radio10_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio10_2.TabIndex = 1
        Me.Radio10_2.Text = "2"
        Me.Radio10_2.UseVisualStyleBackColor = True
        '
        'Radio10_4
        '
        Me.Radio10_4.AutoSize = True
        Me.Radio10_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio10_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio10_4.Location = New System.Drawing.Point(36, 28)
        Me.Radio10_4.Name = "Radio10_4"
        Me.Radio10_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio10_4.TabIndex = 3
        Me.Radio10_4.Text = "4"
        Me.Radio10_4.UseVisualStyleBackColor = True
        '
        'Radio10_3
        '
        Me.Radio10_3.AutoSize = True
        Me.Radio10_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio10_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio10_3.Location = New System.Drawing.Point(36, 67)
        Me.Radio10_3.Name = "Radio10_3"
        Me.Radio10_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio10_3.TabIndex = 2
        Me.Radio10_3.Text = "3"
        Me.Radio10_3.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(76, 81)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(845, 40)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "- Missed some of the grooming standards 1-3 times during past 3 months; at least " & _
    "90% of the time keeps self and work area clean" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " and orderly; usually practices " & _
    "5S principles"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(74, 45)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(785, 40)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "- Always meets grooming standards of the company; always keep self and work area " & _
    "clean and orderly; always practice 5S" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " principles"
        '
        'Radio12_1
        '
        Me.Radio12_1.AutoSize = True
        Me.Radio12_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio12_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio12_1.Location = New System.Drawing.Point(35, 151)
        Me.Radio12_1.Name = "Radio12_1"
        Me.Radio12_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio12_1.TabIndex = 0
        Me.Radio12_1.Text = "1"
        Me.Radio12_1.UseVisualStyleBackColor = True
        '
        'Radio12_2
        '
        Me.Radio12_2.AutoSize = True
        Me.Radio12_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio12_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio12_2.Location = New System.Drawing.Point(35, 117)
        Me.Radio12_2.Name = "Radio12_2"
        Me.Radio12_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio12_2.TabIndex = 1
        Me.Radio12_2.Text = "2"
        Me.Radio12_2.UseVisualStyleBackColor = True
        '
        'Radio12_4
        '
        Me.Radio12_4.AutoSize = True
        Me.Radio12_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio12_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio12_4.Location = New System.Drawing.Point(35, 43)
        Me.Radio12_4.Name = "Radio12_4"
        Me.Radio12_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio12_4.TabIndex = 3
        Me.Radio12_4.Text = "4"
        Me.Radio12_4.UseVisualStyleBackColor = True
        '
        'Radio12_3
        '
        Me.Radio12_3.AutoSize = True
        Me.Radio12_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio12_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio12_3.Location = New System.Drawing.Point(35, 79)
        Me.Radio12_3.Name = "Radio12_3"
        Me.Radio12_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio12_3.TabIndex = 2
        Me.Radio12_3.Text = "3"
        Me.Radio12_3.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Radio12_1)
        Me.GroupBox3.Controls.Add(Me.Radio12_2)
        Me.GroupBox3.Controls.Add(Me.Radio12_4)
        Me.GroupBox3.Controls.Add(Me.Radio12_3)
        Me.GroupBox3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(34, 335)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(941, 201)
        Me.GroupBox3.TabIndex = 46
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Personal Appearance / Cleanliness and Housekeeping ( Meeting grooming and uniform" & _
    " standards of the organization and active concern for cleanliness and 5S standar" & _
    "ds )"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.SandyBrown
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(923, 571)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 29)
        Me.Button1.TabIndex = 47
        Me.Button1.Text = "Submit"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.SandyBrown
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(12, 571)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 29)
        Me.Button2.TabIndex = 50
        Me.Button2.Text = "Previous"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(476, 565)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 34)
        Me.Label14.TabIndex = 51
        Me.Label14.Text = "4 of 4"
        '
        'SelfEvaluationForm4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SelfEvaluationForm4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SelfEvaluationForm4"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Radio11_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio11_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio11_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio11_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Radio10_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio10_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio10_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio10_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Radio12_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio12_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio12_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio12_3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
End Class
