﻿Public Class SelfEvaluationForm3
    Public Username As String
    Public Parameter_iaj As Integer
    Public Parameter_responsibility As Integer
    Public Parameter_es As Integer
    Public Parameter_adaptability As Integer
    Public Parameter_cat As Integer
    Public Parameter_respect As Integer
    Public Parameter_ec As Integer
    Public Parameter_cr As Integer
    Public Parameter_punctuality As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SelfEvaluationForm4.Show()
        Me.Hide()
    End Sub
    Private Sub Radio9_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_1.CheckedChanged
        Parameter_punctuality = 1
    End Sub
    Private Sub Radio9_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_2.CheckedChanged
        Parameter_punctuality = 2
    End Sub
    Private Sub Radio9_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_3.CheckedChanged
        Parameter_punctuality = 3
    End Sub
    Private Sub Radio9_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_4.CheckedChanged
        Parameter_punctuality = 4
    End Sub
    Private Sub Radio8_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_1.CheckedChanged
        Parameter_cr = 1
    End Sub
    Private Sub Radio8_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_2.CheckedChanged
        Parameter_cr = 2
    End Sub
    Private Sub Radio8_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_3.CheckedChanged
        Parameter_cr = 3
    End Sub
    Private Sub Radio8_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_4.CheckedChanged
        Parameter_cr = 4
    End Sub
    Private Sub Radio7_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_1.CheckedChanged
        Parameter_ec = 1
    End Sub
    Private Sub Radio7_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_2.CheckedChanged
        Parameter_ec = 2
    End Sub
    Private Sub Radio7_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_3.CheckedChanged
        Parameter_ec = 3
    End Sub
    Private Sub Radio7_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_4.CheckedChanged
        Parameter_ec = 4
    End Sub

    Private Sub SelfEvaluationForm3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Username = SelfEvaluationForm2.Username
        Parameter_iaj = SelfEvaluationForm2.Parameter_iaj
        Parameter_responsibility = SelfEvaluationForm2.Parameter_responsibility
        Parameter_es = SelfEvaluationForm2.Parameter_es
        Parameter_adaptability = SelfEvaluationForm2.Parameter_adaptability
        Parameter_cat = SelfEvaluationForm2.Parameter_cat
        Parameter_respect = SelfEvaluationForm2.Parameter_respect
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        SelfEvaluationForm2.Show()
        Me.Hide()
    End Sub
End Class