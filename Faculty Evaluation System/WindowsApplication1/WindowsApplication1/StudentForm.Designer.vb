﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StudentForm))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Date_of_Evaluation = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Combobox_Subject = New System.Windows.Forms.ComboBox()
        Me.Combobox_Faculty = New System.Windows.Forms.ComboBox()
        Me.Button_BackAccount = New System.Windows.Forms.Button()
        Me.LblID = New System.Windows.Forms.Label()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.Faculty_name = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Date of Evaluation"
        Me.Label3.Visible = False
        '
        'Date_of_Evaluation
        '
        Me.Date_of_Evaluation.CustomFormat = "MM-dd-yyyy"
        Me.Date_of_Evaluation.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Date_of_Evaluation.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Date_of_Evaluation.Location = New System.Drawing.Point(122, 57)
        Me.Date_of_Evaluation.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Date_of_Evaluation.Name = "Date_of_Evaluation"
        Me.Date_of_Evaluation.Size = New System.Drawing.Size(120, 22)
        Me.Date_of_Evaluation.TabIndex = 5
        Me.Date_of_Evaluation.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(390, 361)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Select Subject"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(390, 333)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Select Faculty"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(466, 387)
        Me.Button1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 30)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Proceed"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Combobox_Subject
        '
        Me.Combobox_Subject.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_Subject.FormattingEnabled = True
        Me.Combobox_Subject.Location = New System.Drawing.Point(498, 355)
        Me.Combobox_Subject.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Combobox_Subject.Name = "Combobox_Subject"
        Me.Combobox_Subject.Size = New System.Drawing.Size(121, 25)
        Me.Combobox_Subject.TabIndex = 1
        Me.Combobox_Subject.Visible = False
        '
        'Combobox_Faculty
        '
        Me.Combobox_Faculty.Font = New System.Drawing.Font("Franklin Gothic Book", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combobox_Faculty.FormattingEnabled = True
        Me.Combobox_Faculty.Location = New System.Drawing.Point(498, 327)
        Me.Combobox_Faculty.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Combobox_Faculty.Name = "Combobox_Faculty"
        Me.Combobox_Faculty.Size = New System.Drawing.Size(121, 25)
        Me.Combobox_Faculty.TabIndex = 0
        '
        'Button_BackAccount
        '
        Me.Button_BackAccount.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Button_BackAccount.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button_BackAccount.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button_BackAccount.FlatAppearance.BorderSize = 0
        Me.Button_BackAccount.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button_BackAccount.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button_BackAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_BackAccount.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button_BackAccount.Location = New System.Drawing.Point(925, 21)
        Me.Button_BackAccount.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Button_BackAccount.Name = "Button_BackAccount"
        Me.Button_BackAccount.Size = New System.Drawing.Size(81, 31)
        Me.Button_BackAccount.TabIndex = 2
        Me.Button_BackAccount.Text = "Logout"
        Me.Button_BackAccount.UseVisualStyleBackColor = False
        '
        'LblID
        '
        Me.LblID.AutoSize = True
        Me.LblID.BackColor = System.Drawing.Color.Transparent
        Me.LblID.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblID.Location = New System.Drawing.Point(12, 21)
        Me.LblID.Name = "LblID"
        Me.LblID.Size = New System.Drawing.Size(26, 24)
        Me.LblID.TabIndex = 3
        Me.LblID.Text = "ID"
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.logo
        Me.RectangleShape1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RectangleShape1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.RectangleShape1.Location = New System.Drawing.Point(418, 99)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(164, 163)
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1018, 616)
        Me.ShapeContainer1.TabIndex = 4
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        Me.RectangleShape2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.RectangleShape2.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.Loginuser2
        Me.RectangleShape2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.RectangleShape2.Location = New System.Drawing.Point(366, 286)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(270, 139)
        '
        'Faculty_name
        '
        Me.Faculty_name.AutoSize = True
        Me.Faculty_name.BackColor = System.Drawing.Color.Transparent
        Me.Faculty_name.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Faculty_name.Location = New System.Drawing.Point(496, 296)
        Me.Faculty_name.Name = "Faculty_name"
        Me.Faculty_name.Size = New System.Drawing.Size(26, 24)
        Me.Faculty_name.TabIndex = 7
        Me.Faculty_name.Text = "ID"
        '
        'StudentForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 616)
        Me.Controls.Add(Me.Faculty_name)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Combobox_Subject)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Combobox_Faculty)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Date_of_Evaluation)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.LblID)
        Me.Controls.Add(Me.Button_BackAccount)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StudentForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "StudentForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Combobox_Subject As System.Windows.Forms.ComboBox
    Friend WithEvents Combobox_Faculty As System.Windows.Forms.ComboBox
    Friend WithEvents Button_BackAccount As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Date_of_Evaluation As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblID As System.Windows.Forms.Label
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents Faculty_name As System.Windows.Forms.Label
End Class
