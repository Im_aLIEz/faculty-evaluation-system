﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AdminForm))
        Me.View_FacultyResult = New System.Windows.Forms.Button()
        Me.Logout_Admin = New System.Windows.Forms.Button()
        Me.Faculty_Management = New System.Windows.Forms.Button()
        Me.Position_Management = New System.Windows.Forms.Button()
        Me.Account_Management = New System.Windows.Forms.Button()
        Me.Student_Management = New System.Windows.Forms.Button()
        Me.Section_Management = New System.Windows.Forms.Button()
        Me.Subject_Management = New System.Windows.Forms.Button()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.SuspendLayout()
        '
        'View_FacultyResult
        '
        Me.View_FacultyResult.BackColor = System.Drawing.Color.DodgerBlue
        Me.View_FacultyResult.Cursor = System.Windows.Forms.Cursors.Hand
        Me.View_FacultyResult.FlatAppearance.BorderSize = 0
        Me.View_FacultyResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.View_FacultyResult.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.View_FacultyResult.Location = New System.Drawing.Point(406, 447)
        Me.View_FacultyResult.Name = "View_FacultyResult"
        Me.View_FacultyResult.Size = New System.Drawing.Size(172, 44)
        Me.View_FacultyResult.TabIndex = 4
        Me.View_FacultyResult.Text = "View Faculty Result"
        Me.View_FacultyResult.UseVisualStyleBackColor = False
        '
        'Logout_Admin
        '
        Me.Logout_Admin.BackColor = System.Drawing.Color.DodgerBlue
        Me.Logout_Admin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Logout_Admin.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption
        Me.Logout_Admin.FlatAppearance.BorderSize = 0
        Me.Logout_Admin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Logout_Admin.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Logout_Admin.Location = New System.Drawing.Point(917, 15)
        Me.Logout_Admin.Name = "Logout_Admin"
        Me.Logout_Admin.Size = New System.Drawing.Size(80, 33)
        Me.Logout_Admin.TabIndex = 5
        Me.Logout_Admin.Text = "Logout"
        Me.Logout_Admin.UseVisualStyleBackColor = False
        '
        'Faculty_Management
        '
        Me.Faculty_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Faculty_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Faculty_Management.FlatAppearance.BorderSize = 0
        Me.Faculty_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Faculty_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Faculty_Management.Location = New System.Drawing.Point(406, 197)
        Me.Faculty_Management.Name = "Faculty_Management"
        Me.Faculty_Management.Size = New System.Drawing.Size(172, 44)
        Me.Faculty_Management.TabIndex = 7
        Me.Faculty_Management.Text = "Faculty Management"
        Me.Faculty_Management.UseVisualStyleBackColor = False
        '
        'Position_Management
        '
        Me.Position_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Position_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Position_Management.FlatAppearance.BorderSize = 0
        Me.Position_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Position_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Position_Management.Location = New System.Drawing.Point(406, 247)
        Me.Position_Management.Name = "Position_Management"
        Me.Position_Management.Size = New System.Drawing.Size(172, 44)
        Me.Position_Management.TabIndex = 8
        Me.Position_Management.Text = "Position Management"
        Me.Position_Management.UseVisualStyleBackColor = False
        '
        'Account_Management
        '
        Me.Account_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Account_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Account_Management.FlatAppearance.BorderSize = 0
        Me.Account_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Account_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Account_Management.Location = New System.Drawing.Point(406, 147)
        Me.Account_Management.Name = "Account_Management"
        Me.Account_Management.Size = New System.Drawing.Size(172, 44)
        Me.Account_Management.TabIndex = 9
        Me.Account_Management.Text = "Account Management"
        Me.Account_Management.UseVisualStyleBackColor = False
        '
        'Student_Management
        '
        Me.Student_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Student_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Student_Management.FlatAppearance.BorderSize = 0
        Me.Student_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Student_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Student_Management.Location = New System.Drawing.Point(406, 347)
        Me.Student_Management.Name = "Student_Management"
        Me.Student_Management.Size = New System.Drawing.Size(172, 44)
        Me.Student_Management.TabIndex = 10
        Me.Student_Management.Text = "Student Management"
        Me.Student_Management.UseVisualStyleBackColor = False
        '
        'Section_Management
        '
        Me.Section_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Section_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Section_Management.FlatAppearance.BorderSize = 0
        Me.Section_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Section_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Section_Management.Location = New System.Drawing.Point(406, 297)
        Me.Section_Management.Name = "Section_Management"
        Me.Section_Management.Size = New System.Drawing.Size(172, 44)
        Me.Section_Management.TabIndex = 12
        Me.Section_Management.Text = "Section Management"
        Me.Section_Management.UseVisualStyleBackColor = False
        '
        'Subject_Management
        '
        Me.Subject_Management.BackColor = System.Drawing.Color.DodgerBlue
        Me.Subject_Management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Subject_Management.FlatAppearance.BorderSize = 0
        Me.Subject_Management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Subject_Management.Font = New System.Drawing.Font("Arial Narrow", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Subject_Management.Location = New System.Drawing.Point(406, 397)
        Me.Subject_Management.Name = "Subject_Management"
        Me.Subject_Management.Size = New System.Drawing.Size(172, 44)
        Me.Subject_Management.TabIndex = 13
        Me.Subject_Management.Text = "Subject Management"
        Me.Subject_Management.UseVisualStyleBackColor = False
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1018, 616)
        Me.ShapeContainer1.TabIndex = 14
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.Low_opacity3
        Me.RectangleShape1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RectangleShape1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.RectangleShape1.Location = New System.Drawing.Point(117, 157)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(273, 265)
        '
        'RectangleShape2
        '
        Me.RectangleShape2.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.Low_opacity3
        Me.RectangleShape2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RectangleShape2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.RectangleShape2.Location = New System.Drawing.Point(604, 146)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(273, 265)
        '
        'AdminForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1018, 616)
        Me.Controls.Add(Me.Subject_Management)
        Me.Controls.Add(Me.Section_Management)
        Me.Controls.Add(Me.Student_Management)
        Me.Controls.Add(Me.Account_Management)
        Me.Controls.Add(Me.Position_Management)
        Me.Controls.Add(Me.Faculty_Management)
        Me.Controls.Add(Me.Logout_Admin)
        Me.Controls.Add(Me.View_FacultyResult)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AdminForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AdminForm"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents View_FacultyResult As System.Windows.Forms.Button
    Friend WithEvents Logout_Admin As System.Windows.Forms.Button
    Friend WithEvents Faculty_Management As System.Windows.Forms.Button
    Friend WithEvents Position_Management As System.Windows.Forms.Button
    Friend WithEvents Account_Management As System.Windows.Forms.Button
    Friend WithEvents Student_Management As System.Windows.Forms.Button
    Friend WithEvents Section_Management As System.Windows.Forms.Button
    Friend WithEvents Subject_Management As System.Windows.Forms.Button
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
End Class
