﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FacultyForm))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Logout_Admin = New System.Windows.Forms.Button()
        Me.LblID = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(417, 218)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(199, 68)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "View Results"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(417, 304)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(199, 68)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Evaluate"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Logout_Admin
        '
        Me.Logout_Admin.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Logout_Admin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Logout_Admin.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue
        Me.Logout_Admin.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Logout_Admin.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Logout_Admin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Logout_Admin.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Logout_Admin.Location = New System.Drawing.Point(931, 9)
        Me.Logout_Admin.Name = "Logout_Admin"
        Me.Logout_Admin.Size = New System.Drawing.Size(75, 29)
        Me.Logout_Admin.TabIndex = 6
        Me.Logout_Admin.Text = "Logout"
        Me.Logout_Admin.UseVisualStyleBackColor = False
        '
        'LblID
        '
        Me.LblID.AutoSize = True
        Me.LblID.BackColor = System.Drawing.Color.Transparent
        Me.LblID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.LblID.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblID.Location = New System.Drawing.Point(12, 12)
        Me.LblID.Name = "LblID"
        Me.LblID.Size = New System.Drawing.Size(29, 26)
        Me.LblID.TabIndex = 7
        Me.LblID.Text = "ID"
        '
        'FacultyForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.LblID)
        Me.Controls.Add(Me.Logout_Admin)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FacultyForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Logout_Admin As System.Windows.Forms.Button
    Friend WithEvents LblID As System.Windows.Forms.Label
End Class
