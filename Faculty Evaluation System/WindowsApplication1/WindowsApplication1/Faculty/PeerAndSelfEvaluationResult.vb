﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class PeerAndSelfEvaluationResult
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Public ID As String
    Dim Total As Double
    Dim Avg As Double
    Dim evaluee As Integer
    Dim get_iaj As Integer
    Dim get_responsibility As Integer
    Dim get_es As Integer
    Dim get_adaptability As Integer
    Dim get_cot As Integer
    Dim get_respect As Integer
    Dim get_ec As Integer
    Dim get_cr As Integer
    Dim get_punctuality As Integer
    Dim get_mc_atl As Integer
    Dim get_da As Integer
    Dim get_pa_cah As Integer

    Dim avg_iaj As Double
    Dim avg_responsibility As Double
    Dim avg_es As Double
    Dim avg_adaptability As Double
    Dim avg_cot As Double
    Dim avg_respect As Double
    Dim avg_ec As Double
    Dim avg_cr As Double
    Dim avg_punctuality As Double
    Dim avg_mc_atl As Double
    Dim avg_da As Double
    Dim avg_pa_cah As Double


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ResultForm.Show()
        Me.Close()
    End Sub
    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles PrintLabel.LinkClicked
        PrintLabel.Visible = False
        Me.PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintLabel.Visible = True
        Me.PrintForm1.Print()
    End Sub
    Private Sub PeerAndSelfEvaluationResult_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ID = ResultForm.Username
        GetEvaluee()
        average_iaj()
        average_responsibility()
        average_es()
        average_adaptability()
        average_cot()
        average_respect()
        average_ec()
        average_cr()
        average_punctuality()
        average_mc_atl()
        average_da()
        average_pa_cah()
        resultIAJ()
        resultadaptability()
        resultresponsibility()
        resultES()
        resultCAT()
        resultrespect()
        resultEC()
        resultcr()
        resultpunctuality()
        resultMCATL()
        resultDA()
        resultpacah()
        resultGrandTotal()
        'NameView()
    End Sub

    Sub GetEvaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblfaculty Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                evaluee = Reader.GetInt32("Evaluee_fclt")
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultadaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_AdaptabilityAvg from tblresultfaculty_Adaptability where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                A_Score.Text = avg_adaptability
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultIAJ()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_IAJAvg from tblresultfaculty_IAJ where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                IAJ_Score.Text = avg_iaj
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultresponsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_responsibilityAvg from tblresultfaculty_responsibility where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                R_Score.Text = avg_responsibility
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultES()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_ESAvg from tblresultfaculty_ES where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                ES_Score.Text = Avg_es
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultCAT()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_CATAvg from tblresultfaculty_CAT where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                CaT_Score.Text = avg_cot
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultrespect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_respectAvg from tblresultfaculty_respect where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                RSPT_Score.Text = avg_respect
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultEC()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_ECAvg from tblresultfaculty_EC where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                EC_Score.Text = avg_ec
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultcr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_crAvg from tblresultfaculty_cr where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                CR_Score.Text = avg_cr
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultpunctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_punctualityAvg from tblresultfaculty_punctuality where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                P_Score.Text = avg_punctuality
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultMCATL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_mc_atlAvg from tblresultfaculty_mc_atl where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                MCAtL_Score.Text = avg_mc_atl
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultDA()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_DAAvg from tblresultfaculty_da where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                DA_Score.Text = avg_da
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultpacah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Faculty_pa_cahAvg from tblresultfaculty_pa_cah where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                PACaH_Score.Text = avg_pa_cah
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cr Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_cr = Reader.GetInt32("Faculty_crDump")
            End While
            avg_cr = get_cr / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_es Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_es = Reader.GetInt32("Faculty_esDump")
            End While
            avg_es = get_es / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_iaj Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_iaj = Reader.GetInt32("Faculty_iajDump")
            End While
            avg_iaj = get_iaj / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_responsibility Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_responsibility = Reader.GetInt32("Faculty_responsibilityDump")
            End While
            avg_responsibility = get_responsibility / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_cot()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cat Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_cot = Reader.GetInt32("Faculty_catDump")
            End While
            avg_cot = get_cot / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_respect Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_respect = Reader.GetInt32("Faculty_respectDump")
            End While
            avg_respect = get_respect / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_ec Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_ec = Reader.GetInt32("Faculty_ecDump")
            End While
            avg_ec = get_ec / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_adaptability Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_adaptability = Reader.GetInt32("Faculty_adaptabilityDump")
            End While
            avg_adaptability = get_adaptability / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_punctuality Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_punctuality = Reader.GetInt32("Faculty_punctualityDump")
            End While
            avg_punctuality = get_punctuality / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_mc_atl Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_mc_atl = Reader.GetInt32("Faculty_mc_atlDump")
            End While
            avg_mc_atl = get_mc_atl / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_da Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_da = Reader.GetInt32("Faculty_daDump")
            End While
            avg_da = get_da / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_pa_cah Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_pa_cah = Reader.GetInt32("Faculty_pa_cahDump")
            End While
            avg_pa_cah = get_pa_cah / evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub resultGrandTotal()
        Total = avg_iaj + avg_responsibility + avg_es + avg_adaptability + avg_cot + avg_respect + avg_ec + avg_cr + avg_punctuality + avg_mc_atl + avg_da + avg_pa_cah
        Avg = Total / 12
        GT_Score.Text = Total
        Ave_Score.Text = Avg
    End Sub





    ' Sub Result()
    '   Mysqlconn = New MySqlConnection
    '     Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
    '  Dim Reader As MySqlDataReader

    '    Try
    '        Mysqlconn.Open()
    'Dim Query As String
    '        Query = "SELECT tblresultfaculty_adaptability.ID_Number, tblresultfaculty_adaptability.Faculty_AdaptabilityAvg, tblresultfaculty_cat.Faculty_CATAvg, tblresultfaculty_cr.Faculty_CRAvg, tblresultfaculty_da.Faculty_DAAvg, tblresultfaculty_ec.Faculty_ECAvg, tblresultfaculty_es.Faculty_ESAvg, tblresultfaculty_iaj.Faculty_IAJAvg, tblresultfaculty_mc_atl.Faculty_MC_ATLAvg, tblresultfaculty_pa_cah.Faculty_PA_CAHAvg, tblresultfaculty_punctuality.Faculty_PunctualityAvg,tblresultfaculty_respect.Faculty_RespectAvg, tblresultfaculty.Faculty_ResponsibilityAvg FROM `tblresultfaculty_adaptability` INNER JOIN tblresultfaculty_cat on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_cr on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_da on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_ec on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_es on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_iaj on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_mc_atl on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_pa_cah on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_punctuality on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' INNER JOIN tblresultfaculty_respect on tblresultfaculty_cat.ID_Number = '" & ResultForm.LblID.Text & "' "
    '        command = New MySqlCommand(Query, Mysqlconn)
    '        Reader = command.ExecuteReader
    '        While Reader.Read
    '            IAJ_Score.Text = Reader.GetString("Faculty_IAJAvg")
    '            R_Score.Text = Reader.GetString("Faculty_ResponsibilityAvg")
    '            ES_Score.Text = Reader.GetString("Faculty_ESAvg")
    '            A_Score.Text = Reader.GetString("Faculty_AdaptabilityAvg")
    '            CaT_Score.Text = Reader.GetString("Faculty_CATAvg")
    '            RSPT_Score.Text = Reader.GetString("Faculty_RespectAvg")
    '            EC_Score.Text = Reader.GetString("Faculty_ECAvg")
    '            CR_Score.Text = Reader.GetString("Faculty_CRAvg")
    '            P_Score.Text = Reader.GetString("Faculty_PunctualityAvg")
    '            MCAtL_Score.Text = Reader.GetString("Faculty_MC_ATLAvg")
    '            DA_Score.Text = Reader.GetString("Faculty_DAAvg")
    '            PACaH_Score.Text = Reader.GetString("Faculty_PA_CAHAvg")

    '            Total = (Convert.ToInt32(IAJ_Score.Text) + Convert.ToInt32(R_Score.Text) + Convert.ToInt32(ES_Score.Text) + Convert.ToInt32(A_Score.Text) + Convert.ToInt32(CaT_Score.Text) + Convert.ToInt32(RSPT_Score.Text) + Convert.ToInt32(EC_Score.Text) + Convert.ToInt32(CR_Score.Text) + Convert.ToInt32(P_Score.Text) + Convert.ToInt32(MCAtL_Score.Text) + Convert.ToInt32(DA_Score.Text) + Convert.ToInt32(PACaH_Score.Text)).ToString()
    '            GT_Score.Text = Total
    '            Ave_Score.Text = (Convert.ToDouble(Total) / 12).ToString()
    '        End While
    '        Mysqlconn.Close()
    '    Catch ex As MySqlException
    '        MessageBox.Show(ex.Message)
    '    Finally
    '        Mysqlconn.Dispose()
    '    End Try
    'End Sub
    'Sub NameView()
    '    Mysqlconn = New MySqlConnection
    '    Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
    '    Dim Reader As MySqlDataReader

    '    Try
    '        Mysqlconn.Open()
    '        Dim Query As String
    '        Query = "select * from tblfaculty where ID_Number = '" & ID & "'"
    '        command = New MySqlCommand(Query, Mysqlconn)
    '        Reader = command.ExecuteReader
    '        While Reader.Read
    '            Dim FirstName = Reader.GetString("First_Name")
    '            Dim LastName = Reader.GetString("Last_Name")
    '            NameLabel.Text = FirstName + " " + LastName
    '        End While
    '        Mysqlconn.Close()
    '    Catch ex As MySqlException
    '        MessageBox.Show(ex.Message)
    '    Finally
    '        Mysqlconn.Dispose()
    '    End Try
    'End Sub

    'Private Sub btn_Back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Admin_FacultyResult_PeerAndSelf.Show()
    '    Me.Hide()
    'End Sub
End Class