﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentEvaluationResult
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StudentEvaluationResult))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Commitment_Score = New System.Windows.Forms.Label()
        Me.KOSM_Score = New System.Windows.Forms.Label()
        Me.TFIL_Score = New System.Windows.Forms.Label()
        Me.MOL_Score = New System.Windows.Forms.Label()
        Me.TableStudentResults = New System.Windows.Forms.TableLayoutPanel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Overall_Score = New System.Windows.Forms.Label()
        Me.OvalShape2 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.OvalShape1 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.PrintLabel = New System.Windows.Forms.LinkLabel()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        Me.TableStudentResults.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(6, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(286, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Parameters"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 260)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(286, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Management of Learning"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 201)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(286, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Teaching for Independent Learning"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(286, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Knowledge of the Subject Matter"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(286, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Commitment"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(301, 24)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(174, 13)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Rating"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Commitment_Score
        '
        Me.Commitment_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Commitment_Score.AutoSize = True
        Me.Commitment_Score.Location = New System.Drawing.Point(301, 83)
        Me.Commitment_Score.Name = "Commitment_Score"
        Me.Commitment_Score.Size = New System.Drawing.Size(174, 13)
        Me.Commitment_Score.TabIndex = 16
        Me.Commitment_Score.Text = "0"
        Me.Commitment_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'KOSM_Score
        '
        Me.KOSM_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KOSM_Score.AutoSize = True
        Me.KOSM_Score.Location = New System.Drawing.Point(301, 142)
        Me.KOSM_Score.Name = "KOSM_Score"
        Me.KOSM_Score.Size = New System.Drawing.Size(174, 13)
        Me.KOSM_Score.TabIndex = 17
        Me.KOSM_Score.Text = "0"
        Me.KOSM_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TFIL_Score
        '
        Me.TFIL_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TFIL_Score.AutoSize = True
        Me.TFIL_Score.Location = New System.Drawing.Point(301, 201)
        Me.TFIL_Score.Name = "TFIL_Score"
        Me.TFIL_Score.Size = New System.Drawing.Size(174, 13)
        Me.TFIL_Score.TabIndex = 18
        Me.TFIL_Score.Text = "0"
        Me.TFIL_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MOL_Score
        '
        Me.MOL_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MOL_Score.AutoSize = True
        Me.MOL_Score.Location = New System.Drawing.Point(301, 260)
        Me.MOL_Score.Name = "MOL_Score"
        Me.MOL_Score.Size = New System.Drawing.Size(174, 13)
        Me.MOL_Score.TabIndex = 19
        Me.MOL_Score.Text = "0"
        Me.MOL_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableStudentResults
        '
        Me.TableStudentResults.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableStudentResults.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.TableStudentResults.ColumnCount = 2
        Me.TableStudentResults.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableStudentResults.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableStudentResults.Controls.Add(Me.MOL_Score, 1, 4)
        Me.TableStudentResults.Controls.Add(Me.TFIL_Score, 1, 3)
        Me.TableStudentResults.Controls.Add(Me.KOSM_Score, 1, 2)
        Me.TableStudentResults.Controls.Add(Me.Commitment_Score, 1, 1)
        Me.TableStudentResults.Controls.Add(Me.Label16, 1, 0)
        Me.TableStudentResults.Controls.Add(Me.Label2, 0, 1)
        Me.TableStudentResults.Controls.Add(Me.Label3, 0, 2)
        Me.TableStudentResults.Controls.Add(Me.Label4, 0, 3)
        Me.TableStudentResults.Controls.Add(Me.Label5, 0, 4)
        Me.TableStudentResults.Controls.Add(Me.Label1, 0, 0)
        Me.TableStudentResults.Controls.Add(Me.Label7, 0, 6)
        Me.TableStudentResults.Controls.Add(Me.Overall_Score, 1, 6)
        Me.TableStudentResults.Location = New System.Drawing.Point(78, 231)
        Me.TableStudentResults.Name = "TableStudentResults"
        Me.TableStudentResults.RowCount = 7
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableStudentResults.Size = New System.Drawing.Size(481, 414)
        Me.TableStudentResults.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 377)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(286, 13)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Overall Score"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Overall_Score
        '
        Me.Overall_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Overall_Score.AutoSize = True
        Me.Overall_Score.Location = New System.Drawing.Point(301, 377)
        Me.Overall_Score.Name = "Overall_Score"
        Me.Overall_Score.Size = New System.Drawing.Size(174, 13)
        Me.Overall_Score.TabIndex = 21
        Me.Overall_Score.Text = "0"
        Me.Overall_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'OvalShape2
        '
        Me.OvalShape2.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.tesda_logo_0
        Me.OvalShape2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.OvalShape2.Location = New System.Drawing.Point(458, 66)
        Me.OvalShape2.Name = "OvalShape2"
        Me.OvalShape2.Size = New System.Drawing.Size(90, 90)
        '
        'OvalShape1
        '
        Me.OvalShape1.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.logo500px_406x405
        Me.OvalShape1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.OvalShape1.Location = New System.Drawing.Point(72, 69)
        Me.OvalShape1.Name = "OvalShape1"
        Me.OvalShape1.Size = New System.Drawing.Size(90, 90)
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.OvalShape1, Me.OvalShape2})
        Me.ShapeContainer1.Size = New System.Drawing.Size(621, 742)
        Me.ShapeContainer1.TabIndex = 7
        Me.ShapeContainer1.TabStop = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(211, 113)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(161, 13)
        Me.Label19.TabIndex = 11
        Me.Label19.Text = "TRC Alfonso, Concepcion, Tarlac"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(200, 98)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(202, 13)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "CONCEPCION VOCATIONAL SCHOOL"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(173, 83)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(263, 13)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Technical Education and Skills Development Authority"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Constantia", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(173, 154)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(277, 14)
        Me.Label20.TabIndex = 13
        Me.Label20.Text = "SUMMARY - PERFORMANCE APPRAISAL SHEET"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(188, 215)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(115, 13)
        Me.Label22.TabIndex = 15
        Me.Label22.Text = "                                    "
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(84, 215)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(101, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Evaluation Quarter :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Constantia", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(75, 663)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(369, 13)
        Me.Label24.TabIndex = 17
        Me.Label24.Text = "Therefore, I hereby attest that I provided just, objective and truthful judgment." & _
    ""
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Constantia", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(101, 650)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(428, 13)
        Me.Label23.TabIndex = 16
        Me.Label23.Text = "This is to verify that I fully understand the procedure and purpose of this evalu" & _
    "ation process."
        '
        'PrintLabel
        '
        Me.PrintLabel.AutoSize = True
        Me.PrintLabel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PrintLabel.LinkColor = System.Drawing.Color.Blue
        Me.PrintLabel.Location = New System.Drawing.Point(581, 9)
        Me.PrintLabel.Name = "PrintLabel"
        Me.PrintLabel.Size = New System.Drawing.Size(28, 13)
        Me.PrintLabel.TabIndex = 18
        Me.PrintLabel.TabStop = True
        Me.PrintLabel.Text = "Print"
        '
        'PrintForm1
        '
        Me.PrintForm1.DocumentName = "document"
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'StudentEvaluationResult
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(621, 742)
        Me.Controls.Add(Me.PrintLabel)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TableStudentResults)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StudentEvaluationResult"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "StudentEvaluationResult"
        Me.TableStudentResults.ResumeLayout(False)
        Me.TableStudentResults.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Commitment_Score As System.Windows.Forms.Label
    Friend WithEvents KOSM_Score As System.Windows.Forms.Label
    Friend WithEvents TFIL_Score As System.Windows.Forms.Label
    Friend WithEvents MOL_Score As System.Windows.Forms.Label
    Friend WithEvents TableStudentResults As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Overall_Score As System.Windows.Forms.Label
    Friend WithEvents OvalShape2 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents OvalShape1 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents PrintLabel As System.Windows.Forms.LinkLabel
    Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
End Class
