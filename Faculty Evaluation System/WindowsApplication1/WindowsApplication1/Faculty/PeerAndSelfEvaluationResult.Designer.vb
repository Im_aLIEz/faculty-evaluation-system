﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PeerAndSelfEvaluationResult
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PeerAndSelfEvaluationResult))
        Me.TablePeerAndSelfResult = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PACaH_Score = New System.Windows.Forms.Label()
        Me.DA_Score = New System.Windows.Forms.Label()
        Me.MCAtL_Score = New System.Windows.Forms.Label()
        Me.P_Score = New System.Windows.Forms.Label()
        Me.CR_Score = New System.Windows.Forms.Label()
        Me.EC_Score = New System.Windows.Forms.Label()
        Me.RSPT_Score = New System.Windows.Forms.Label()
        Me.CaT_Score = New System.Windows.Forms.Label()
        Me.A_Score = New System.Windows.Forms.Label()
        Me.ES_Score = New System.Windows.Forms.Label()
        Me.R_Score = New System.Windows.Forms.Label()
        Me.IAJ_Score = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GT_Score = New System.Windows.Forms.Label()
        Me.Ave_Score = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.OvalShape2 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.OvalShape1 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.EvaluationQuarterLabel = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.PrintLabel = New System.Windows.Forms.LinkLabel()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        Me.Label25 = New System.Windows.Forms.Label()
        Me.NameLabel = New System.Windows.Forms.Label()
        Me.TablePeerAndSelfResult.SuspendLayout()
        Me.SuspendLayout()
        '
        'TablePeerAndSelfResult
        '
        Me.TablePeerAndSelfResult.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TablePeerAndSelfResult.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.TablePeerAndSelfResult.ColumnCount = 2
        Me.TablePeerAndSelfResult.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TablePeerAndSelfResult.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 322.0!))
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label1, 0, 0)
        Me.TablePeerAndSelfResult.Controls.Add(Me.PACaH_Score, 1, 12)
        Me.TablePeerAndSelfResult.Controls.Add(Me.DA_Score, 1, 11)
        Me.TablePeerAndSelfResult.Controls.Add(Me.MCAtL_Score, 1, 10)
        Me.TablePeerAndSelfResult.Controls.Add(Me.P_Score, 1, 9)
        Me.TablePeerAndSelfResult.Controls.Add(Me.CR_Score, 1, 8)
        Me.TablePeerAndSelfResult.Controls.Add(Me.EC_Score, 1, 7)
        Me.TablePeerAndSelfResult.Controls.Add(Me.RSPT_Score, 1, 6)
        Me.TablePeerAndSelfResult.Controls.Add(Me.CaT_Score, 1, 5)
        Me.TablePeerAndSelfResult.Controls.Add(Me.A_Score, 1, 4)
        Me.TablePeerAndSelfResult.Controls.Add(Me.ES_Score, 1, 3)
        Me.TablePeerAndSelfResult.Controls.Add(Me.R_Score, 1, 2)
        Me.TablePeerAndSelfResult.Controls.Add(Me.IAJ_Score, 1, 1)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label16, 1, 0)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label2, 0, 1)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label3, 0, 2)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label5, 0, 4)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label6, 0, 5)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label7, 0, 6)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label8, 0, 7)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label9, 0, 8)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label10, 0, 9)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label11, 0, 10)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label12, 0, 11)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label13, 0, 12)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label14, 0, 14)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label15, 0, 15)
        Me.TablePeerAndSelfResult.Controls.Add(Me.GT_Score, 1, 14)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Ave_Score, 1, 15)
        Me.TablePeerAndSelfResult.Controls.Add(Me.Label4, 0, 3)
        Me.TablePeerAndSelfResult.Location = New System.Drawing.Point(68, 270)
        Me.TablePeerAndSelfResult.Name = "TablePeerAndSelfResult"
        Me.TablePeerAndSelfResult.RowCount = 16
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978459!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.848119!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.978458!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.94223!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.560119!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.943241!))
        Me.TablePeerAndSelfResult.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.943241!))
        Me.TablePeerAndSelfResult.Size = New System.Drawing.Size(481, 414)
        Me.TablePeerAndSelfResult.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Parameters"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PACaH_Score
        '
        Me.PACaH_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PACaH_Score.AutoSize = True
        Me.PACaH_Score.Location = New System.Drawing.Point(159, 315)
        Me.PACaH_Score.Name = "PACaH_Score"
        Me.PACaH_Score.Size = New System.Drawing.Size(316, 13)
        Me.PACaH_Score.TabIndex = 27
        Me.PACaH_Score.Text = "0"
        Me.PACaH_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DA_Score
        '
        Me.DA_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DA_Score.AutoSize = True
        Me.DA_Score.Location = New System.Drawing.Point(159, 282)
        Me.DA_Score.Name = "DA_Score"
        Me.DA_Score.Size = New System.Drawing.Size(316, 13)
        Me.DA_Score.TabIndex = 26
        Me.DA_Score.Text = "0"
        Me.DA_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MCAtL_Score
        '
        Me.MCAtL_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MCAtL_Score.AutoSize = True
        Me.MCAtL_Score.Location = New System.Drawing.Point(159, 252)
        Me.MCAtL_Score.Name = "MCAtL_Score"
        Me.MCAtL_Score.Size = New System.Drawing.Size(316, 13)
        Me.MCAtL_Score.TabIndex = 25
        Me.MCAtL_Score.Text = "0"
        Me.MCAtL_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'P_Score
        '
        Me.P_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.P_Score.AutoSize = True
        Me.P_Score.Location = New System.Drawing.Point(159, 223)
        Me.P_Score.Name = "P_Score"
        Me.P_Score.Size = New System.Drawing.Size(316, 13)
        Me.P_Score.TabIndex = 24
        Me.P_Score.Text = "0"
        Me.P_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CR_Score
        '
        Me.CR_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CR_Score.AutoSize = True
        Me.CR_Score.Location = New System.Drawing.Point(159, 199)
        Me.CR_Score.Name = "CR_Score"
        Me.CR_Score.Size = New System.Drawing.Size(316, 13)
        Me.CR_Score.TabIndex = 23
        Me.CR_Score.Text = "0"
        Me.CR_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EC_Score
        '
        Me.EC_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EC_Score.AutoSize = True
        Me.EC_Score.Location = New System.Drawing.Point(159, 175)
        Me.EC_Score.Name = "EC_Score"
        Me.EC_Score.Size = New System.Drawing.Size(316, 13)
        Me.EC_Score.TabIndex = 22
        Me.EC_Score.Text = "0"
        Me.EC_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RSPT_Score
        '
        Me.RSPT_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RSPT_Score.AutoSize = True
        Me.RSPT_Score.Location = New System.Drawing.Point(159, 151)
        Me.RSPT_Score.Name = "RSPT_Score"
        Me.RSPT_Score.Size = New System.Drawing.Size(316, 13)
        Me.RSPT_Score.TabIndex = 21
        Me.RSPT_Score.Text = "0"
        Me.RSPT_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CaT_Score
        '
        Me.CaT_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CaT_Score.AutoSize = True
        Me.CaT_Score.Location = New System.Drawing.Point(159, 127)
        Me.CaT_Score.Name = "CaT_Score"
        Me.CaT_Score.Size = New System.Drawing.Size(316, 13)
        Me.CaT_Score.TabIndex = 20
        Me.CaT_Score.Text = "0"
        Me.CaT_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'A_Score
        '
        Me.A_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.A_Score.AutoSize = True
        Me.A_Score.Location = New System.Drawing.Point(159, 103)
        Me.A_Score.Name = "A_Score"
        Me.A_Score.Size = New System.Drawing.Size(316, 13)
        Me.A_Score.TabIndex = 19
        Me.A_Score.Text = "0"
        Me.A_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ES_Score
        '
        Me.ES_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ES_Score.AutoSize = True
        Me.ES_Score.Location = New System.Drawing.Point(159, 79)
        Me.ES_Score.Name = "ES_Score"
        Me.ES_Score.Size = New System.Drawing.Size(316, 13)
        Me.ES_Score.TabIndex = 18
        Me.ES_Score.Text = "0"
        Me.ES_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'R_Score
        '
        Me.R_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.R_Score.AutoSize = True
        Me.R_Score.Location = New System.Drawing.Point(159, 55)
        Me.R_Score.Name = "R_Score"
        Me.R_Score.Size = New System.Drawing.Size(316, 13)
        Me.R_Score.TabIndex = 17
        Me.R_Score.Text = "0"
        Me.R_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'IAJ_Score
        '
        Me.IAJ_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IAJ_Score.AutoSize = True
        Me.IAJ_Score.Location = New System.Drawing.Point(159, 31)
        Me.IAJ_Score.Name = "IAJ_Score"
        Me.IAJ_Score.Size = New System.Drawing.Size(316, 13)
        Me.IAJ_Score.TabIndex = 16
        Me.IAJ_Score.Text = "0"
        Me.IAJ_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(159, 7)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(316, 13)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Name"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Initiative and Judgement"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(144, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Responsibility"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(144, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Adaptability"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 127)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(144, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Cooperation and Teamwork"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 151)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Respect"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 175)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(144, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Extra-Curricular"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 199)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(144, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Customer Relations"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 223)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(144, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Punctuality"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 246)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 26)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Mental Capacity / Ability to Learn"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 282)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(144, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Deadline Awareness"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 302)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(144, 39)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Personal Appearance / Cleanliness and Housekeeping"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 360)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(144, 13)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Grand Total"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 389)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(144, 13)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Average"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GT_Score
        '
        Me.GT_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GT_Score.AutoSize = True
        Me.GT_Score.Location = New System.Drawing.Point(159, 360)
        Me.GT_Score.Name = "GT_Score"
        Me.GT_Score.Size = New System.Drawing.Size(316, 13)
        Me.GT_Score.TabIndex = 28
        Me.GT_Score.Text = "0"
        Me.GT_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Ave_Score
        '
        Me.Ave_Score.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Ave_Score.AutoSize = True
        Me.Ave_Score.Location = New System.Drawing.Point(159, 389)
        Me.Ave_Score.Name = "Ave_Score"
        Me.Ave_Score.Size = New System.Drawing.Size(316, 13)
        Me.Ave_Score.TabIndex = 29
        Me.Ave_Score.Text = "0"
        Me.Ave_Score.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Emotional Stability"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(178, 100)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(263, 13)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Technical Education and Skills Development Authority"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.OvalShape2, Me.OvalShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(621, 733)
        Me.ShapeContainer1.TabIndex = 8
        Me.ShapeContainer1.TabStop = False
        '
        'OvalShape2
        '
        Me.OvalShape2.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.tesda_logo_0
        Me.OvalShape2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.OvalShape2.Location = New System.Drawing.Point(461, 82)
        Me.OvalShape2.Name = "OvalShape2"
        Me.OvalShape2.Size = New System.Drawing.Size(90, 90)
        '
        'OvalShape1
        '
        Me.OvalShape1.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.logo500px_406x405
        Me.OvalShape1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.OvalShape1.Location = New System.Drawing.Point(71, 82)
        Me.OvalShape1.Name = "OvalShape1"
        Me.OvalShape1.Size = New System.Drawing.Size(90, 90)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(205, 115)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(202, 13)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "CONCEPCION VOCATIONAL SCHOOL"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Constantia", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(216, 130)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(161, 13)
        Me.Label19.TabIndex = 7
        Me.Label19.Text = "TRC Alfonso, Concepcion, Tarlac"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Constantia", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(178, 194)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(277, 14)
        Me.Label20.TabIndex = 9
        Me.Label20.Text = "SUMMARY - PERFORMANCE APPRAISAL SHEET"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(74, 254)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(101, 13)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "Evaluation Quarter :"
        '
        'EvaluationQuarterLabel
        '
        Me.EvaluationQuarterLabel.AutoSize = True
        Me.EvaluationQuarterLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EvaluationQuarterLabel.Location = New System.Drawing.Point(178, 254)
        Me.EvaluationQuarterLabel.Name = "EvaluationQuarterLabel"
        Me.EvaluationQuarterLabel.Size = New System.Drawing.Size(33, 13)
        Me.EvaluationQuarterLabel.TabIndex = 11
        Me.EvaluationQuarterLabel.Text = "Label"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Constantia", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(91, 687)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(428, 13)
        Me.Label23.TabIndex = 12
        Me.Label23.Text = "This is to verify that I fully understand the procedure and purpose of this evalu" & _
    "ation process."
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Constantia", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(65, 700)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(369, 13)
        Me.Label24.TabIndex = 13
        Me.Label24.Text = "Therefore, I hereby attest that I provided just, objective and truthful judgment." & _
    ""
        '
        'PrintLabel
        '
        Me.PrintLabel.AutoSize = True
        Me.PrintLabel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PrintLabel.LinkColor = System.Drawing.Color.Blue
        Me.PrintLabel.Location = New System.Drawing.Point(590, 9)
        Me.PrintLabel.Name = "PrintLabel"
        Me.PrintLabel.Size = New System.Drawing.Size(28, 13)
        Me.PrintLabel.TabIndex = 14
        Me.PrintLabel.TabStop = True
        Me.PrintLabel.Text = "Print"
        '
        'PrintForm1
        '
        Me.PrintForm1.DocumentName = "document"
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(313, 254)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(41, 13)
        Me.Label25.TabIndex = 15
        Me.Label25.Text = "Name :"
        '
        'NameLabel
        '
        Me.NameLabel.AutoSize = True
        Me.NameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NameLabel.Location = New System.Drawing.Point(360, 254)
        Me.NameLabel.Name = "NameLabel"
        Me.NameLabel.Size = New System.Drawing.Size(33, 13)
        Me.NameLabel.TabIndex = 16
        Me.NameLabel.Text = "Label"
        '
        'PeerAndSelfEvaluationResult
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(621, 733)
        Me.Controls.Add(Me.NameLabel)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.PrintLabel)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.EvaluationQuarterLabel)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TablePeerAndSelfResult)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PeerAndSelfEvaluationResult"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TransparencyKey = System.Drawing.Color.Gray
        Me.TablePeerAndSelfResult.ResumeLayout(False)
        Me.TablePeerAndSelfResult.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TablePeerAndSelfResult As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents PACaH_Score As System.Windows.Forms.Label
    Friend WithEvents DA_Score As System.Windows.Forms.Label
    Friend WithEvents MCAtL_Score As System.Windows.Forms.Label
    Friend WithEvents P_Score As System.Windows.Forms.Label
    Friend WithEvents CR_Score As System.Windows.Forms.Label
    Friend WithEvents EC_Score As System.Windows.Forms.Label
    Friend WithEvents RSPT_Score As System.Windows.Forms.Label
    Friend WithEvents CaT_Score As System.Windows.Forms.Label
    Friend WithEvents A_Score As System.Windows.Forms.Label
    Friend WithEvents ES_Score As System.Windows.Forms.Label
    Friend WithEvents R_Score As System.Windows.Forms.Label
    Friend WithEvents IAJ_Score As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GT_Score As System.Windows.Forms.Label
    Friend WithEvents Ave_Score As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents OvalShape2 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents OvalShape1 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents EvaluationQuarterLabel As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents PrintLabel As System.Windows.Forms.LinkLabel
    Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
    Friend WithEvents NameLabel As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
End Class
