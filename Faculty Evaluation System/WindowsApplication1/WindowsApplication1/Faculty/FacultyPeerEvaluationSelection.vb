﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class FacultyPeerEvaluationSelection
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim UserLogged As String
    Public Evaluating As String

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FacultyPeerEvaluationForm1.Show()
        Me.Hide()
    End Sub

    Private Sub Button_BackAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackAccount.Click
        FacultyEvaluationSelection.Show()
        Me.Hide()
    End Sub

    Private Sub FacultyPeerEvaluationSelection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        comboboxformload()
        'Mysqlconn = New MySqlConnection
        'Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        'Dim Reader As MySqlDataReader
        'Try
        '    Mysqlconn.Open()
        '    Dim query As String
        '    query = "select * from dbcvs.tblfaculty"
        '    Command = New MySqlCommand(query, Mysqlconn)
        '    Reader = Command.ExecuteReader
        '    While Reader.Read
        '        Dim Fname = Reader.GetString("First_Name")
        '        Dim LName = Reader.GetString("Last_Name")
        '        Combobox1.Items.Add(Fname + " " + LName)
        '    End While
        '    Mysqlconn.Close()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'Finally
        '    Mysqlconn.Dispose()
        'End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Evaluating = ComboBox1.Text
        comboboxchange()
    End Sub

    Sub comboboxformload()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim ID_Number = Reader.GetString("ID_Number")
                ComboBox1.Items.Add(ID_Number)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub comboboxchange()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty where ID_Number = '" & Evaluating & "'"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Fname = Reader.GetString("First_Name")
                Dim LName = Reader.GetString("Last_Name")
                Faculty_Name.Text = (Fname + " " + LName)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class