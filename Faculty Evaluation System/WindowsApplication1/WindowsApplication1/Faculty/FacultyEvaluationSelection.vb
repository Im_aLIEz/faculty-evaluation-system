﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class FacultyEvaluationSelection
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Public Username As String
    Dim selfeval As Integer
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SelfEvaluationForm1.Show()
        Me.Hide()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FacultyPeerEvaluationSelection.Show()
        Me.Hide()

    End Sub

    Private Sub Button_BackAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackAccount.Click
        FacultyForm.Show()
        Me.Hide()
    End Sub

    Private Sub FacultyEvaluationSelection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Username = FacultyForm.Username
        facultyName()
        selfcheck()
        If selfeval = 1 Then
            Button1.Enabled = False
        Else
            Button1.Enabled = True
        End If
    End Sub

    Sub selfcheck()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty where ID_Number = '" & Username & "'"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                selfeval = Reader.GetInt32("Evaluee_self")
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub facultyName()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty where ID_Number = '" & Username & "'"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Fname = Reader.GetString("First_Name")
                Dim LName = Reader.GetString("Last_Name")
                LblID.Text = (Fname + " " + LName)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class