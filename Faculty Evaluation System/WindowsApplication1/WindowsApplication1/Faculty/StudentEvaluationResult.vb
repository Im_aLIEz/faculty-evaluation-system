﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class StudentEvaluationResult
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Public ID As String
    Dim Avg As Double
    Dim evaluee As Integer
    Dim Get_Commitment As Integer
    Dim Get_KOSM As Integer
    Dim Get_TFIL As Integer
    Dim Get_MOL As Integer
    Dim avg_Commitment As Double
    Dim avg_KOSM As Double
    Dim avg_TFIL As Double
    Dim avg_MOL As Double
    Dim Result_Total As Double

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ResultForm.Show()
        Me.Close()
    End Sub
    Private Sub PrintLabel_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles PrintLabel.LinkClicked
        PrintLabel.Visible = False
        Me.PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintLabel.Visible = True
        Me.PrintForm1.Print()
    End Sub
    Private Sub StudentEvaluationResult_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ID = ResultForm.Username
        GetEvaluee()
        average_commitment()
        average_KOSM()
        average_mol()
        average_tfil()
        result_Commitment()
        result_KOSM()
        result_MOL()
        result_TFIL()
        Overall()
    End Sub

    Sub GetEvaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblfaculty Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                evaluee = Reader.GetInt32("Evaluee_std")
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub average_commitment()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_commitment Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_Commitment = Reader.GetInt32("std_commitmentDump")
            End While
            avg_Commitment = Get_Commitment / evaluee
            avg_Commitment = avg_Commitment / 5
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_KOSM()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_kosm Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_KOSM = Reader.GetInt32("std_kosmDump")
            End While
            avg_KOSM = Get_KOSM / evaluee
            avg_KOSM = avg_KOSM / 5
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_tfil()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_tfil Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_TFIL = Reader.GetInt32("std_tfilDump")
            End While
            avg_TFIL = Get_TFIL / evaluee
            avg_TFIL = avg_TFIL / 5
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub average_mol()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultstd_mol Where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Get_MOL = Reader.GetInt32("Std_MOLDump")
            End While
            avg_MOL = Get_MOL / evaluee
            avg_MOL = avg_MOL / 5
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub result_Commitment()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Std_CommitmentAvg from tblresultstd_commitment where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Commitment_Score.Text = avg_Commitment
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub result_KOSM()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Std_KOSMAvg from tblresultstd_KOSM where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                KOSM_Score.Text = avg_KOSM
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub result_TFIL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Std_TFILAvg from tblresultstd_TFIL where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                TFIL_Score.Text = avg_TFIL
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub result_MOL()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select Std_MOLavg from tblresultstd_MOL where ID_Number = '" & ID & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                MOL_Score.Text = avg_MOL
            End While
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Overall()
        Result_Total = avg_Commitment + avg_KOSM + avg_MOL + avg_TFIL
        Overall_Score.Text = Result_Total / 4
    End Sub
End Class