﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient

Public Class StudentForm
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim command2 As MySqlCommand
    Dim UserLogged As String
    Public Username
    Public Evaluating As String

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        StudentEvaluationForm1.Show()
        Me.Hide()
    End Sub
    Private Sub Button_BackAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BackAccount.Click
        LoginForm.Show()
        StudentEvaluationForm1.Close()
        StudentEvaluationForm2.Close()
        StudentEvaluationForm3.Close()
        StudentEvaluationForm4.Close()
        LoginForm.ID_Box.Text = ""
        LoginForm.Password_box.Text = ""
        Me.Close()
    End Sub

    Private Sub StudentForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim ID_Number = Reader.GetString("ID_Number")
                Combobox_Faculty.Items.Add(ID_Number)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        Subject()
        Username = LoginForm.Username
        studentName()
    End Sub

    Sub addDate()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update dbcvs.tblstudent set Evaluation_Date='" & Date_of_Evaluation.Text & "' where ID_Number = '" & UserLogged & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            MessageBox.Show("Data Saved")
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
        LblID.Text = LoginForm.Username
    End Sub
    Sub Subject()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from tblsubject"
            command = New MySqlCommand(query, Mysqlconn)
            reader = command.ExecuteReader
            While reader.Read
                Dim subject1 = reader.GetString("Subject")
                Dim subjectcode1 = reader.GetString("Subject_Code")
                Combobox_Subject.Items.Add(subject1 + " : " + subjectcode1)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub studentName()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblstudent where ID_Number = '" & Username & "'"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Fname = Reader.GetString("First_Name")
                Dim LName = Reader.GetString("Last_Name")
                LblID.Text = (Fname + " " + LName)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Private Sub Combobox_Faculty_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Combobox_Faculty.SelectedIndexChanged
        Evaluating = Combobox_Faculty.Text
        comboboxchange()
    End Sub
    Sub comboboxchange()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim query As String
            query = "select * from dbcvs.tblfaculty where ID_Number = '" & Evaluating & "'"
            command = New MySqlCommand(query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read
                Dim Fname = Reader.GetString("First_Name")
                Dim LName = Reader.GetString("Last_Name")
                Faculty_Name.Text = (Fname + " " + LName)
            End While
            Mysqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class