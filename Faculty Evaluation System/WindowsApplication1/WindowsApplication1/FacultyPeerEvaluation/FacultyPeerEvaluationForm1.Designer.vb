﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyPeerEvaluationForm1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FacultyPeerEvaluationForm1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Radio1_1 = New System.Windows.Forms.RadioButton()
        Me.Radio1_2 = New System.Windows.Forms.RadioButton()
        Me.Radio1_4 = New System.Windows.Forms.RadioButton()
        Me.Radio1_3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Radio2_1 = New System.Windows.Forms.RadioButton()
        Me.Radio2_2 = New System.Windows.Forms.RadioButton()
        Me.Radio2_4 = New System.Windows.Forms.RadioButton()
        Me.Radio2_3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Radio3_1 = New System.Windows.Forms.RadioButton()
        Me.Radio3_2 = New System.Windows.Forms.RadioButton()
        Me.Radio3_4 = New System.Windows.Forms.RadioButton()
        Me.Radio3_3 = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Radio1_1)
        Me.GroupBox1.Controls.Add(Me.Radio1_2)
        Me.GroupBox1.Controls.Add(Me.Radio1_4)
        Me.GroupBox1.Controls.Add(Me.Radio1_3)
        Me.GroupBox1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(37, 23)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(941, 155)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Initiative and Judgement ( Active attempts to influence events to achieve goals )" & _
    ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(73, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(474, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "- Less than 75% of the time carries out tasks without further instructions"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(702, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "- More than 75% of the time carries out tasks without further instructions; initi" & _
    "ates necessary details of work"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(814, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "- Usually or 90% of the time carries out tasks without further instructions; init" & _
    "iates necessary details of work without being told" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(73, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(347, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "- Always carries out tasks without further instructions"
        '
        'Radio1_1
        '
        Me.Radio1_1.AutoSize = True
        Me.Radio1_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_1.Location = New System.Drawing.Point(36, 113)
        Me.Radio1_1.Name = "Radio1_1"
        Me.Radio1_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_1.TabIndex = 0
        Me.Radio1_1.Text = "1"
        Me.Radio1_1.UseVisualStyleBackColor = True
        '
        'Radio1_2
        '
        Me.Radio1_2.AutoSize = True
        Me.Radio1_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_2.Location = New System.Drawing.Point(36, 90)
        Me.Radio1_2.Name = "Radio1_2"
        Me.Radio1_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_2.TabIndex = 1
        Me.Radio1_2.Text = "2"
        Me.Radio1_2.UseVisualStyleBackColor = True
        '
        'Radio1_4
        '
        Me.Radio1_4.AutoSize = True
        Me.Radio1_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_4.Location = New System.Drawing.Point(36, 44)
        Me.Radio1_4.Name = "Radio1_4"
        Me.Radio1_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_4.TabIndex = 3
        Me.Radio1_4.Text = "4"
        Me.Radio1_4.UseVisualStyleBackColor = True
        '
        'Radio1_3
        '
        Me.Radio1_3.AutoSize = True
        Me.Radio1_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio1_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio1_3.Location = New System.Drawing.Point(36, 67)
        Me.Radio1_3.Name = "Radio1_3"
        Me.Radio1_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio1_3.TabIndex = 2
        Me.Radio1_3.Text = "3"
        Me.Radio1_3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Radio2_1)
        Me.GroupBox2.Controls.Add(Me.Radio2_2)
        Me.GroupBox2.Controls.Add(Me.Radio2_4)
        Me.GroupBox2.Controls.Add(Me.Radio2_3)
        Me.GroupBox2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(36, 183)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(941, 155)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Responsibility ( Willingness to take on additional responsibilities )"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(73, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(760, 20)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "- Never volunteers to undertake work. Requires constant prodding to do work. Will" & _
    " not accept additional responsibility."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(73, 92)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(540, 20)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "- Needs some prodding to do work. Is not a self-starter. Does just enough to get " & _
    "by."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(73, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(474, 20)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "- Will accept new responsibility when necessary. Is a good routine worker."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(73, 46)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(528, 20)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "- Is a self-starter. Works well when given responsibility. Does more than expecte" & _
    "d."
        '
        'Radio2_1
        '
        Me.Radio2_1.AutoSize = True
        Me.Radio2_1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Radio2_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio2_1.Location = New System.Drawing.Point(36, 113)
        Me.Radio2_1.Name = "Radio2_1"
        Me.Radio2_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio2_1.TabIndex = 0
        Me.Radio2_1.Text = "1"
        Me.Radio2_1.UseVisualStyleBackColor = True
        '
        'Radio2_2
        '
        Me.Radio2_2.AutoSize = True
        Me.Radio2_2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Radio2_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio2_2.Location = New System.Drawing.Point(36, 90)
        Me.Radio2_2.Name = "Radio2_2"
        Me.Radio2_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio2_2.TabIndex = 1
        Me.Radio2_2.Text = "2"
        Me.Radio2_2.UseVisualStyleBackColor = True
        '
        'Radio2_4
        '
        Me.Radio2_4.AutoSize = True
        Me.Radio2_4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Radio2_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio2_4.Location = New System.Drawing.Point(36, 44)
        Me.Radio2_4.Name = "Radio2_4"
        Me.Radio2_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio2_4.TabIndex = 3
        Me.Radio2_4.Text = "4"
        Me.Radio2_4.UseVisualStyleBackColor = True
        '
        'Radio2_3
        '
        Me.Radio2_3.AutoSize = True
        Me.Radio2_3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Radio2_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio2_3.Location = New System.Drawing.Point(36, 67)
        Me.Radio2_3.Name = "Radio2_3"
        Me.Radio2_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio2_3.TabIndex = 2
        Me.Radio2_3.Text = "3"
        Me.Radio2_3.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Radio3_1)
        Me.GroupBox3.Controls.Add(Me.Radio3_2)
        Me.GroupBox3.Controls.Add(Me.Radio3_4)
        Me.GroupBox3.Controls.Add(Me.Radio3_3)
        Me.GroupBox3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(37, 344)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(941, 155)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Emotional Stability"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(73, 115)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(333, 20)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "- Behaves immaturely; lacking frustration tolerance"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(73, 92)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(630, 20)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "- Needs improvements; changeably in attitudes; occasionally resorts to children b" & _
    "ehavior (childish)"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(73, 69)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(705, 20)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "- Understands his/her weaknesses and strong points and can discuss them with obje" & _
    "ctivity; realistic and calm"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(73, 34)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(775, 40)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "- Exceptional self-insight objectivity. adaptability has complete understanding o" & _
    "f self and can realistically suggest ways of " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "self-improvements"
        '
        'Radio3_1
        '
        Me.Radio3_1.AutoSize = True
        Me.Radio3_1.Checked = True
        Me.Radio3_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio3_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio3_1.Location = New System.Drawing.Point(36, 113)
        Me.Radio3_1.Name = "Radio3_1"
        Me.Radio3_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio3_1.TabIndex = 0
        Me.Radio3_1.TabStop = True
        Me.Radio3_1.Text = "1"
        Me.Radio3_1.UseVisualStyleBackColor = True
        '
        'Radio3_2
        '
        Me.Radio3_2.AutoSize = True
        Me.Radio3_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio3_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio3_2.Location = New System.Drawing.Point(36, 90)
        Me.Radio3_2.Name = "Radio3_2"
        Me.Radio3_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio3_2.TabIndex = 1
        Me.Radio3_2.Text = "2"
        Me.Radio3_2.UseVisualStyleBackColor = True
        '
        'Radio3_4
        '
        Me.Radio3_4.AutoSize = True
        Me.Radio3_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio3_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio3_4.Location = New System.Drawing.Point(35, 30)
        Me.Radio3_4.Name = "Radio3_4"
        Me.Radio3_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio3_4.TabIndex = 3
        Me.Radio3_4.Text = "4"
        Me.Radio3_4.UseVisualStyleBackColor = True
        '
        'Radio3_3
        '
        Me.Radio3_3.AutoSize = True
        Me.Radio3_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio3_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio3_3.Location = New System.Drawing.Point(36, 67)
        Me.Radio3_3.Name = "Radio3_3"
        Me.Radio3_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio3_3.TabIndex = 2
        Me.Radio3_3.Text = "3"
        Me.Radio3_3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.SandyBrown
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(923, 571)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 29)
        Me.Button1.TabIndex = 39
        Me.Button1.Text = "Next"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.SandyBrown
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(12, 571)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 29)
        Me.Button2.TabIndex = 40
        Me.Button2.Text = "Previous"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(473, 565)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 34)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "1 of 4"
        '
        'FacultyPeerEvaluationForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyPeerEvaluationForm1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Radio1_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio1_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Radio2_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio2_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio2_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio2_3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Radio3_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio3_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio3_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio3_3 As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
