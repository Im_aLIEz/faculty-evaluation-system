﻿Public Class FacultyPeerEvaluationForm1
    Public Rating_IAJ As Integer
    Public Rating_Responsibility As Integer
    Public Rating_ES As Integer

    Private Sub Radio1_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio1_4.CheckedChanged
        Rating_IAJ = 4
    End Sub
    Private Sub Radio1_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio1_3.CheckedChanged
        Rating_IAJ = 3
    End Sub
    Private Sub Radio1_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio1_2.CheckedChanged
        Rating_IAJ = 2
    End Sub
    Private Sub Radio1_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio1_1.CheckedChanged
        Rating_IAJ = 1
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FacultyPeerEvaluationForm2.Show()
        Me.Hide()
    End Sub
    Private Sub Radio2_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio2_1.CheckedChanged
        Rating_Responsibility = 1
    End Sub
    Private Sub Radio2_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio2_2.CheckedChanged
        Rating_Responsibility = 2
    End Sub
    Private Sub Radio2_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio2_3.CheckedChanged
        Rating_Responsibility = 3
    End Sub
    Private Sub Radio2_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio2_4.CheckedChanged
        Rating_Responsibility = 4
    End Sub
    Private Sub Radio3_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio3_1.CheckedChanged
        Rating_ES = 1
    End Sub
    Private Sub Radio3_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio3_2.CheckedChanged
        Rating_ES = 2
    End Sub
    Private Sub Radio3_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio3_3.CheckedChanged
        Rating_ES = 3
    End Sub
    Private Sub Radio3_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio3_4.CheckedChanged
        Rating_ES = 4
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FacultyPeerEvaluationSelection.Show()
        FacultyPeerEvaluationForm2.Close()
        FacultyPeerEvaluationForm3.Close()
        FacultyPeerEvaluationForm4.Close()
        Me.Close()
    End Sub
End Class