﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Public Class FacultyPeerEvaluationForm4
    Public Rating_MC_ATL As Integer
    Public Rating_DA As Integer
    Public Rating_PA_CAH As Integer
    Dim evaluee As Integer
    Dim IAJ As Integer
    Dim Responsibility As Integer
    Dim ES As Integer
    Dim Adaptability As Integer
    Dim COT As Integer
    Dim Respect As Integer
    Dim EC As Integer
    Dim CR As Integer
    Dim Punctuality As Integer
    Dim Mysqlconn As MySqlConnection
    Dim command As MySqlCommand
    Dim get_iaj As Integer
    Dim get_responsibility As Integer
    Dim get_es As Integer
    Dim get_adaptability As Integer
    Dim get_cot As Integer
    Dim get_respect As Integer
    Dim get_ec As Integer
    Dim get_cr As Integer
    Dim get_punctuality As Integer
    Dim get_mc_atl As Integer
    Dim get_da As Integer
    Dim get_pa_cah As Integer

    'IAJ = FacultyPeerEvaluationForm1.Rating_IAJ
    '    Responsibility = FacultyPeerEvaluationForm1.Rating_Responsibility
    '    ES = FacultyPeerEvaluationForm1.Rating_ES
    '    Adaptability = FacultyPeerEvaluationForm2.Rating_Adaptability
    '    COT = FacultyPeerEvaluationForm2.Rating_CoT
    '    Respect = FacultyPeerEvaluationForm2.Rating_Respect
    '    EC = FacultyPeerEvaluationForm3.Rating_EC
    '    CR = FacultyPeerEvaluationForm3.Rating_CR
    '    Punctuality = FacultyPeerEvaluationForm3.Rating_Punctuality

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IAJ = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Responsibility = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf ES = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Adaptability = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf COT = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Respect = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf EC = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf CR = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Punctuality = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Rating_DA = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Rating_MC_ATL = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        ElseIf Rating_PA_CAH = 0 Then
            MsgBox("Please Fill up every parameter to submit.")
        Else
            GetEvaluee()
            setevaluee()
            Dump_adaptability()
            Dump_cot()
            Dump_cr()
            Dump_da()
            Dump_ec()
            Dump_es()
            Dump_iaj()
            Dump_mc_atl()
            Dump_pa_cah()
            Dump_punctuality()
            Dump_respect()
            Dump_responsibility()
            Update_iaj()
            Update_responsibility()
            Update_es()
            Update_adaptability()
            Update_cot()
            Update_respect()
            Update_ec()
            Update_cr()
            Update_punctuality()
            Update_mc_atl()
            Update_da()
            Update_pa_cah()
            MsgBox("Thank you for Participating in the Evaluation, the results will be subjected for Review.")
            FacultyForm.Refresh()
            FacultyEvaluationSelection.Close()
            FacultyPeerEvaluationSelection.Close()
            ResultForm.Close()
            StudentEvaluationResult.Close()
            FacultyForm.Show()
            Me.Close()
        End If
    End Sub
    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FacultyPeerEvaluationForm3.Show()
        Me.Hide()
    End Sub

    Private Sub Radio10_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_1.CheckedChanged
        Rating_MC_ATL = 1
    End Sub

    Private Sub Radio10_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_4.CheckedChanged
        Rating_MC_ATL = 4
    End Sub

    Private Sub Radio10_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_3.CheckedChanged
        Rating_MC_ATL = 3
    End Sub

    Private Sub Radio10_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio10_2.CheckedChanged
        Rating_MC_ATL = 2
    End Sub

    Private Sub Radio11_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_1.CheckedChanged
        Rating_DA = 1
    End Sub

    Private Sub Radio11_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_2.CheckedChanged
        Rating_DA = 2
    End Sub

    Private Sub Radio11_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_3.CheckedChanged
        Rating_DA = 3
    End Sub

    Private Sub Radio11_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio11_4.CheckedChanged
        Rating_DA = 4
    End Sub

    Private Sub Radio12_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_1.CheckedChanged
        Rating_PA_CAH = 1
    End Sub

    Private Sub Radio12_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_2.CheckedChanged
        Rating_PA_CAH = 2
    End Sub

    Private Sub Radio12_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_3.CheckedChanged
        Rating_PA_CAH = 3
    End Sub

    Private Sub Radio12_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio12_4.CheckedChanged
        Rating_PA_CAH = 4
    End Sub

    Private Sub FacultyPeerEvaluationForm4_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IAJ = FacultyPeerEvaluationForm1.Rating_IAJ
        Responsibility = FacultyPeerEvaluationForm1.Rating_Responsibility
        ES = FacultyPeerEvaluationForm1.Rating_ES
        Adaptability = FacultyPeerEvaluationForm2.Rating_Adaptability
        COT = FacultyPeerEvaluationForm2.Rating_CoT
        Respect = FacultyPeerEvaluationForm2.Rating_Respect
        EC = FacultyPeerEvaluationForm3.Rating_EC
        CR = FacultyPeerEvaluationForm3.Rating_CR
        Punctuality = FacultyPeerEvaluationForm3.Rating_Punctuality

    End Sub
    Sub GetEvaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblfaculty Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                Evaluee = Reader.GetInt32("Evaluee_fclt")
            End While
            Evaluee = Evaluee + 1
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub setevaluee()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblfaculty set evaluee_fclt='" & evaluee & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub Dump_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_iaj Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_iaj = Reader.GetInt32("Faculty_IAJDump")
            End While
            get_iaj = get_iaj + IAJ
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_responsibility Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_responsibility = Reader.GetInt32("Faculty_responsibilityDump")
            End While
            get_responsibility = get_responsibility + Responsibility
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_es Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_es = Reader.GetInt32("Faculty_esDump")
            End While
            get_es = get_es + ES
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_adaptability Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_adaptability = Reader.GetInt32("Faculty_adaptabilityDump")
            End While
            get_adaptability = get_adaptability + Adaptability
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_cot()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cat Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_cot = Reader.GetInt32("Faculty_catDump")
            End While
            get_cot = get_cot + COT
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_respect Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_respect = Reader.GetInt32("Faculty_respectDump")
            End While
            get_respect = get_respect + Respect
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_ec Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_ec = Reader.GetInt32("Faculty_ecDump")
            End While
            get_ec = get_ec + EC
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_cr Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_cr = Reader.GetInt32("Faculty_crDump")
            End While
            get_cr = get_cr + CR
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_punctuality Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_punctuality = Reader.GetInt32("Faculty_punctualityDump")
            End While
            get_punctuality = get_punctuality + Punctuality
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_mc_atl Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_mc_atl = Reader.GetInt32("Faculty_mc_atlDump")
            End While
            get_mc_atl = get_mc_atl + Rating_MC_ATL
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_da Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_da = Reader.GetInt32("Faculty_daDump")
            End While
            get_da = get_da + Rating_DA
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Dump_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Select * from tblresultfaculty_pa_cah Where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            While Reader.Read()
                get_pa_cah = Reader.GetInt32("Faculty_pa_cahDump")
            End While
            get_pa_cah = get_pa_cah + Rating_PA_CAH
            ' T_iaj = P_iaj / Evaluee
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub

    Sub Update_iaj()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_iaj set Faculty_IAJDump='" & get_iaj & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_responsibility()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_responsibility set Faculty_responsibilityDump='" & get_responsibility & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_es()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_es set Faculty_esDump='" & get_es & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_adaptability()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_adaptability set Faculty_adaptabilityDump='" & get_adaptability & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_cot()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_cat set Faculty_catDump='" & get_cot & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_respect()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_respect set Faculty_respectDump='" & get_respect & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_ec()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_ec set Faculty_ecDump='" & get_ec & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_cr()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_cr set Faculty_crDump='" & get_cr & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_punctuality()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_punctuality set Faculty_punctualityDump='" & get_punctuality & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_mc_atl()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_mc_atl set Faculty_mc_atlDump='" & get_mc_atl & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_da()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_da set Faculty_daDump='" & get_da & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
    Sub Update_pa_cah()
        Mysqlconn = New MySqlConnection
        Mysqlconn.ConnectionString = "server=localhost;userid=root;database=dbcvs"
        Dim Reader As MySqlDataReader
        Try
            Mysqlconn.Open()
            Dim Query As String
            Query = "Update tblresultfaculty_pa_cah set Faculty_pa_cahDump='" & get_pa_cah & "' where ID_Number = '" & FacultyPeerEvaluationSelection.Evaluating & "'"
            command = New MySqlCommand(Query, Mysqlconn)
            Reader = command.ExecuteReader
            Mysqlconn.Close()
        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        Finally
            Mysqlconn.Dispose()
        End Try
    End Sub
End Class