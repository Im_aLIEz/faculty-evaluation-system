﻿Public Class FacultyPeerEvaluationForm2
    Public Rating_Adaptability As Integer
    Public Rating_CoT As Integer
    Public Rating_Respect As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FacultyPeerEvaluationForm3.Show()
        Me.Hide()
    End Sub
    Private Sub BackButton_Click(sender As System.Object, e As System.EventArgs) Handles BackButton.Click
        FacultyPeerEvaluationForm1.Show()
        Me.Hide()
    End Sub
    
    Private Sub Radio4_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_4.CheckedChanged
        Rating_Adaptability = 4
    End Sub
    Private Sub Radio4_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_2.CheckedChanged
        Rating_Adaptability = 2
    End Sub
    Private Sub Radio4_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_3.CheckedChanged
        Rating_Adaptability = 3
    End Sub
    Private Sub Radio4_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio4_1.CheckedChanged
        Rating_Adaptability = 1
    End Sub

    Private Sub Radio5_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_4.CheckedChanged
        Rating_CoT = 4
    End Sub

    Private Sub Radio5_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_2.CheckedChanged
        Rating_CoT = 2
    End Sub

    Private Sub Radio5_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_3.CheckedChanged
        Rating_CoT = 3
    End Sub

    Private Sub Radio5_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio5_1.CheckedChanged
        Rating_CoT = 1
    End Sub

    Private Sub Radio6_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_1.CheckedChanged
        Rating_Respect = 1
    End Sub

    Private Sub Radio6_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_2.CheckedChanged
        Rating_Respect = 2
    End Sub

    Private Sub Radio6_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_3.CheckedChanged
        Rating_Respect = 3
    End Sub

    Private Sub Radio6_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio6_4.CheckedChanged
        Rating_Respect = 4
    End Sub
End Class