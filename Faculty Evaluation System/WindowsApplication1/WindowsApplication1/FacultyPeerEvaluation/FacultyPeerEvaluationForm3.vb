﻿Public Class FacultyPeerEvaluationForm3
    Public Rating_EC As Integer
    Public Rating_CR As Integer
    Public Rating_Punctuality As Integer

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FacultyPeerEvaluationForm4.Show()
        Me.Hide()
    End Sub
    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FacultyPeerEvaluationForm2.Show()
        Me.Hide()
    End Sub

    Private Sub Radio7_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_1.CheckedChanged
        Rating_EC = 1
    End Sub

    Private Sub Radio7_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_2.CheckedChanged
        Rating_EC = 2
    End Sub

    Private Sub Radio7_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_3.CheckedChanged
        Rating_EC = 3
    End Sub

    Private Sub Radio7_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio7_4.CheckedChanged
        Rating_EC = 4
    End Sub

    Private Sub Radio8_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_1.CheckedChanged
        Rating_CR = 1
    End Sub

    Private Sub Radio8_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_2.CheckedChanged
        Rating_CR = 2
    End Sub

    Private Sub Radio8_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_3.CheckedChanged
        Rating_CR = 3
    End Sub

    Private Sub Radio8_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio8_4.CheckedChanged
        Rating_CR = 4
    End Sub

    Private Sub Radio9_1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_1.CheckedChanged
        Rating_Punctuality = 1
    End Sub

    Private Sub Radio9_2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_2.CheckedChanged
        Rating_Punctuality = 2
    End Sub

    Private Sub Radio9_3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_3.CheckedChanged
        Rating_Punctuality = 3
    End Sub

    Private Sub Radio9_4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Radio9_4.CheckedChanged
        Rating_Punctuality = 4
    End Sub
End Class