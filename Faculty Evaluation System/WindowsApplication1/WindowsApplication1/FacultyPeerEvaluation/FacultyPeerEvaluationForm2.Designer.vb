﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyPeerEvaluationForm2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Radio6_1 = New System.Windows.Forms.RadioButton()
        Me.Radio6_2 = New System.Windows.Forms.RadioButton()
        Me.Radio6_4 = New System.Windows.Forms.RadioButton()
        Me.Radio6_3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Radio5_1 = New System.Windows.Forms.RadioButton()
        Me.Radio5_2 = New System.Windows.Forms.RadioButton()
        Me.Radio5_4 = New System.Windows.Forms.RadioButton()
        Me.Radio5_3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Radio4_1 = New System.Windows.Forms.RadioButton()
        Me.Radio4_2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Radio4_4 = New System.Windows.Forms.RadioButton()
        Me.Radio4_3 = New System.Windows.Forms.RadioButton()
        Me.BackButton = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.SandyBrown
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(923, 571)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 29)
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "Next"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Radio6_1)
        Me.GroupBox3.Controls.Add(Me.Radio6_2)
        Me.GroupBox3.Controls.Add(Me.Radio6_4)
        Me.GroupBox3.Controls.Add(Me.Radio6_3)
        Me.GroupBox3.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(36, 344)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(942, 155)
        Me.GroupBox3.TabIndex = 42
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Respect ( Courtesy and commitment to service to everybody e.g. customers, superri" & _
    "ors, co-workers )"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(73, 103)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(424, 20)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "- Tends to act and speak roughly; shows bad temper and attitude"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(73, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(510, 20)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "- More then 75% of the time respectful in speech and action toward everybody"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(73, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(379, 20)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "- Always respectful in speech and action toward everybody"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(73, 36)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(619, 20)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "- Actively encourages others to be respectful and well-behaved; always a model of" & _
    " good conduct"
        '
        'Radio6_1
        '
        Me.Radio6_1.AutoSize = True
        Me.Radio6_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio6_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio6_1.Location = New System.Drawing.Point(36, 101)
        Me.Radio6_1.Name = "Radio6_1"
        Me.Radio6_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio6_1.TabIndex = 0
        Me.Radio6_1.Text = "1"
        Me.Radio6_1.UseVisualStyleBackColor = True
        '
        'Radio6_2
        '
        Me.Radio6_2.AutoSize = True
        Me.Radio6_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio6_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio6_2.Location = New System.Drawing.Point(36, 78)
        Me.Radio6_2.Name = "Radio6_2"
        Me.Radio6_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio6_2.TabIndex = 1
        Me.Radio6_2.Text = "2"
        Me.Radio6_2.UseVisualStyleBackColor = True
        '
        'Radio6_4
        '
        Me.Radio6_4.AutoSize = True
        Me.Radio6_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio6_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio6_4.Location = New System.Drawing.Point(36, 34)
        Me.Radio6_4.Name = "Radio6_4"
        Me.Radio6_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio6_4.TabIndex = 3
        Me.Radio6_4.Text = "4"
        Me.Radio6_4.UseVisualStyleBackColor = True
        '
        'Radio6_3
        '
        Me.Radio6_3.AutoSize = True
        Me.Radio6_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio6_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio6_3.Location = New System.Drawing.Point(36, 55)
        Me.Radio6_3.Name = "Radio6_3"
        Me.Radio6_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio6_3.TabIndex = 2
        Me.Radio6_3.Text = "3"
        Me.Radio6_3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Radio5_1)
        Me.GroupBox2.Controls.Add(Me.Radio5_2)
        Me.GroupBox2.Controls.Add(Me.Radio5_4)
        Me.GroupBox2.Controls.Add(Me.Radio5_3)
        Me.GroupBox2.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(36, 183)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(942, 155)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cooperation and Teamwork"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(73, 119)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(844, 20)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "- Rarely maintains an effective working relationship and openness to others' view" & _
    "s; rarely contributes to building positive team spirit"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(65, 82)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(848, 40)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "  - Sometimes maintains an effective working relationship and openness to others'" & _
    " views; sometimes contributes to building positive" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "   team spirit"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(72, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(857, 20)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "- Usually maintains an effective working relationship and openness to others' vie" & _
    "ws; usually contributes to building positive team spirit"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(73, 38)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(854, 20)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "- Always maintains an effective working relationship and openness to others' view" & _
    "s; always contributes to building positive team spirit"
        '
        'Radio5_1
        '
        Me.Radio5_1.AutoSize = True
        Me.Radio5_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio5_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio5_1.Location = New System.Drawing.Point(36, 117)
        Me.Radio5_1.Name = "Radio5_1"
        Me.Radio5_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio5_1.TabIndex = 0
        Me.Radio5_1.Text = "1"
        Me.Radio5_1.UseVisualStyleBackColor = True
        '
        'Radio5_2
        '
        Me.Radio5_2.AutoSize = True
        Me.Radio5_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio5_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio5_2.Location = New System.Drawing.Point(35, 82)
        Me.Radio5_2.Name = "Radio5_2"
        Me.Radio5_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio5_2.TabIndex = 1
        Me.Radio5_2.Text = "2"
        Me.Radio5_2.UseVisualStyleBackColor = True
        '
        'Radio5_4
        '
        Me.Radio5_4.AutoSize = True
        Me.Radio5_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio5_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio5_4.Location = New System.Drawing.Point(36, 38)
        Me.Radio5_4.Name = "Radio5_4"
        Me.Radio5_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio5_4.TabIndex = 3
        Me.Radio5_4.Text = "4"
        Me.Radio5_4.UseVisualStyleBackColor = True
        '
        'Radio5_3
        '
        Me.Radio5_3.AutoSize = True
        Me.Radio5_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio5_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio5_3.Location = New System.Drawing.Point(35, 61)
        Me.Radio5_3.Name = "Radio5_3"
        Me.Radio5_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio5_3.TabIndex = 2
        Me.Radio5_3.Text = "3"
        Me.Radio5_3.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Radio4_1)
        Me.GroupBox1.Controls.Add(Me.Radio4_2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.Radio4_4)
        Me.GroupBox1.Controls.Add(Me.Radio4_3)
        Me.GroupBox1.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(36, 22)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(942, 155)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Adaptablity ( Job performance under pressure and adaptability to changes in the w" & _
    "ork environment )"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(73, 105)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(578, 40)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "- Cannot performs well under pressure; cannot adapt in changes in the work enviro" & _
    "nment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(657, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "- Sometimes performs well under pressure; can sometimes adapt in changes in the w" & _
    "ork environment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(602, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "- Usually performs well under pressure; can usually adapt in changes in the work " & _
    "environment"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(73, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(605, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "- Always performs well under pressure; can always adapt to changes in the work en" & _
    "vironment."
        '
        'Radio4_1
        '
        Me.Radio4_1.AutoSize = True
        Me.Radio4_1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio4_1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio4_1.Location = New System.Drawing.Point(36, 103)
        Me.Radio4_1.Name = "Radio4_1"
        Me.Radio4_1.Size = New System.Drawing.Size(36, 24)
        Me.Radio4_1.TabIndex = 0
        Me.Radio4_1.Text = "1"
        Me.Radio4_1.UseVisualStyleBackColor = True
        '
        'Radio4_2
        '
        Me.Radio4_2.AutoSize = True
        Me.Radio4_2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio4_2.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio4_2.Location = New System.Drawing.Point(36, 80)
        Me.Radio4_2.Name = "Radio4_2"
        Me.Radio4_2.Size = New System.Drawing.Size(36, 24)
        Me.Radio4_2.TabIndex = 1
        Me.Radio4_2.Text = "2"
        Me.Radio4_2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(87, 55)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(36, 24)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.Text = "4"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Radio4_4
        '
        Me.Radio4_4.AutoSize = True
        Me.Radio4_4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio4_4.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio4_4.Location = New System.Drawing.Point(36, 34)
        Me.Radio4_4.Name = "Radio4_4"
        Me.Radio4_4.Size = New System.Drawing.Size(36, 24)
        Me.Radio4_4.TabIndex = 3
        Me.Radio4_4.Text = "4"
        Me.Radio4_4.UseVisualStyleBackColor = True
        '
        'Radio4_3
        '
        Me.Radio4_3.AutoSize = True
        Me.Radio4_3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Radio4_3.Font = New System.Drawing.Font("Franklin Gothic Book", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Radio4_3.Location = New System.Drawing.Point(36, 57)
        Me.Radio4_3.Name = "Radio4_3"
        Me.Radio4_3.Size = New System.Drawing.Size(36, 24)
        Me.Radio4_3.TabIndex = 2
        Me.Radio4_3.Text = "3"
        Me.Radio4_3.UseVisualStyleBackColor = True
        '
        'BackButton
        '
        Me.BackButton.BackColor = System.Drawing.Color.SandyBrown
        Me.BackButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BackButton.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BackButton.Location = New System.Drawing.Point(12, 571)
        Me.BackButton.Name = "BackButton"
        Me.BackButton.Size = New System.Drawing.Size(83, 29)
        Me.BackButton.TabIndex = 44
        Me.BackButton.Text = "Previous"
        Me.BackButton.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Franklin Gothic Medium Cond", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(473, 565)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 34)
        Me.Label13.TabIndex = 45
        Me.Label13.Text = "2 of 4"
        '
        'FacultyPeerEvaluationForm2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.BackgroundImage = Global.Faculty_Evaluation_System.My.Resources.Resources.login1
        Me.ClientSize = New System.Drawing.Size(1018, 612)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.BackButton)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyPeerEvaluationForm2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FacultyPeerEvaluationForm2"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Radio6_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio6_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio6_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio6_3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Radio5_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio5_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio5_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio5_3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Radio4_1 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio4_2 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio4_4 As System.Windows.Forms.RadioButton
    Friend WithEvents Radio4_3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents BackButton As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
