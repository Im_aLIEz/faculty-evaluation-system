-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2018 at 11:10 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbcvs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblaccount`
--

CREATE TABLE `tblaccount` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `Priv_Lvl` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblaccount`
--

INSERT INTO `tblaccount` (`ID`, `ID_Number`, `Password`, `Priv_Lvl`) VALUES
(35, '1998-2778', 'faculty', 'Faculty'),
(36, '1442-3488', 'faculty', 'Faculty'),
(37, '1534-1123', 'faculty', 'Faculty'),
(38, '1999-2776', 'faculty', 'Faculty'),
(39, '017-155-2017', 'student', 'Student'),
(40, '1990-2022', 'faculty', 'Faculty'),
(41, '2103-4122', 'faculty', 'Faculty'),
(42, '1997-0810', 'faculty', 'Faculty'),
(43, '2001-0518', 'faculty', 'Faculty'),
(44, '2316-2213', 'faculty', 'Faculty'),
(46, '1990-1201', 'faculty', 'Faculty'),
(47, 'cvsadmin', 'cvsadmin', 'Admin'),
(48, '018-213-2017', 'student', 'Student'),
(49, '017-535-2017', 'student', 'Student'),
(50, '018-565-2017', 'student', 'Student'),
(51, '018-522-2017', 'student', 'Student'),
(53, '015-200-2017', 'student', 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `tblfaculty`
--

CREATE TABLE `tblfaculty` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Last_Name` varchar(30) NOT NULL,
  `First_Name` varchar(50) NOT NULL,
  `Position` varchar(50) NOT NULL,
  `Section` varchar(20) NOT NULL,
  `Evaluation_Score` varchar(10) NOT NULL,
  `Evaluation_Date` varchar(60) NOT NULL,
  `Evaluee_std` int(20) NOT NULL,
  `Evaluee_fclt` int(20) NOT NULL,
  `Evaluee_self` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblfaculty`
--

INSERT INTO `tblfaculty` (`ID`, `ID_Number`, `Last_Name`, `First_Name`, `Position`, `Section`, `Evaluation_Score`, `Evaluation_Date`, `Evaluee_std`, `Evaluee_fclt`, `Evaluee_self`) VALUES
(1, '1998-2778', 'Paz', 'Aileen', 'Instructor-I', 'Section-a', '0', '0000-00-00', 0, 1, 1),
(6, '1442-3488', 'Talisik', 'Consolacion', '', '', '', '0000-00-00', 0, 1, 1),
(7, '1534-1123', 'Llorente', 'Lerma', '', '', '', '0000-00-00', 0, 0, 0),
(8, '1999-2776', 'Taborlupa', 'Irene', '', '', '', '0000-00-00', 0, 0, 0),
(9, '1990-2022', 'Briones', 'Rico', '', '', '', '0000-00-00', 0, 0, 0),
(10, '2103-4122', 'Felix', 'Marlon', '', '', '', '0000-00-00', 0, 0, 0),
(11, '1997-0810', 'Ocasion', 'Mary Grace', '', '', '', '0000-00-00', 0, 0, 0),
(12, '2001-0518', 'Flores', 'Raymond', '', '', '', '0000-00-00', 0, 0, 0),
(13, '2316-2213', 'Mola', 'Carlos', '', '', '', '0000-00-00', 0, 1, 1),
(14, '1990-1201', 'Yturralde', 'Alvin', '', '', '', '0000-00-00', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblposition`
--

CREATE TABLE `tblposition` (
  `ID` int(20) NOT NULL,
  `Position` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblposition`
--

INSERT INTO `tblposition` (`ID`, `Position`) VALUES
(3, 'Instructor-I'),
(4, 'Instructor-II'),
(5, 'Instructor-III'),
(6, 'Vocational Instruction Supervisor'),
(7, 'Assistant Professor I'),
(8, 'Assistant Professor II'),
(9, 'Assistant Professor III'),
(10, 'Assistant Professor IV'),
(11, 'PE Instructor'),
(13, 'Instructor-IV');

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty`
--

CREATE TABLE `tblresultfaculty` (
  `ID` int(20) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Evaluators` varchar(20) NOT NULL,
  `EvaluationResult_Student` varchar(20) NOT NULL,
  `EvaluationResult_Faculty` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_adaptability`
--

CREATE TABLE `tblresultfaculty_adaptability` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_AdaptabilityAvg` float NOT NULL,
  `Faculty_AdaptabilityDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_adaptability`
--

INSERT INTO `tblresultfaculty_adaptability` (`ID`, `ID_Number`, `Faculty_AdaptabilityAvg`, `Faculty_AdaptabilityDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(11, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_cat`
--

CREATE TABLE `tblresultfaculty_cat` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_CATAvg` float NOT NULL,
  `Faculty_CATDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_cat`
--

INSERT INTO `tblresultfaculty_cat` (`ID`, `ID_Number`, `Faculty_CATAvg`, `Faculty_CATDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(11, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_cr`
--

CREATE TABLE `tblresultfaculty_cr` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_CRAvg` float NOT NULL,
  `Faculty_CRDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_cr`
--

INSERT INTO `tblresultfaculty_cr` (`ID`, `ID_Number`, `Faculty_CRAvg`, `Faculty_CRDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_da`
--

CREATE TABLE `tblresultfaculty_da` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_DAAvg` float NOT NULL,
  `Faculty_DADump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_da`
--

INSERT INTO `tblresultfaculty_da` (`ID`, `ID_Number`, `Faculty_DAAvg`, `Faculty_DADump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_ec`
--

CREATE TABLE `tblresultfaculty_ec` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_ECAvg` float NOT NULL,
  `Faculty_ECDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_ec`
--

INSERT INTO `tblresultfaculty_ec` (`ID`, `ID_Number`, `Faculty_ECAvg`, `Faculty_ECDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_es`
--

CREATE TABLE `tblresultfaculty_es` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_ESAvg` float NOT NULL,
  `Faculty_ESDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_es`
--

INSERT INTO `tblresultfaculty_es` (`ID`, `ID_Number`, `Faculty_ESAvg`, `Faculty_ESDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(11, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_iaj`
--

CREATE TABLE `tblresultfaculty_iaj` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_IAJAvg` float NOT NULL,
  `Faculty_IAJDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_iaj`
--

INSERT INTO `tblresultfaculty_iaj` (`ID`, `ID_Number`, `Faculty_IAJAvg`, `Faculty_IAJDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_mc_atl`
--

CREATE TABLE `tblresultfaculty_mc_atl` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_MC_ATLAvg` float NOT NULL,
  `Faculty_MC_ATLDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tblresultfaculty_mc_atl`
--

INSERT INTO `tblresultfaculty_mc_atl` (`ID`, `ID_Number`, `Faculty_MC_ATLAvg`, `Faculty_MC_ATLDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_pa_cah`
--

CREATE TABLE `tblresultfaculty_pa_cah` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_PA_CAHAvg` float NOT NULL,
  `Faculty_PA_CAHDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tblresultfaculty_pa_cah`
--

INSERT INTO `tblresultfaculty_pa_cah` (`ID`, `ID_Number`, `Faculty_PA_CAHAvg`, `Faculty_PA_CAHDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_punctuality`
--

CREATE TABLE `tblresultfaculty_punctuality` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_PunctualityAvg` float NOT NULL,
  `Faculty_PunctualityDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_punctuality`
--

INSERT INTO `tblresultfaculty_punctuality` (`ID`, `ID_Number`, `Faculty_PunctualityAvg`, `Faculty_PunctualityDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_respect`
--

CREATE TABLE `tblresultfaculty_respect` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_RespectAvg` float NOT NULL,
  `Faculty_RespectDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_respect`
--

INSERT INTO `tblresultfaculty_respect` (`ID`, `ID_Number`, `Faculty_RespectAvg`, `Faculty_RespectDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultfaculty_responsibility`
--

CREATE TABLE `tblresultfaculty_responsibility` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Faculty_ResponsibilityAvg` float NOT NULL,
  `Faculty_ResponsibilityDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultfaculty_responsibility`
--

INSERT INTO `tblresultfaculty_responsibility` (`ID`, `ID_Number`, `Faculty_ResponsibilityAvg`, `Faculty_ResponsibilityDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultstd_commitment`
--

CREATE TABLE `tblresultstd_commitment` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Std_CommitmentAvg` float NOT NULL,
  `Std_CommitmentDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultstd_commitment`
--

INSERT INTO `tblresultstd_commitment` (`ID`, `ID_Number`, `Std_CommitmentAvg`, `Std_CommitmentDump`) VALUES
(1, '1998-2778', 0, 0),
(2, '1442-3488', 0, 0),
(3, '1534-1123', 0, 0),
(4, '1999-2776', 0, 0),
(5, '1990-2022', 0, 0),
(6, '2103-4122', 0, 0),
(7, '1997-0810', 0, 0),
(8, '2001-0518', 0, 0),
(9, '2316-2213', 0, 0),
(11, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultstd_kosm`
--

CREATE TABLE `tblresultstd_kosm` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Std_KOSMAvg` float NOT NULL,
  `Std_KOSMDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultstd_kosm`
--

INSERT INTO `tblresultstd_kosm` (`ID`, `ID_Number`, `Std_KOSMAvg`, `Std_KOSMDump`) VALUES
(1, '1998-2778', 0, 0),
(2, '1442-3488', 0, 0),
(3, '1534-1123', 0, 0),
(4, '1999-2776', 0, 0),
(5, '1990-2022', 0, 0),
(6, '2103-4122', 0, 0),
(7, '1997-0810', 0, 0),
(8, '2001-0518', 0, 0),
(9, '2316-2213', 0, 0),
(11, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultstd_mol`
--

CREATE TABLE `tblresultstd_mol` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Std_MOLAvg` float NOT NULL,
  `Std_MOLDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultstd_mol`
--

INSERT INTO `tblresultstd_mol` (`ID`, `ID_Number`, `Std_MOLAvg`, `Std_MOLDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresultstd_tfil`
--

CREATE TABLE `tblresultstd_tfil` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Std_TFILAvg` float NOT NULL,
  `Std_TFILDump` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresultstd_tfil`
--

INSERT INTO `tblresultstd_tfil` (`ID`, `ID_Number`, `Std_TFILAvg`, `Std_TFILDump`) VALUES
(2, '1998-2778', 0, 0),
(3, '1442-3488', 0, 0),
(4, '1534-1123', 0, 0),
(5, '1999-2776', 0, 0),
(6, '1990-2022', 0, 0),
(7, '2103-4122', 0, 0),
(8, '1997-0810', 0, 0),
(9, '2001-0518', 0, 0),
(10, '2316-2213', 0, 0),
(12, '1990-1201', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblsection`
--

CREATE TABLE `tblsection` (
  `ID` int(11) NOT NULL,
  `Section` varchar(20) NOT NULL,
  `Subject_Code` varchar(20) NOT NULL,
  `Faculty_ID` varchar(20) NOT NULL,
  `Student_ID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsection`
--

INSERT INTO `tblsection` (`ID`, `Section`, `Subject_Code`, `Faculty_ID`, `Student_ID`) VALUES
(4, 'Cookery II-A', '', '', ''),
(5, 'EIM I-A', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

CREATE TABLE `tblstudent` (
  `ID` int(11) NOT NULL,
  `ID_Number` varchar(20) NOT NULL,
  `Last_Name` varchar(50) NOT NULL,
  `First_Name` varchar(50) NOT NULL,
  `Evaluation_Date` varchar(50) NOT NULL,
  `Section` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`ID`, `ID_Number`, `Last_Name`, `First_Name`, `Evaluation_Date`, `Section`) VALUES
(1, '017-155-2017', 'Espino', 'Ruthcell', '', 'Cookery II-A'),
(2, '018-213-2017', 'Bhathi', 'Rashid', '', 'Cookery II-A'),
(3, '017-535-2017', 'Rodrigues', 'Kris Joy', '', 'Cookery II-A'),
(4, '018-565-2017', 'Cao', 'Clifford', '', 'Cookery II-A'),
(5, '018-522-2017', 'Gonzales', 'JV', '', 'Cookery II-A'),
(6, '015-200-2017', 'Pedro', 'Juan', '', 'Cookery II-A');

-- --------------------------------------------------------

--
-- Table structure for table `tblsubject`
--

CREATE TABLE `tblsubject` (
  `ID` int(20) NOT NULL,
  `Subject_Code` varchar(20) NOT NULL,
  `Subject` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubject`
--

INSERT INTO `tblsubject` (`ID`, `Subject_Code`, `Subject`) VALUES
(5, 'PCOM', 'Perform Computer Operations & Management');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblaccount`
--
ALTER TABLE `tblaccount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblfaculty`
--
ALTER TABLE `tblfaculty`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblposition`
--
ALTER TABLE `tblposition`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty`
--
ALTER TABLE `tblresultfaculty`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_adaptability`
--
ALTER TABLE `tblresultfaculty_adaptability`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_cat`
--
ALTER TABLE `tblresultfaculty_cat`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_cr`
--
ALTER TABLE `tblresultfaculty_cr`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_da`
--
ALTER TABLE `tblresultfaculty_da`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_ec`
--
ALTER TABLE `tblresultfaculty_ec`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_es`
--
ALTER TABLE `tblresultfaculty_es`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_iaj`
--
ALTER TABLE `tblresultfaculty_iaj`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_mc_atl`
--
ALTER TABLE `tblresultfaculty_mc_atl`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_pa_cah`
--
ALTER TABLE `tblresultfaculty_pa_cah`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_punctuality`
--
ALTER TABLE `tblresultfaculty_punctuality`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_respect`
--
ALTER TABLE `tblresultfaculty_respect`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultfaculty_responsibility`
--
ALTER TABLE `tblresultfaculty_responsibility`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultstd_commitment`
--
ALTER TABLE `tblresultstd_commitment`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultstd_kosm`
--
ALTER TABLE `tblresultstd_kosm`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultstd_mol`
--
ALTER TABLE `tblresultstd_mol`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresultstd_tfil`
--
ALTER TABLE `tblresultstd_tfil`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblsection`
--
ALTER TABLE `tblsection`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblstudent`
--
ALTER TABLE `tblstudent`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblsubject`
--
ALTER TABLE `tblsubject`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblaccount`
--
ALTER TABLE `tblaccount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tblfaculty`
--
ALTER TABLE `tblfaculty`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tblposition`
--
ALTER TABLE `tblposition`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty`
--
ALTER TABLE `tblresultfaculty`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblresultfaculty_adaptability`
--
ALTER TABLE `tblresultfaculty_adaptability`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_cat`
--
ALTER TABLE `tblresultfaculty_cat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_cr`
--
ALTER TABLE `tblresultfaculty_cr`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_da`
--
ALTER TABLE `tblresultfaculty_da`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_ec`
--
ALTER TABLE `tblresultfaculty_ec`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_es`
--
ALTER TABLE `tblresultfaculty_es`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_iaj`
--
ALTER TABLE `tblresultfaculty_iaj`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_mc_atl`
--
ALTER TABLE `tblresultfaculty_mc_atl`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_pa_cah`
--
ALTER TABLE `tblresultfaculty_pa_cah`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_punctuality`
--
ALTER TABLE `tblresultfaculty_punctuality`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_respect`
--
ALTER TABLE `tblresultfaculty_respect`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultfaculty_responsibility`
--
ALTER TABLE `tblresultfaculty_responsibility`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblresultstd_commitment`
--
ALTER TABLE `tblresultstd_commitment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblresultstd_kosm`
--
ALTER TABLE `tblresultstd_kosm`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblresultstd_mol`
--
ALTER TABLE `tblresultstd_mol`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblresultstd_tfil`
--
ALTER TABLE `tblresultstd_tfil`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblsection`
--
ALTER TABLE `tblsection`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblstudent`
--
ALTER TABLE `tblstudent`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tblsubject`
--
ALTER TABLE `tblsubject`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
